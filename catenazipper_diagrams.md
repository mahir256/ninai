# How CatenaZippers work

The bolded arrows in the below graph represent a CatenaZipper whose root
is "tar" and whose cursor is "god". It has a length of 3, for the nodes "ta", "ända", and "god":

```plantuml
    digraph g{
    node [shape=invhouse];
    edge [dir=back fontsize=8 fontcolor=blue];
    "ta" [ordering=out penwidth=3];
    "ta" -> "begynnelse" [xlabel=Q164573];
    "ta" -> "sällan" [tailport=se headport=nw xlabel=Q10401562];
    "ta" -> "ända" [xlabel=Q2990574 penwidth=3];
    "begynnelse" -> "ond" [xlabel=Q10401368 tailport=sw headport=ne];
    "ända" [penwidth=3];
    "god" [penwidth=3];
    "ända" -> "god" [tailport=sw headport=ne penwidth=3 xlabel=Q10401368];
    }
```
Some things to note about the above graph:

* The individual nodes are inverted house shapes. The bottom point of these shapes serves as a
  natural divider between logical left dependents and logical right dependents, which are indicated by
  the part of the parent node to which a connecting arrow from a dependent points.
* The type of relationship between a dependent Catena and a parent is represented with a Qid
  next to the arrow.
* The text in each node will generally be the lemma of the lexeme in that Catena.

Some things to note about other related graphs:

* If the types of relationship between certain nodes is not significant in a description,
  then those relationship types may be omitted from graphs used in that description.
* If a Catena identifier is used in a node, then it will be presented in boldface.
* If a form representation (or other non-lemma textual representation) is used in a node, then it will be presented
  in italics.
* If an entity ID (whether lexeme ID or sense ID) is used in a node, then it will be underlined.
* Inflection sets will not be represented as lists of Qids, since those lists frequently will either lead to
  large nodes in the graph or small text in a regular-sized node.
* Similarly, Catena configurations will not be represented in a textual form (other means will be used instead).

## When a CatenaZipper is modified

When the cursor is modified in any way (for instance, when a Catena is modified,
a new dependent must be created, or when that dependent is removed), a new CatenaZipper
with all parents adjusted to refer to that new dependent must also be created--a
textbook case of [persistence](https://en.wikipedia.org/wiki/Persistent_data_structure).

Using the CatenaZipper in the above graph, if the "god" node is modified at all,
then the following occur:

* A new "god" node is ultimately created, distinct from the original "god" node but sharing
    all details except for whatever was modified (marked with "†" and yellow background
    in the diagram below):

```plantuml
digraph g{
    node [shape=invhouse];
    edge [dir=back fontsize=8 fontcolor=blue];
    "ta" [ordering=out penwidth=3];
    "ta" -> "begynnelse" [xlabel=Q164573];
    "ta" -> "sällan" [tailport=se headport=nw xlabel=Q10401562];
    "ta" -> "ända" [xlabel=Q2990574 penwidth=3];
    "begynnelse" -> "ond" [xlabel=Q10401368 tailport=sw headport=ne];
    "ända" [penwidth=3];
    "god" [penwidth=1];
    "god†" [style=filled fillcolor=yellow penwidth=3];
    "ända" -> "god" [tailport=sw headport=ne penwidth=3 xlabel=Q10401368];
}
```

* Because of this distinction, the link between "ända" and "god" must also be modified to
    point to the new "god" node. Since this link exists in the "ända" node, modifying that
    link entails modifying the "ända" node and thus creating a new one:

```plantuml
digraph g{
    node [shape=invhouse];
    edge [dir=back fontsize=8 fontcolor=blue];
    "ta" [ordering=out penwidth=3];
    "ta" -> "begynnelse" [xlabel=Q164573];
    "ta" -> "sällan" [tailport=se headport=nw xlabel=Q10401562];
    "ta" -> "ända" [xlabel=Q2990574 penwidth=3];
    "begynnelse" -> "ond" [xlabel=Q10401368 tailport=sw headport=ne];
    "ända†" [penwidth=3 style=filled fillcolor=yellow];
    "god" [penwidth=1];
    "god†" [penwidth=3 style=filled fillcolor=yellow];
    "ända" -> "god" [tailport=sw headport=ne xlabel=Q10401368];
    "ända†" -> "god†" [tailport=sw headport=ne penwidth=3 xlabel=Q10401368];
}
```

* Modifying the "ända" node requires adjusting the link between "ta" and "ända", which
    entails, in addition to creating a new "ta" node, also adjusting links to the other
    branches of the "ta" node to point to the new one:

```plantuml
digraph g{
    node [shape=invhouse];
    edge [dir=back fontsize=8 fontcolor=blue];
    "ta" -> "ända" [xlabel=Q2990574]
    "ända" -> "god" [xlabel=Q10401368]
    "ta†" [ordering=out penwidth=3 style=filled fillcolor=yellow];
    "ta†" -> "begynnelse" [xlabel=Q164573];
    "ta†" -> "sällan" [tailport=se headport=nw xlabel=Q10401562];
    "ta†" -> "ända†" [xlabel=Q2990574 penwidth=3];
    "begynnelse" -> "ond" [xlabel=Q10401368 tailport=sw headport=ne];
    "ända†" [penwidth=3 style=filled fillcolor=yellow];
    "god" [penwidth=1];
    "god†" [penwidth=3 style=filled fillcolor=yellow];
    "ända†" -> "god†" [tailport=sw headport=ne penwidth=3 xlabel=Q10401368];
}
```

The end result of this entire process is a new CatenaZipper reflecting all of the changes
made, to which other changes may be applied. *If the original CatenaZipper for this proverb were to be
used later, it would not reflect the changes above--only the new CatenaZipper can be used for that!*

## CatenaZipper modifications

### attach_left_of_root

As an example, if **self** were the Catena with the below structure:

```plantuml
digraph g{
node [shape=invhouse];
edge [dir=back fontsize=8 fontcolor=blue];
"brüten" -> "langsam"
"brüten" -> "Adler"
"brüten" -> "Taube"
"brüten" -> "Park"
"Park" [ordering="out"];
"Park" -> "in" [tailport=sw headport=ne];
"Park" -> "das" [tailport=sw headport=ne];
}
```

then this function with **rel** = [Q12724479](https://www.wikidata.org/entity/Q12724479) and **dependents** being the below Catena:

```plantuml
digraph g{
node [shape=invhouse];
edge [dir=back];
"Minuten" -> "drei";
"Minuten" -> "lang";
}
```

would yield the following:

```plantuml
digraph g{
node [shape=invhouse];
edge [dir=back fontsize=8];
"brüten" [ordering="out"];
"brüten" -> "Minuten" [xlabel="Q12724479"];
"Minuten" [ordering="out"];
"Minuten" -> "drei";
"Minuten" -> "lang";
"brüten" -> "langsam";
"brüten" -> "Adler" [tailport=sw headport=ne];
"brüten" -> "Taube";
"brüten" -> "Park";
"Park" [ordering="out"];
"Park" -> "in" [tailport=sw headport=ne];
"Park" -> "das" [tailport=sw headport=ne];
}
```

## More CatenaZipper examples

### Adler und Tauben

```plantuml
digraph g{
node [shape=invhouse];
edge [dir=back fontsize=8 fontcolor=blue];
"brüten" -> "langsam"
"brüten" -> "Adler"
"brüten" -> "Taube"
"brüten" -> "Park"
"Park" [ordering="out"];
"Park" -> "in" [tailport=sw headport=ne];
"Park" -> "das" [tailport=sw headport=ne];
}
```
