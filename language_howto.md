# So you want to improve Ninai/Udiron's support for your language?

You have some sentences in mind you want to generate, or you found some other sentences you'd like to generate, in your language, and you want to make this happen with Ninai/Udiron?
You've come to the right place!

This page serves as a guide for those wanting to contribute to Ninai/Udiron, with the eventual goal of being able to produce a sentence of some sort in a given language from some abstract content.

If your language has *absolutely nothing* set up in Ninai/Udiron for it, then you may end up spending a bit more time with this page.
Once you have many of the one-time things set up, however, there will be less to do later should you need to return to this page!

## Some preliminary notes

You will need to have Ninai and Udiron set up to begin using this page, for which see the README.

It may be daunting to those not previously experienced with programming to have to work with Python code directly, but it is hoped that code here can be written so that it is simple and readable and affected by Python-specific quirks as little as possible, and so that the results from development of this software are more readily transferable to Wikifunctions in the future.

A bullet point beginning with 'ⓘ' represents a general clarifying note about an earlier mentioned step.

A bullet point beginning with '⚕' represents a difference between how things are implemented now, in purely Python code, and how things could be different with Wikifunctions. You may skip these if you wish, but if you have questions about a particular statement in one of these points, the default answer is 'ask [Denny](https://meta.wikimedia.org/wiki/User:DVrandecic_(WMF))!'.

* ⚕ With respect to the first paragraph above, there are quite a few libraries that could be imported to make the code in Ninai/Udiron more readable and intuitive--including [more-itertools](https://more-itertools.readthedocs.io/en/stable/) and [funcy](https://funcy.readthedocs.io/en/stable/)--but as there is no guarantee that these or equivalents of their functions will be available or replicable on Wikifunctions, the sole reliance on Python's standard library will nevertheless cause certain parts of the code to be difficult to simplify or abstract away easily.

## Prologue: Create an abstract representation of what you want to say

The first thing you will need to do is come up with a way to represent your sentence in a language-independent fashion resembling nested functions:

```python
SortOf(
    Like(
        This(),
        And(),
        That(
            And("That", And(
                "This"
            ))
        )
    )
)
```

You can check out [the list of Constructors on Elemwala](https://elemwala.toolforge.org/constructors) to find ones that are useful to you, whose examples might inspire you to introduce some new Constructors, and to assemble some abstract content from them!
(If all the ones you need already exist, you may skip step 1 below!)

## 0. Add some lexicographical data to Wikidata

The sentences you want to express likely consist of words, phrases, or their parts, so in general lexemes for those things, along with senses representing their meanings, need to exist on Wikidata.

Regardless of what modeling choices you make for lexemes in your language, make sure that they are consistent across lexemes of similar types, and that exceptions themselves can be identified readily!
(Your job in later steps may be complicated significantly if this does not end up being the case!)

More information on what you may need to do can be found [on this general documentation page](https://www.wikidata.org/wiki/Wikidata:Lexicographical_data/Documentation), [on this best practices page](https://www.wikidata.org/wiki/Wikidata:Lexicographical_data/Best_practices), or [on a documentation page specific to your language](https://www.wikidata.org/wiki/Wikidata:Lexicographical_data/Documentation/Languages) (if it exists).

## 1. Add some Constructors for your abstract content

*(If all the Constructors you need already exist, you may skip this step!)*

To introduce a new type of Constructor, you will need to define a special function, called an *argument filter*, that will process the arguments you provide into a form that the base system can handle.

### Code setup (for new files)

If you are not adding to a file that already contains defined Constructor types--that is, you are creating an entirely new file to reside in [ninai.constructors](https://gitlab.com/mahir256/ninai/-/blob/main/ninai/constructors) (let us call this new file `myconstructor.py`)--you will need to do the following:

* Add the following import lines to the start of your file:
```python
import ninai.base.interfaces as I
from ninai import Constructor, set_argument_filter
```
* Add the following line to the bottom of [ninai.constructors.\_\_init\_\_](https://gitlab.com/mahir256/ninai/-/blob/main/ninai/constructors/__init__.py):
```python
from ninai.constructors.myconstructor import *
```

### Defining an argument filter (for all files)

An argument filter may be defined in three lines of actual code (and at least one line of documentation) such as follows:

```python
@set_argument_filter("Q1049476", ["Q1", "Q12345"], ["Q164573", "Q175026"]) # Line 1
def MyConstructorType(*args: I.ConstructorArgument) -> Constructor: # Line 2
    """ Your short description of what this Constructor represents here. """
    return Constructor.new() # Line 3
```

Line 1 has three obligatory parameters: an identifier used to represent the Constructor type whenever abstract content is *not* written in Python code; similar identifiers for other Constructor types indicating a subtype relationship; and identifiers for any obligatory arguments to the Constructor (used in renderers to access those arguments).
(In the code sample above, the Constructor type 'Q1049476' is marked as a subtype of the Constructor types 'Q1' and 'Q12345' and requires at least two arguments when used, which will be found in entries 'Q164573' and 'Q175026'.)
Line 2 should be the same in all argument filters except for substituting `MyConstructorType` with some more human-readable name, which will be used whenever abstract content needs to be written directly in Python code.
Line 3 should be the same in all argument filters.

### Notes

* ⓘ The identifiers in line 1 are all Wikidata item IDs; while they do not *have* to be such IDs, they at least contribute to the language-neutrality of the overall system (but see the below point).
* ⚕ Since Python code is organized into modules, any code residing in those modules which needs to be used requires that it first be imported. In Wikifunctions, any function can be called simply by naming it--no import mechanism needed.
* ⚕ The human-readable name in line 2 is currently in English, but it could be in another language so long as it is understood what it means when looking at Python code. In Wikifunctions, code would need to use a ZID instead of any single human-readable name, but at least translations of this name could reside in the function object corresponding to that ZID.
* ⚕ The Python decorator `set_argument_filter` does quite a bit of magic, including substituting line 3 with an actual procedure that processes inputs and registering the new Constructor type in several places. In Wikifunctions, these steps would need to be performed manually for each new Constructor type.

## 2. Add some functions to edit syntax

Now that you have Constructor types at your disposal, you will need to consider what processes may be involved in building a sentence in your language and add functions for these processes to Udiron. This too is language dependent, so the links in section 0 above will be helpful as well, but [this walkthrough](https://www.wikidata.org/wiki/Wikidata:Lexicographical_data/Universal_Dependencies/Walkthrough) should help clarify how the syntax of languages is modeled on Wikidata, and correspondingly how it is manifested in Ninai/Udiron. You may also find code for other languages useful to look at to determine how they handle a particular phenomenon and how your language may be similar or differ in handling the same phenomenon.

### One-time steps (for new languages)

If your language does not have a subfolder in [udiron.langs], you should do the following:
* Create such a folder (usually with a language code, but some other lowercase name may suffice as well--let us call the folder "und").
* Add the name of this folder to the line defining `__all__` in [udiron.langs.\_\_init\_\_](https://gitlab.com/mahir256/udiron/-/blob/main/udiron/langs/__init__.py#L24):
```python
__all__ = ['mul','und']
```
* Create three files inside your new folder:
  * `constants.py`, which holds Wikidata IDs for items, lexemes, and senses of particular relevance to your language;
  * `__init__.py`, which holds the actual syntax editors for your language; and
  * `to_str.py`, which holds methods used when converting syntax trees to readable strings.

As a matter of good style, you should define a constant in `constants.py` and use that whenever you need to refer to a specific Wikidata ID in `__init__.py` or `to_str.py` (and later in step 3 for Ninai renderers).
Here's an example constants file showing how different constants may be defined:
```python
from tfsl.interfaces import Qid, Lid, LSid

turkish_grammar = Qid('Q897661')
biz_pronoun_lexeme = Lid('L294424')
biz_pronoun_sense = LSid('L294424-S1')
```
The import line above need only be present once, but there can be as many constants as are needed for the handling of your language. Typically these will represent lexemes for function words/morphemes, senses on those lexemes, or items representing features of grammatical significance to your language.

### General steps (for all languages)

The files `__init__.py` and `to_str.py` (and `constants.py`) may need to be revised at different times as new needs surface with regard to the editing of syntax trees in your language.

Defining syntax tree editors in `__init__.py` can generally be done in whatever way you wish, so long as those editors are accessible from Ninai (see the next step) and they rely only on the Python standard library. As a good practice, however, if you find yourself re-implementing in your language a mechanism that at least two other languages have already implemented in the exact same way, you may consider instead creating a more general function, to reside in the `mul` subfolder of `udiron.langs`, and use that in a way specific to your language instead. The file `function_reference.md` contains a list of functions that may be useful in lots of places, including defining syntax tree editors.

(Get in touch with Mahir256 for more information about setting up the `to_str.py` file.)

### Notes

* ⚕ In principle something like the `constants.py` file would not be necessary on Wikifunctions, as a facility should in principle exist to auto-convert Wikidata IDs to readable labels in the user interface.

## 3. Add some Renderers for Constructors

Now that we have some Constructor types, and now that we have some syntax tree editors, we need to develop a way to connect these: this is achieved through developing Renderers for the Constructors in your abstract content!

Each Constructor type must have a Renderer for it in your language in order to render any abstract content containing that Constructor type; if such a Renderer does not exist, then rendering will fail.

### One-time steps (for new languages)

If your language lacks a file named after it in [ninai.renderers](https://gitlab.com/mahir256/ninai/-/tree/main/ninai/renderers), you should do the following:
* Create such a file (generally with the same name as the Udiron subfolder you created in step 2--which in our example case would be `und.py`).
* Add the name of this file--without the `.py` suffix--to the line defining `__all__` in [ninai.renderers.\_\_init\_\_](https://gitlab.com/mahir256/ninai/-/blob/main/ninai/renderers/__init__.py#L24):
```python
__all__ = ['mul','und']
```
* Add at least the following lines to the start of this file:
```python
from tfsl import langs

import ninai.base.interfaces as I
from ninai.renderers import register_
```

### Common steps (for all languages)

The simple skeleton of a Renderer is as follows:
```python
@register_("renderer", "Q1049476", langs.und_) # Line 1
def myconstructortype_renderer_und(args: RendererArguments) -> I.RendererOutput: # Line 2
    """ Your short description of what this Renderer does here. """
    # code that defines 'output' and 'config' goes here
    return output, config # Line 3
```
Line 1 above requires at least two substitutions: of "Q1049476" (the identifier used to represent the Constructor type defined in step 1) for the corresponding identifier of the Constructor type you're defining a Renderer for, and of `langs.und_` with the specific object in [tfsl.languages.langs](https://gitlab.wikimedia.org/toolforge-repos/twofivesixlex/-/blob/main/tfsl/languages.py#L87) representing your language (e.g. for Latin, which appears as `self.la_` in that file, use `langs.la_`).
Line 2 should be the same in all Renderers except for substituting `myconstructortype_renderer_und` with some more human-readable name.
The code that defines 'output' and 'config' should be calling out to methods defined in `udiron.langs.und`, so that another line needs to be added to the top of `und.py`:
```python
import udiron.langs.und as und
```
(The `as und` portion of the above is a simple shorthand so that, for a function `function_name` defined in `udiron.langs.und`, you need only refer to `und.function_name` each time and not `udiron.langs.und.function_name`.)
Line 3 should ideally be the same, but it need not be so long as what is `return`ed from the Renderer is a `CatenaZipper` followed by a `FunctionConfig` (contact Mahir256 for information on what this sentence means).

A lot of the same flexibility that exists with creating syntax tree editors also applies with creating Constructor Renderers.

### Notes

* ⚕ The Python decorator `register_` does some magic steps registering the new Renderer so that it can be referred to readily by the base system. In Wikifunctions, these steps would need to be performed manually for each new Renderer.

## 4. Try rendering your abstract content!

Once you have completed the steps above, you can try to render your abstract content from the prologue as follows:

```python
from tfsl import langs

from ninai.constructors import *
from ninai.renderers import und

myabstractcontent = SortOf(
    Like(
        This(),
        And(),
        That(
            And("That", And(
                "This"
            ))
        )
    )
)
print(myabstractcontent(langs.und_))
```

## Epilogue: Add the code to Ninai/Udiron!

If Wikifunctions existed right now, most of the steps you performed above would be taking place through editing objects using its web interface. Yet, alas, there are only Git repositories and other things of that nature here, so there is only the option of committing code to the Ninai and Udiron repositories once you feel comfortable enough to add it. Contact Mahir256 if you need help with this step.
