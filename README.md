# Ninai

* கண்தெரிந்து வழி நடக்கும்படி நினை. (Take care that you walk with your eyes open.)
* ஒரு நன்றி செய்தவரை உள்ள அளவும் நினை. (Think of those who have done you even one favour as long as you live.)

([translations above by Peter Percival](https://en.wikisource.org/wiki/Tamil_Proverbs))

## About

***The base system is currently being modified to increase code quality and documentation! Check out [this revision](https://gitlab.com/mahir256/ninai/-/tree/41be7b646245c5ba9bd6c4bf811124ce225fc329) for the state of this repository prior to these modifications!***

Ninai (from the classical Tamil for 'to think') is a statement construction system that uses, among other things, Wikidata items and lexemes with other grammatical primitives for the construction of complex assertions that can be rendered using [Udiron](https://gitlab.com/mahir256/udiron/) into a given language.

(The author of this project is willing to relinquish the name Ninai to the set of constructors that will actually be in use on the Abstract Wikipedia, whether or not derived from what is developed here.)

## Setup

(The code here was developed with Python 3.9 and 3.10;
in particular, the dependency on graphlib in ninai.base.argumentfilters requires
that a minimum of Python 3.9 be used to run it.)

You should first [set up udiron](https://gitlab.com/mahir256/udiron/)--visit the "Setup" section of that library's README for further steps--before proceeding with the following.

[Clone this repository](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository):

```
git clone https://gitlab.com/mahir256/ninai
```

The cloned repository should now reside in some folder, which may vary depending on where you performed the clone. Let's call that folder `/path/to/ninai` (or `C:\path\to\ninai` on a Windows system).

Make sure the cloned repository ends up in your PYTHONPATH. The simplest way to do this, [for a Unix (Linux/Mac OS/BSD) system](https://stackoverflow.com/q/3402168/), is below, where the folder path `/path/to/ninai` must be substituted with the actual folder into which Ninai was cloned:

```
export PYTHONPATH=$PYTHONPATH:/path/to/ninai
```

([On Windows this process is a bit different.](https://stackoverflow.com/q/3701646))

Now install Ninai's dependencies, again substituting the paths accordingly:

```
pip install -r /path/to/ninai/requirements.txt
```

Now rename 'config.ini.example' to 'config.ini' and specify
1) where the translation network should be stored ('CachePath'),
2) how long (in seconds) this network should be stored before regeneration ('TimeToLive'), and
3) the URL (including host name and port number) of the graph server to which Ninai should address graph-related requests ('GraphServerURL').

(When running the graph server using `python3 -m ninai.graph.server`, a port must be specified inside the `GraphServerURL`, since this is passed directly to `SimpleXMLRPCServer` as one the elements of the 'addr' argument. This is not strictly necessary when only using the graph client.)

## Use

Some examples of Ninai's use may be found [here](demonstrations.md).

[Find the auto-generated documentation of Ninai here](https://gitlab.com/mahir256/ninai/-/wikis/home).

## Licensing

All code herein is Apache 2.0 unless otherwise stated below.
