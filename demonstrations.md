# Demonstrations of Ninai

This is a list of demonstration utterances for different languages.
They are listed below in an order roughly corresponding to when some functionality
that was needed for their operation was introduced.
Descriptions of each demonstration are given in the documentation comment
at the top of each file.

For information about the individual Constructors, see [the auto-generated documentation for them](https://gitlab.com/mahir256/ninai/-/wikis/ninai.constructors).

## How to run these

First you need to connect to a graph server locally. You can run a basic one using command:

```
$ python3 -m ninai.graph.server
```

Once this message appears (this should take a little while--the port number shown will be that specified in config.ini)...

```
Listening on port 7834...
The Ninai graph server is now running.
graph_server> 
```

...you can then use another terminal to run e.g.

```
$ python3 ./demonstrations/jupiter.py
```

(The file name to use in the command above is the name of the file pointed to by one of the links below; since
the Jupiter example resides at 'demonstrations/jupiter.py', that is what is used in the command above.)

## Basic demonstrations list

The languages involved in each of these examples is given after each link.

* [Jupiter sentences](demonstrations/jupiter.py) (Bengali, Swedish)
* [Kishore Kumar (1)](demonstrations/kishore1.py) (Bengali, Swedish)
* [Kishore Kumar (2)](demonstrations/kishore2.py) (Bengali)
* [Descriptions of places and people](demonstrations/descriptions.py) (German)
* [Interactions between objects](demonstrations/interactions.py) (Bengali, Swedish)
* [Wegnehmen, arbeiten, and schneien](demonstrations/wegnehmen.py) (German)
* [Ring Ring, bara...](demonstrations/ringring.py) (Bengali, Swedish)
* [Mauvais et Bougie](demonstrations/bougie.py) (French)
* [Planets in Breton](demonstrations/planets.py) (Breton)
* [Swimming and lakes](demonstrations/lakes.py) (German, Swedish)

## Reference contexts

Below are a number of examples of the 'RefContext',
a structure in which world modeling could occur
and the styles of renderer outputs might be adjusted.

(Note that the precise interfaces to a 'RefContext' are subject to change.)

* [Items about two people](demonstrations/refcontext_people.py) (German)
* [Items about two people (with meta-constructors)](demonstrations/refcontext_meta.py) (German)
* [Saving and modifying constructors](demonstrations/refcontext_saving.py) (German)
* ["If you dressed like a wife..."](demonstrations/refcontext_jodibousajogo.py) (Bengali)
* [Language styles for planets](demonstrations/refcontext_planet.py) (Swedish)
* [Sense usage locations for bread rolls](demonstrations/refcontext_breadrolls.py) (German, Swedish)
* [Exclamations in Bokmål](demonstrations/refcontext_exclamations.py) (Bokmål)
* [Books in the park](demonstrations/refcontext_park.py) (German, Swedish)
* [Breadrolls in language varieties](demonstrations/refcontext_breadrolls2.py) (German, Swedish)
