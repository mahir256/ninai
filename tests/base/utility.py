"""Holds tests for functions in ninai.base.utility."""

import tfsl.interfaces as tfsli
import udiron.base.constants as c

import ninai.base.utility as u
from tests import WikifunctionsFunctionTest

hridoy = tfsli.LSid("L301993-S1")
buch = tfsli.LSid("L7895-S1")


class IsDesiredGender(WikifunctionsFunctionTest):
    """Tests for the 'is_desired_gender' function.

    Attributes:
        args: Function being run and arguments thereto.
    """
    args = (
        u.is_desired_gender, [
            ((hridoy, c.masculine), {}, False),
            ((buch, c.neuter), {}, True),
        ],
    )


class IsDesiredPos(WikifunctionsFunctionTest):
    """Tests for the 'is_desired_pos' function.

    Attributes:
        args: Function being run and arguments thereto.
    """
    args = (
        u.is_desired_pos, [
            ((hridoy, c.verb), {}, False),
            ((buch, c.noun), {}, True),
        ],
    )
