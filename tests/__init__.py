"""Holds the base class for all tests in Ninai."""

import unittest
from typing import Any, Callable, List, Mapping, Optional, Tuple

FunctionType = Optional[Callable[..., Any]]
ParametersType = List[Tuple[Tuple[Any, ...], Mapping[str, Any], Any]]


class WikifunctionsFunctionTest(unittest.TestCase):
    """Base class for all tests in Ninai.

    Each subclass must provide its own tuple for 'args',
    where the first entry is the actual function to be tested,
    and the second entry is a list of lists of parameters,
    where each list of parameters consists of, in order,
    1) positional arguments,
    2) keyword arguments, and
    3) either an expected return value (against which the actual return value is equality-compared) or a function, taking the actual return value as its only argument, which if returning True passes the test.

    `test` runs subtests by taking the function and, for each parameter list,
    supplying the arguments to the function and then checking that the actual
    return value is the same as the expected return value.

    Attributes:
        args: Function being run and arguments thereto.
    """
    args: Tuple[
        FunctionType,
        ParametersType,
    ] = (None, [])

    def test(self) -> None:
        """Runs all tests."""
        function, parameters = self.args
        if callable(function):
            for args, kwargs, ret in parameters:
                with self.subTest(args=args, kwargs=kwargs, ret=ret):
                    if callable(ret):
                        result = function(*args, **kwargs)
                        self.assertTrue(ret(result))
                    else:
                        self.assertEqual(function(*args, **kwargs), ret)
