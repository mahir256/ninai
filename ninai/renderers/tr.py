"""Holds all renderers for Turkish."""

from typing import TYPE_CHECKING

import udiron.base.constants as c
import udiron.base.interfaces as udironi
import udiron.langs.tr as tr
import udiron.langs.tr.constants as ctr
from tfsl import Language, langs
from udiron import CatenaZipper, FunctionConfig

import ninai.base.constructorinterfaces as ci
import ninai.base.interfaces as i
from ninai.graph import (add_to_fallback_chain, calque, cognate,
                         constructorify, transcribe)
from ninai.renderers import register
from ninai.renderers.mul import (add_full_stop_if_needed, get_listener_length,
                                 retrieve_sense)

if TYPE_CHECKING:
    from ninai import Context, RendererArguments

add_to_fallback_chain(langs.tr_, "predicate", calque.calque)
add_to_fallback_chain(langs.tr_, "substantive", transcribe.transcribe)
add_to_fallback_chain(langs.tr_, "substantive", constructorify.constructorify)
add_to_fallback_chain(langs.tr_, "substantive", calque.calque)
add_to_fallback_chain(langs.tr_, "substantive", cognate.cognate)


@register("renderer", c.speaker, langs.tr_)
def speaker_renderer_tr(args: "RendererArguments") -> i.RendererOutput:
    """Should return the pronoun 'ben' or a synonym thereof.

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.
    """
    return retrieve_sense(ctr.ben_pronoun, args), args.config


@register("renderer", c.listener, langs.tr_)
def listener_renderer_tr(args: "RendererArguments") -> i.RendererOutput:
    """Should return the pronoun 'sen' or a synonym thereof.

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.
    """
    if get_listener_length(args) > 1:
        return retrieve_sense(ctr.siz_pronoun, args), args.config
    return retrieve_sense(ctr.sen_pronoun, args), args.config


@register("renderer", c.classificational_construction, langs.tr_)
def classificational_renderer_tr(args: "RendererArguments") -> i.RendererOutput:
    """Should produces a classificational sentence.

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.
    """
    subject = args.core(c.subject)
    attribute = args.core(c.class_item)
    return attribute.attach_left_of_root(subject, c.subject), args.config


@register("renderer", c.attributional_construction, langs.tr_)
def attributional_renderer_tr(args: "RendererArguments") -> i.RendererOutput:
    """Should produces an attributional sentence.

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.
    """
    subject = args.core(c.subject)
    attribute = args.core(c.attributive)
    return attribute.attach_left_of_root(subject, c.subject), args.config


@register("renderer", c.locative, langs.tr_)
def locative_renderer_tr(args: "RendererArguments") -> i.RendererOutput:
    """Yields a phrase marked with the locative suffix.

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.
    """
    result = tr.mark_with_locative(args.core(c.complement), args.config)
    return result, args.config


@register("renderer", c.ablative, langs.tr_)
def ablative_renderer_tr(args: "RendererArguments") -> i.RendererOutput:
    """Yields a phrase marked with the ablative suffix.

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.
    """
    result = tr.mark_with_ablative(args.core(c.complement), args.config)
    return result, args.config


@register("renderer", c.allative, langs.tr_)
def allative_renderer_tr(args: "RendererArguments") -> i.RendererOutput:
    """Yields a phrase marked with the dative suffix.

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.
    """
    result = tr.mark_with_dative(args.core(c.complement), args.config)
    return result, args.config


@register("renderer", c.possession, langs.tr_)
def possession_renderer_tr(args: "RendererArguments") -> i.RendererOutput:
    """Produces a noun phrase indicating one object possessed by another.

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.
    """
    result = tr.possessive_noun_phrase(args.core(c.possessor), args.core(c.possessed), args.config)
    return result, args.config


@register("renderer", c.inessive, langs.tr_)
def inessive_renderer_tr(args: "RendererArguments") -> i.RendererOutput:
    """Yields a postpositional phrase using 'iç' and the locative suffix.

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.
    """
    complement = tr.mark_with_genitive(args.core(c.complement), args.config)
    result = tr.possessive_marked_postpositional_phrase(
        complement,
        retrieve_sense(ctr.ic_inside, args),
        c.locative,
        args.config,
    )
    return result, args.config


@register("renderer", c.illative, langs.tr_)
def illative_renderer_tr(args: "RendererArguments") -> i.RendererOutput:
    """Yields a postpositional phrase using 'iç' and the dative suffix.

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.
    """
    complement = tr.mark_with_genitive(args.core(c.complement), args.config)
    result = tr.possessive_marked_postpositional_phrase(
        complement,
        retrieve_sense(ctr.ic_inside, args),
        c.dative,
        args.config,
    )
    return result, args.config


@register("initial_catena_processor", langs.tr_)
def icp_tr(verb_catena: CatenaZipper) -> CatenaZipper:
    """Performs some initial processing of the verb Catena.

    Args:
        verb_catena: Verb Catena to process.

    Returns:
        Processed verb Catena.

    Todo:
        should probably do something (like split the Catena if it represents a phrase)
    """
    return verb_catena


@register("syntactic_role_function", c.subject, langs.tr_)
def srf_subj_tr(verb_catena: CatenaZipper, subject_catena: CatenaZipper, args: "RendererArguments") -> CatenaZipper:
    """Attaches a subject and inflects the verb for person and number of that subject.

    Args:
        verb_catena: Verb catena, possibly with other things attached to it.
        subject_catena: Subject to attach.
        args: Rest of the RendererArguments.

    Returns:
        Verb Catena with the subject attached to it.
    """
    verb_catena = tr.attach_argument_to_predicate(verb_catena, subject_catena, c.subject, FunctionConfig.new())
    verb_catena = tr.inflect_for_person(verb_catena, subject_catena)
    verb_catena = tr.inflect_for_number(verb_catena, subject_catena)
    return verb_catena


@register("syntactic_role_function", c.direct_object, langs.tr_)
def srf_obj_tr(verb_catena: CatenaZipper, object_catena: CatenaZipper, args: "RendererArguments") -> CatenaZipper:
    """Attaches a direct object after marking it with the accusative suffix.

    Args:
        verb_catena: Verb catena, possibly with other things attached to it.
        object_catena: Direct object to attach.
        args: Rest of the RendererArguments.

    Returns:
        Verb Catena with the direct object attached to it.
    """
    object_catena = tr.mark_with_accusative(object_catena, args.config)
    verb_catena = tr.attach_argument_to_predicate(verb_catena, object_catena, c.direct_object, FunctionConfig.new())
    return verb_catena


@register("syntactic_role_function", c.indirect_object, langs.tr_)
def srf_iobj_tr(verb_catena: CatenaZipper, object_catena: CatenaZipper, args: "RendererArguments") -> CatenaZipper:
    """Attaches an indirect object after marking it with the dative suffix.

    Args:
        verb_catena: Verb catena, possibly with other things attached to it.
        object_catena: Indirect object to attach.
        args: Rest of the RendererArguments.

    Returns:
        Verb Catena with the indirect object attached to it.
    """
    object_catena = tr.mark_with_dative(object_catena, args.config)
    verb_catena = tr.attach_argument_to_predicate(verb_catena, object_catena, c.indirect_object, FunctionConfig.new())
    return verb_catena


@register("inflection_function", langs.tr_)
def inflection_function_tr(input_catena: CatenaZipper, args: "RendererArguments") -> CatenaZipper:
    """Simply calls inflect_for_tense_aspect_mood.

    Args:
        input_catena: Catena to be inflected.
        args: Rest of the RendererArguments.

    Returns:
        Inflected Catena.
    """
    return tr.inflect_for_tense_aspect_mood(input_catena, args.config)


def process_scope_outputs(base_catena: CatenaZipper, outputs: udironi.ScopeArgumentMapping, config: FunctionConfig) -> CatenaZipper:
    """Processes scope outputs, possibly attaching them to a base Catena.

    Args:
        base_catena: Catena to which other arguments are being attached.
        outputs: Mapping of scope argument types to lists of scope arguments.
        config: Additional function configuration.

    Returns:
        Input Catena with scope outputs added to it.

    Todo:
        Perhaps this should discard an output from the dict once it is attached to the catena?
    """
    for key in outputs:
        if ci.is_constructor_subtype(key, c.spatial_relation):
            for entry in outputs[key]:
                base_catena = tr.attach_adverbial_to_predicate(base_catena, entry, config)
    return base_catena


@register("scope_output_processor", langs.tr_)
def sop_tr(input_catena: CatenaZipper, scope_outputs: udironi.ScopeArgumentMapping, config: FunctionConfig) -> CatenaZipper:
    """Handles any arguments not covered by a syntactic role function.

    Args:
        input_catena: Catena to which other arguments are being attached.
        scope_outputs: Mapping of scope argument types to lists of scope arguments.
        config: Additional function configuration.

    Returns:
        Input Catena with scope outputs added to it.
    """
    return process_scope_outputs(input_catena, scope_outputs, config)


@register("post_hook", c.attributional_construction, langs.tr_)
@register("post_hook", c.classificational_construction, langs.tr_)
@register("post_hook", c.predicate, langs.tr_)
def action_post_hook_tr(output: CatenaZipper, language: Language, context: "Context", config: FunctionConfig) -> i.PostHookOutput:
    """Simply adds an ending punctuation mark.

    Args:
        output: Original output.
        language: Language being rendered into.
        context: Context of this rendering step.
        config: Additional function configuration.

    Returns:
        Original output with punctuation added, and unmodified FunctionConfig.
    """
    return add_full_stop_if_needed(output, language, context, config), config
