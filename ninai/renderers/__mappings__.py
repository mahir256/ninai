"""Holds the mappings from languages to different functions for different Constructor renderers.

* ⚠️ Warning: Methods in this library rely on the language fallbacks defined
  in udiron.langs!
* ⌨ Implementation detail: The functions defined here are unlikely to be useful on Wikifunctions,
  since the actual maps should themselves be editable on that platform.
"""

from collections import defaultdict
from typing import (TYPE_CHECKING, Any, Callable, DefaultDict, Dict, Literal,
                    TypeVar)

from typing_extensions import ParamSpec
from udiron.base.interfaces import get_language_fallbacks

if TYPE_CHECKING:
    from tfsl import Language

    import ninai.base.interfaces as i

__arg_pre_hooks__: DefaultDict[str, DefaultDict[str, Dict["Language", "i.ArgPreHook"]]] = defaultdict(lambda: defaultdict(dict))


def get_arg_pre_hook(constructor: str, arg: str, lang: "Language") -> "i.ArgPreHook":
    """Retrieves an i.ArgPreHook for a given constructor-argument-language triple.

    Args:
        constructor: Constructor type to get an ArgPreHook for.
        arg: Argument name to get an ArgPreHook for.
        lang: Language to get an ArgPreHook for.

    Returns:
        Appropriate ArgPreHook.
    """
    return __arg_pre_hooks__[constructor][arg][lang]


def set_arg_pre_hook_registration(constructor: str, arg: str, lang: "Language") -> Callable[["i.ArgPreHook"], "i.ArgPreHook"]:
    """Maps a constructor-argument-language triple to an i.ArgPreHook.

    Args:
        constructor: Constructor type to register an ArgPreHook for.
        arg: Argument name to register an ArgPreHook for.
        lang: Language to register an ArgPreHook for.

    Returns:
        Function performing the actual registration.
    """
    def set_arg_pre_hook(method_in: "i.ArgPreHook") -> "i.ArgPreHook":
        """Registers an ArgPreHook.

        Args:
            method_in: Method to register.

        Returns:
            Unchanged method, registered.
        """
        __arg_pre_hooks__[constructor][arg][lang] = method_in
        return method_in
    return set_arg_pre_hook


__arg_post_hooks__: DefaultDict[str, DefaultDict[str, Dict["Language", "i.ArgPostHook"]]] = defaultdict(lambda: defaultdict(dict))


def get_arg_post_hook(constructor: str, arg: str, lang: "Language") -> "i.ArgPostHook":
    """Retrieves an i.ArgPostHook for a given constructor-argument-language triple.

    Args:
        constructor: Constructor type to get an ArgPostHook for.
        arg: Argument name to get an ArgPostHook for.
        lang: Language to get an ArgPostHook for.

    Returns:
        Appropriate ArgPostHook.

    """
    return __arg_post_hooks__[constructor][arg][lang]


def set_arg_post_hook_registration(constructor: str, arg: str, lang: "Language") -> Callable[["i.ArgPostHook"], "i.ArgPostHook"]:
    """Maps a constructor-argument-language triple to an i.ArgPostHook.

    Args:
        constructor: Constructor type to register an ArgPostHook for.
        arg: Argument name to register an ArgPostHook for.
        lang: Language to register an ArgPostHook for.

    Returns:
        Function performing the actual registration.
    """
    def set_arg_post_hook(method_in: "i.ArgPostHook") -> "i.ArgPostHook":
        """Registers an ArgPostHook.

        Args:
            method_in: Method to register.

        Returns:
            Unchanged method, registered.
        """
        __arg_post_hooks__[constructor][arg][lang] = method_in
        return method_in
    return set_arg_post_hook


__pre_hooks__: DefaultDict[str, Dict["Language", "i.PreHook"]] = defaultdict(dict)


def get_pre_hook(constructor: str, lang: "Language") -> "i.PreHook":
    """Retrieves a i.PreHook for a given constructor-language pair.

    Args:
        constructor: Constructor type to find a pre-hook for.
        lang: Language to find a PreHook for.

    Returns:
        The appropriate PreHook.
    """
    return __pre_hooks__[constructor][lang]


def find_pre_hook(constructor_type: str, language_in: "Language") -> "i.PreHook":
    """Special case of get_pre_hook where the language falls back to mul (Q20923490).

    Args:
        constructor_type: Constructor type to find a pre-hook for.
        language_in: Language to find a PreHook for.

    Returns:
        The appropriate PreHook.

    Raises:
        KeyError: if a PreHook could not be found for the given language-type pair.
    """
    for language_to_try in get_language_fallbacks(language_in):
        try:
            return get_pre_hook(constructor_type, language_to_try)
        except KeyError:
            continue
    raise KeyError(f"No pre-hook for {constructor_type} in language {language_in}")


def set_pre_hook_registration(constructor: str, lang: "Language") -> Callable[["i.PreHook"], "i.PreHook"]:
    """Maps a constructor-language pair to a i.PreHook.

    Args:
        constructor: Constructor type to register a PreHook for.
        lang: Language to register a PreHook for.

    Returns:
        Function performing the actual registration.
    """
    def set_pre_hook(method_in: "i.PreHook") -> "i.PreHook":
        """Registers a PreHook.

        Args:
            method_in: Method to register.

        Returns:
            Unchanged method, registered.
        """
        __pre_hooks__[constructor][lang] = method_in
        return method_in
    return set_pre_hook


__post_hooks__: DefaultDict[str, Dict["Language", "i.PostHook"]] = defaultdict(dict)


def get_post_hook(constructor: str, lang: "Language") -> "i.PostHook":
    """Retrieves a i.PostHook for a given constructor-language pair.

    Args:
        constructor: Constructor type to find a post-hook for.
        lang: Language to find a PostHook for.

    Returns:
        The appropriate PostHook.
    """
    return __post_hooks__[constructor][lang]


def find_post_hook(constructor_type: str, language_in: "Language") -> "i.PostHook":
    """Special case of get_post_hook where the language falls back to mul (Q20923490).

    Args:
        constructor_type: Constructor type to find a post-hook for.
        language_in: Language to find a PostHook for.

    Returns:
        The appropriate PostHook.

    Raises:
        KeyError: if a PostHook could not be found for the given language-type pair.
    """
    for language_to_try in get_language_fallbacks(language_in):
        try:
            return get_post_hook(constructor_type, language_to_try)
        except KeyError:
            continue
    raise KeyError(f"No post-hook for {constructor_type} in language {language_in}")


def set_post_hook_registration(constructor: str, lang: "Language") -> Callable[["i.PostHook"], "i.PostHook"]:
    """Maps a constructor-language pair to a i.PostHook.

    Args:
        constructor: Constructor type to register a PostHook for.
        lang: Language to register a PostHook for.

    Returns:
        Function performing the actual registration.
    """
    def set_post_hook(method_in: "i.PostHook") -> "i.PostHook":
        """Registers a PostHook.

        Args:
            method_in: Method to register.

        Returns:
            Unchanged method, registered.
        """
        __post_hooks__[constructor][lang] = method_in
        return method_in
    return set_post_hook


__renderers__: DefaultDict[str, Dict["Language", "i.Renderer"]] = defaultdict(dict)


def get_renderer(constructor: str, lang: "Language") -> "i.Renderer":
    """Retrieves a i.Renderer for a given constructor-language pair.

    Args:
        constructor: Constructor type to find a Renderer for.
        lang: Language to find a Renderer for.

    Returns:
        The appropriate Renderer.
    """
    return __renderers__[constructor][lang]


def set_renderer_registration(constructor: str, lang: "Language") -> Callable[["i.Renderer"], "i.Renderer"]:
    """Maps a constructor-language pair to a i.Renderer.

    Args:
        constructor: Constructor type to register a Renderer for.
        lang: Language to register a Renderer for.

    Returns:
        Function performing the actual registration.
    """
    def set_renderer(method_in: "i.Renderer") -> "i.Renderer":
        """Registers a Renderer.

        Args:
            method_in: Method to register.

        Returns:
            Unchanged method, registered.
        """
        __renderers__[constructor][lang] = method_in
        return method_in
    return set_renderer


def find_main_renderer(constructor_type: str, language_in: "Language") -> "i.Renderer":
    """Special case of get_renderer where the language falls back to mul (Q20923490).

    Args:
        constructor_type: Constructor type to find a Renderer for.
        language_in: Language to find a Renderer for.

    Returns:
        The appropriate Renderer.

    Raises:
        KeyError: if a Renderer could not be found for the given language-type pair.
    """
    for language_to_try in get_language_fallbacks(language_in):
        try:
            return get_renderer(constructor_type, language_to_try)
        except KeyError:
            continue
    raise KeyError(f"No renderer for {constructor_type} in language {language_in}")


__syntactic_role_functions__: DefaultDict[str, Dict["Language", "i.SyntacticRoleFunction"]] = defaultdict(dict)


def get_syntactic_role_function(role: str, lang: "Language") -> "i.SyntacticRoleFunction":
    """Retrieves a i.SyntacticRoleFunction for a given language.

    Args:
        role: Semantic argument role to find a function for.
        lang: Language to find a SyntacticRoleFunction for.

    Returns:
        The appropriate SyntacticRoleFunction.
    """
    return __syntactic_role_functions__[role][lang]


def set_syntactic_role_function_registration(role: str, lang: "Language") -> Callable[["i.SyntacticRoleFunction"], "i.SyntacticRoleFunction"]:
    """Maps a syntactic role-language pair to a i.SyntacticRoleFunction.

    Args:
        role: Semantic argument role to register a SyntacticRoleFunction for.
        lang: Language to register a SyntacticRoleFunction for.

    Returns:
        Function performing the actual registration.
    """
    def set_syntactic_role_function(method_in: "i.SyntacticRoleFunction") -> "i.SyntacticRoleFunction":
        """Registers a SyntacticRoleFunction.

        Args:
            method_in: Method to register.

        Returns:
            Unchanged method, registered.
        """
        __syntactic_role_functions__[role][lang] = method_in
        return method_in
    return set_syntactic_role_function


def find_syntactic_role_function(role: str, language_in: "Language") -> "i.SyntacticRoleFunction":
    """Special case of get_syntactic_role_function where the language falls back to mul (Q20923490).

    Args:
        role: Semantic argument role to find a function for.
        language_in: Language to find a SyntacticRoleFunction for.

    Returns:
        The appropriate SyntacticRoleFunction.

    Raises:
        KeyError: if a SyntacticRoleFunction could not be found for the given language-role pair.
    """
    for language_to_try in get_language_fallbacks(language_in):
        try:
            return get_syntactic_role_function(role, language_to_try)
        except KeyError:
            continue
    raise KeyError(f"No syntactic role function for {role} in language {language_in}")


__sense_refines__: Dict["Language", "i.SenseRefinement"] = {}


def get_sense_refine(lang: "Language") -> "i.SenseRefinement":
    """Retrieves a i.SenseRefinement for a given language.

    Args:
        lang: Language to find a SenseRefinement for.

    Returns:
        The appropriate SenseRefinement.
    """
    return __sense_refines__[lang]


def set_sense_refine_registration(lang: "Language") -> Callable[["i.SenseRefinement"], "i.SenseRefinement"]:
    """Maps a language to a i.SenseRefinement.

    Args:
        lang: Language to register a SenseRefinement for.

    Returns:
        Function performing the actual registration.
    """
    def set_sense_refine(method_in: "i.SenseRefinement") -> "i.SenseRefinement":
        """Registers a SenseRefinement.

        Args:
            method_in: Method to register.

        Returns:
            Unchanged method, registered.
        """
        __sense_refines__[lang] = method_in
        return method_in
    return set_sense_refine


def find_sense_refine(language_in: "Language") -> "i.SenseRefinement":
    """Special case of get_syntactic_role_function where the language falls back to mul (Q20923490).

    Args:
        language_in: Language to find a SenseRefinement for.

    Returns:
        The appropriate SenseRefinement.

    Raises:
        KeyError: if a SenseRefinement could not be found for the given language.
    """
    for language_to_try in get_language_fallbacks(language_in):
        try:
            return get_sense_refine(language_to_try)
        except KeyError:
            continue
    raise KeyError(f"No sense refinement function in language {language_in}")


__initial_catena_processors__: Dict["Language", "i.InitialCatenaProcessor"] = {}


def get_initial_catena_processor(lang: "Language") -> "i.InitialCatenaProcessor":
    """Retrieves an i.InitialCatenaProcessor for a given language.

    Args:
        lang: Language to find an InitialCatenaProcessor for.

    Returns:
        The appropriate InitialCatenaProcessor.
    """
    return __initial_catena_processors__[lang]


def set_initial_catena_processor_registration(lang: "Language") -> Callable[["i.InitialCatenaProcessor"], "i.InitialCatenaProcessor"]:
    """Maps a language to an i.InitialCatenaProcessor.

    Args:
        lang: Language to register an InitialCatenaProcessor for.

    Returns:
        Function performing the actual registration.
    """
    def set_initial_catena_processor(method_in: "i.InitialCatenaProcessor") -> "i.InitialCatenaProcessor":
        """Registers an InitialCatenaProcessor.

        Args:
            method_in: Method to register.

        Returns:
            Unchanged method, registered.
        """
        __initial_catena_processors__[lang] = method_in
        return method_in
    return set_initial_catena_processor


def find_initial_catena_processor(language_in: "Language") -> "i.InitialCatenaProcessor":
    """Special case of get_initial_catena_processor where the language falls back to mul (Q20923490).

    Args:
        language_in: Language to find an InitialCatenaProcessor for.

    Returns:
        The appropriate InitialCatenaProcessor.

    Raises:
        KeyError: if an InitialCatenaProcessor could not be found for the given language.
    """
    for language_to_try in get_language_fallbacks(language_in):
        try:
            return get_initial_catena_processor(language_to_try)
        except KeyError:
            continue
    raise KeyError(f"No initial Catena processor for language {language_in}")


__thematic_inflection_additions__: Dict["Language", "i.ThematicInflectionAddition"] = {}


def get_thematic_inflection_addition(lang: "Language") -> "i.ThematicInflectionAddition":
    """Retrieves a i.ThematicInflectionAddition for a given language.

    Args:
        lang: Language to find a ThematicInflectionAddition for.

    Returns:
        The appropriate ThematicInflectionAddition.
    """
    return __thematic_inflection_additions__[lang]


def set_thematic_inflection_addition_registration(lang: "Language") -> Callable[["i.ThematicInflectionAddition"], "i.ThematicInflectionAddition"]:
    """Maps a language to a i.ThematicInflectionAddition.

    Args:
        lang: Language to register a ThematicInflectionAddition for.

    Returns:
        Function performing the actual registration.
    """
    def set_thematic_inflection_addition(method_in: "i.ThematicInflectionAddition") -> "i.ThematicInflectionAddition":
        """Registers a ThematicInflectionAddition.

        Args:
            method_in: Method to register.

        Returns:
            Unchanged method, registered.
        """
        __thematic_inflection_additions__[lang] = method_in
        return method_in
    return set_thematic_inflection_addition


def find_thematic_inflection_addition(language_in: "Language") -> "i.ThematicInflectionAddition":
    """Special case of get_thematic_inflection_addition where the language falls back to mul (Q20923490).

    Args:
        language_in: Language to find a ThematicInflectionAddition for.

    Returns:
        The appropriate ThematicInflectionAddition.

    Raises:
        KeyError: if a ThematicInflectionAddition could not be found for the given language.
    """
    for language_to_try in get_language_fallbacks(language_in):
        try:
            return get_thematic_inflection_addition(language_to_try)
        except KeyError:
            continue
    raise KeyError(f"No thematic inflection addition for language {language_in}")


__inflection_functions__: Dict["Language", "i.InflectionFunction"] = {}


def get_inflection_function(lang: "Language") -> "i.InflectionFunction":
    """Retrieves an i.InflectionFunction for a given language.

    Args:
        lang: Language to find an InflectionFunction for.

    Returns:
        The appropriate InflectionFunction.
    """
    return __inflection_functions__[lang]


def set_inflection_function_registration(lang: "Language") -> Callable[["i.InflectionFunction"], "i.InflectionFunction"]:
    """Maps a language to an i.InflectionFunction.

    Args:
        lang: Language to register an InflectionFunction for.

    Returns:
        Function performing the actual registration.
    """
    def set_inflection_function(method_in: "i.InflectionFunction") -> "i.InflectionFunction":
        """Registers an InflectionFunction.

        Args:
            method_in: Method to register.

        Returns:
            Unchanged method, registered.
        """
        __inflection_functions__[lang] = method_in
        return method_in
    return set_inflection_function


def find_inflection_function(language_in: "Language") -> "i.InflectionFunction":
    """Special case of get_inflection_function where the language falls back to mul (Q20923490).

    Args:
        language_in: Language to find an InflectionFunction for.

    Returns:
        The appropriate InflectionFunction.

    Raises:
        KeyError: if an InflectionFunction could not be found for the given language.
    """
    for language_to_try in get_language_fallbacks(language_in):
        try:
            return get_inflection_function(language_to_try)
        except KeyError:
            continue
    raise KeyError(f"No inflection function in language {language_in}")


__scope_output_processors__: Dict["Language", "i.ScopeOutputProcessor"] = {}


def get_scope_output_processor(lang: "Language") -> "i.ScopeOutputProcessor":
    """Retrieves a i.ScopeOutputProcessor for a given language.

    Args:
        lang: Language to find a ScopeOutputProcessor for.

    Returns:
        The appropriate ScopeOutputProcessor.
    """
    return __scope_output_processors__[lang]


def set_scope_output_processor_registration(lang: "Language") -> Callable[["i.ScopeOutputProcessor"], "i.ScopeOutputProcessor"]:
    """Maps a language to a i.ScopeOutputProcessor.

    Args:
        lang: Language to register a ScopeOutputProcessor for.

    Returns:
        Function performing the actual registration.
    """
    def set_scope_output_processor(method_in: "i.ScopeOutputProcessor") -> "i.ScopeOutputProcessor":
        """Registers a ScopeOutputProcessor.

        Args:
            method_in: Method to register.

        Returns:
            Unchanged method, registered.
        """
        __scope_output_processors__[lang] = method_in
        return method_in
    return set_scope_output_processor


def find_scope_output_processor(language_in: "Language") -> "i.ScopeOutputProcessor":
    """Special case of get_scope_output_processor where the language falls back to mul (Q20923490).

    Args:
        language_in: Language to find a ScopeOutputProcessor for.

    Returns:
        The appropriate ScopeOutputProcessor.

    Raises:
        KeyError: if a ScopeOutputProcessor could not be found for the given language.
    """
    for language_to_try in get_language_fallbacks(language_in):
        try:
            return get_scope_output_processor(language_to_try)
        except KeyError:
            continue
    raise KeyError(f"No scope output processor in language {language_in}")


__thematic_relationless_processors__: Dict["Language", "i.ThematicRelationlessProcessor"] = {}


def get_thematic_relationless_processor(lang: "Language") -> "i.ThematicRelationlessProcessor":
    """Retrieves a i.ThematicRelationlessProcessor for a given language.

    Args:
        lang: Language to find a ThematicRelationlessProcessor for.

    Returns:
        The appropriate ThematicRelationlessProcessor.
    """
    return __thematic_relationless_processors__[lang]


def set_thematic_relationless_processor_registration(lang: "Language") -> Callable[["i.ThematicRelationlessProcessor"], "i.ThematicRelationlessProcessor"]:
    """Maps a language to a i.ThematicRelationlessProcessor.

    Args:
        lang: Language to register a ThematicRelationlessProcessor for.

    Returns:
        Function performing the actual registration.
    """
    def set_thematic_relationless_processor(method_in: "i.ThematicRelationlessProcessor") -> "i.ThematicRelationlessProcessor":
        """Registers a ThematicRelationlessProcessor.

        Args:
            method_in: Method to register.

        Returns:
            Unchanged method, registered.
        """
        __thematic_relationless_processors__[lang] = method_in
        return method_in
    return set_thematic_relationless_processor


def find_thematic_relationless_processor(language_in: "Language") -> "i.ThematicRelationlessProcessor":
    """Special case of get_thematic_relationless_processor where the language falls back to mul (Q20923490).

    Args:
        language_in: Language to find a ThematicRelationlessProcessor for.

    Returns:
        The appropriate ThematicRelationlessProcessor.

    Raises:
        KeyError: if a ThematicRelationlessProcessor could not be found for the given language.
    """
    for language_to_try in get_language_fallbacks(language_in):
        try:
            return get_thematic_relationless_processor(language_to_try)
        except KeyError:
            continue
    raise KeyError(f"No thematic relationless processor in language {language_in}")


__constructorifies__: Dict["Language", "i.Constructorify"] = {}


def get_constructorify(lang: "Language") -> "i.Constructorify":
    """Retrieves a i.Constructorify for a given language.

    Args:
        lang: Language to find a Constructorify for.

    Returns:
        The appropriate Constructorify.
    """
    return __constructorifies__[lang]


def set_constructorify_registration(lang: "Language") -> Callable[["i.Constructorify"], "i.Constructorify"]:
    """Maps a language to a i.Constructorify.

    Args:
        lang: Language to register a Constructorify for.

    Returns:
        Function performing the actual registration.
    """
    def set_constructorify(method_in: "i.Constructorify") -> "i.Constructorify":
        """Registers a Constructorify.

        Args:
            method_in: Method to register.

        Returns:
            Unchanged method, registered.
        """
        __constructorifies__[lang] = method_in
        return method_in
    return set_constructorify


def find_constructorify(language_in: "Language") -> "i.Constructorify":
    """Special case of get_constructorify where the language falls back to mul (Q20923490).

    Args:
        language_in: Language to find a Constructorify for.

    Returns:
        The appropriate Constructorify.

    Raises:
        KeyError: if a Constructorify could not be found for the given language.
    """
    for language_to_try in get_language_fallbacks(language_in):
        try:
            return get_constructorify(language_to_try)
        except KeyError:
            continue
    raise KeyError(f"No constructorify in language {language_in}")


__transcribes__: Dict["Language", "i.Transcribe"] = {}


def get_transcribe(lang: "Language") -> "i.Transcribe":
    """Retrieves a i.Transcribe for a given language.

    Args:
        lang: Language to find a Transcribe for.

    Returns:
        The appropriate Transcribe.
    """
    return __transcribes__[lang]


def set_transcribe_registration(lang: "Language") -> Callable[["i.Transcribe"], "i.Transcribe"]:
    """Maps a language to a i.Transcribe.

    Args:
        lang: Language to register a Transcribe for.

    Returns:
        Function performing the actual registration.
    """
    def set_transcribe(method_in: "i.Transcribe") -> "i.Transcribe":
        """Registers a Transcribe.

        Args:
            method_in: Method to register.

        Returns:
            Unchanged method, registered.
        """
        __transcribes__[lang] = method_in
        return method_in
    return set_transcribe


def find_transcribe(language_in: "Language") -> "i.Transcribe":
    """Special case of get_transcribe where the language falls back to mul (Q20923490).

    Args:
        language_in: Language to find a Transcribe for.

    Returns:
        The appropriate Transcribe.

    Raises:
        KeyError: if a Transcribe could not be found for the given language.
    """
    for language_to_try in get_language_fallbacks(language_in):
        try:
            return get_transcribe(language_to_try)
        except KeyError:
            continue
    raise KeyError(f"No transcribe in language {language_in}")


__binominal_calques__: Dict["Language", "i.BinominalCompoundCalque"] = {}


def get_binominal_compound_calque(lang: "Language") -> "i.BinominalCompoundCalque":
    """Retrieves a i.BinominalCompoundCalque for a given language.

    Args:
        lang: Language to find a BinominalCompoundCalque for.

    Returns:
        The appropriate BinominalCompoundCalque.
    """
    return __binominal_calques__[lang]


def set_binominal_compound_calque_registration(lang: "Language") -> Callable[["i.BinominalCompoundCalque"], "i.BinominalCompoundCalque"]:
    """Maps a language to a i.BinominalCompoundCalque.

    Args:
        lang: Language to register a BinominalCompoundCalque for.

    Returns:
        Function performing the actual registration.
    """
    def set_binominal_compound_calque(method_in: "i.BinominalCompoundCalque") -> "i.BinominalCompoundCalque":
        """Registers a BinominalCompoundCalque.

        Args:
            method_in: Method to register.

        Returns:
            Unchanged method, registered.
        """
        __binominal_calques__[lang] = method_in
        return method_in
    return set_binominal_compound_calque


def find_binominal_compound_calque(language_in: "Language") -> "i.BinominalCompoundCalque":
    """Special case of get_binominal_compound_calque where the language falls back to mul (Q20923490).

    Args:
        language_in: Language to find a BinominalCompoundCalque for.

    Returns:
        The appropriate BinominalCompoundCalque.

    Raises:
        KeyError: if a BinominalCompoundCalque could not be found for the given language.
    """
    for language_to_try in get_language_fallbacks(language_in):
        try:
            return get_binominal_compound_calque(language_to_try)
        except KeyError:
            continue
    raise KeyError(f"No binominal compound calque in language {language_in}")


MappingName = Literal[
    "pre_hook", "arg_pre_hook", "arg_post_hook", "renderer", "post_hook",
    "sense_refine",
    "initial_catena_processor", "thematic_inflection_addition", "syntactic_role_function", "inflection_function", "scope_output_processor", "thematic_relationless_processor",
    "constructorify", "transcribe", "binominal_compound_calque",
]

MappingMethodP = ParamSpec("MappingMethodP")
MappingMethodT = TypeVar("MappingMethodT")

mapping_to_registration: Dict[str, Callable[..., Callable[..., Any]]] = {
    "pre_hook": set_pre_hook_registration,
    "arg_pre_hook": set_arg_pre_hook_registration,
    "arg_post_hook": set_arg_post_hook_registration,
    "renderer": set_renderer_registration,
    "post_hook": set_post_hook_registration,
    "sense_refine": set_sense_refine_registration,
    "initial_catena_processor": set_initial_catena_processor_registration,
    "thematic_inflection_addition": set_thematic_inflection_addition_registration,
    "syntactic_role_function": set_syntactic_role_function_registration,
    "inflection_function": set_inflection_function_registration,
    "scope_output_processor": set_scope_output_processor_registration,
    "thematic_relationless_processor": set_thematic_relationless_processor_registration,
    "constructorify": set_constructorify_registration,
    "transcribe": set_transcribe_registration,
    "binominal_compound_calque": set_binominal_compound_calque_registration,
}


def register(mapping_name: MappingName, *indices: Any) -> Callable[[Callable[MappingMethodP, MappingMethodT]], Callable[MappingMethodP, MappingMethodT]]:
    """Registers a method in a named mapping with some indices.

    * ⌨ Implementation detail: In Python, this function exists as a convenience measure.
        In Wikifunctions, since no function dispatch mechanism natively exists,
        the individual dictionaries that map from a set of parameters to a
        particular type of function will need to be filled manually.
    * ⌨ Implementation detail: This calls separately defined functions depending on whether the function being decorated is an
        i.ArgPreHook, i.ArgPostHook, PreHook, PostHook, i.Renderer, i.SyntacticRoleFunction,
        i.SenseRefinement, i.InitialCatenaProcessor, i.ThematicInflectionAddition,
        i.InflectionFunction, i.ScopeOutputProcessor, or i.ThematicRelationlessProcessor.

    Args:
        mapping_name: A name corresponding to the name of the Protocol that the function being decorated satisfies.
        indices: The specific indices which should be mapped to the function being decorated (varying by Protocol).

    Returns:
        The method being decorated, unchanged save for it being registered in the appropriate dictionary.
    """
    registration_function = mapping_to_registration[mapping_name]
    output_method = registration_function(*indices)
    return output_method
