"""Holds all renderers for Norwegian."""

import udiron.base.constants as c
import udiron.base.interfaces as udironi
import udiron.langs.norwegian.constants as cno
from tfsl import Language, langs
from udiron import CatenaZipper, FunctionConfig
from udiron.langs import import_language

import ninai.base.interfaces as i
from ninai import Context, RendererArguments, is_constructor_subtype
from ninai.graph import add_to_fallback_chain, calque, cognate, transcribe
from ninai.renderers import register
from ninai.renderers.mul import (add_full_stop_if_needed, get_listener_length,
                                 retrieve_sense)

add_to_fallback_chain(langs.nb_, "predicate", calque.calque)
add_to_fallback_chain(langs.nb_, "substantive", transcribe.transcribe)
add_to_fallback_chain(langs.nb_, "substantive", cognate.cognate)
add_to_fallback_chain(langs.nb_, "substantive", calque.calque)


norwegian, _ = import_language("norwegian")


@register("renderer", c.speaker, langs.nb_)
def speaker_renderer_nb(args: RendererArguments) -> i.RendererOutput:
    """Should return the pronoun 'jeg' or a synonym thereof.

    * ⓘ Q117978803 § 4.2.4.1 🗏 139

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.
    """
    return retrieve_sense(cno.jeg_pronoun, args), args.config


@register("renderer", c.listener, langs.nb_)
def listener_renderer_nb(args: RendererArguments) -> i.RendererOutput:
    """Returns the pronoun 'du' or 'dere' depending on the number of listeners.

    * ⓘ Q117978803 § 4.2.4.2 🗏 139

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.
    """
    if get_listener_length(args) > 1:
        return retrieve_sense(cno.dere_pronoun, args), args.config
    return retrieve_sense(cno.du_pronoun, args), args.config


@register("renderer", c.classificational_construction, langs.nb_)
def classificational_renderer_nb(args: RendererArguments) -> i.RendererOutput:
    """Should produce a classificational sentence.

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.
    """
    subject = args.core(c.subject)
    complement = args.core(c.class_item)

    article = retrieve_sense(cno.en_indefinite, args)
    complement = norwegian.make_indefinite_instance(complement, article, args.config)
    linking_verb = retrieve_sense(cno.vaere_quality, args)
    sentence = norwegian.attach_predicative_complement(subject, complement, linking_verb, args.config)
    sentence = process_scope_outputs(sentence, args.scope_args, args.config)
    inflected_sentence = norwegian.inflect_for_tense_aspect_mood(sentence, args.config)
    return inflected_sentence, args.config


@register("renderer", c.predpossessive_construction, langs.nb_)
def predpossessive_renderer_nb(args: RendererArguments) -> i.RendererOutput:
    """Should produce a predpossessive sentence.

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.
    """
    subject = args.core(c.possessor)
    complement = args.core(c.possessed)

    article = retrieve_sense(cno.en_indefinite, args)
    complement = norwegian.make_indefinite_instance(complement, article, args.config)
    linking_verb = retrieve_sense(cno.ha_possession, args)
    sentence = norwegian.attach_predicative_complement(subject, complement, linking_verb, args.config)
    sentence = process_scope_outputs(sentence, args.scope_args, args.config)
    inflected_sentence = norwegian.inflect_for_tense_aspect_mood(sentence, args.config)
    return inflected_sentence, args.config


@register("renderer", c.locative, langs.nb_)
def locative_renderer_nb(args: RendererArguments) -> i.RendererOutput:
    """Yields a phrase marked with the locative suffix.

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.
    """
    preposition = retrieve_sense(cno.i_in, args)
    result = norwegian.mark_with_preposition(args.core(c.complement), preposition, args.config)
    return result, args.config


@register("renderer", c.simultaneity, langs.nb_)
def contemporaneity_renderer_nb(args: RendererArguments) -> i.RendererOutput:
    """Prefixes a clause with 'så lenge'.

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.
    """
    saa_lenge = retrieve_sense(cno.saa_lenge_while, args).expand()
    result = norwegian.mark_with_conjunction(args.core(c.predicate), saa_lenge, args.config)
    return result, args.config


@register("renderer", "Q99632598", langs.nb_)
def succession_renderer_nb(args: RendererArguments) -> i.RendererOutput:
    """Prefixes a clause with 'efter'.

    * ⓘ Q117978803 § 9.5.1.1 🗏 385

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.
    """
    efter = retrieve_sense(cno.efter_after, args).expand()
    predicate = args.core(c.predicate)
    if len(predicate.get_inflections()) > 0 or (predicate.has_rel(c.copula) and len(predicate.rel(c.copula).get_inflections()) > 0) or (predicate.has_rel(c.auxiliary_verb) and len(predicate.rel(c.auxiliary_verb).get_inflections()) > 0):
        at_subordinator = retrieve_sense(cno.at_subordinator, args).expand()
        predicate = predicate.attach_leftmost(at_subordinator, c.subordinate_conjunction)
    result = norwegian.mark_with_conjunction(predicate, efter, args.config)
    return result, args.config


@register("renderer", "Q52063259", langs.nb_)
def precedence_renderer_nb(args: RendererArguments) -> i.RendererOutput:
    """Prefixes a clause with 'før'.

    * ⓘ Q117978803 § 9.5.1.4 🗏 386

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.
    """
    foer = retrieve_sense(cno.foer_before, args).expand()
    result = norwegian.mark_with_conjunction(args.core(c.predicate), foer, args.config)
    return result, args.config


@register("post_hook", c.predicate, langs.nb_)
@register("post_hook", c.classificational_construction, langs.nb_)
@register("post_hook", c.predpossessive_construction, langs.nb_)
def action_post_hook_nb(output: CatenaZipper, language: Language, context: Context, config: FunctionConfig) -> i.PostHookOutput:
    """Simply adds an ending punctuation mark.

    * ⓘ Q117978803 § 13 🗏 509

    Args:
        output: Original output.
        language: Language being rendered into.
        context: Context of this rendering step.
        config: Additional function configuration.

    Returns:
        Original output with punctuation added, and unmodified FunctionConfig.
    """
    return add_full_stop_if_needed(output, language, context, config), config


@register("initial_catena_processor", langs.nb_)
def icp_nb(verb_catena: CatenaZipper) -> CatenaZipper:
    """Performs some initial processing of the verb Catena.

    Args:
        verb_catena: Verb Catena to process.

    Returns:
        Processed verb Catena.
    """
    return verb_catena.expand()


@register("syntactic_role_function", c.subject, langs.nb_)
def srf_subj_nb(verb_catena: CatenaZipper, subject_catena: CatenaZipper, args: RendererArguments) -> CatenaZipper:
    """Attaches a subject.

    Args:
        verb_catena: Verb catena, possibly with other things attached to it.
        subject_catena: Subject to attach.
        args: Rest of the RendererArguments.

    Returns:
        Verb Catena with the subject attached to it.
    """
    verb_catena = norwegian.attach_subject_to_predicate(verb_catena, subject_catena, c.subject, FunctionConfig.new())
    return verb_catena


@register("syntactic_role_function", c.direct_object, langs.nb_)
def srf_obj_nb(verb_catena: CatenaZipper, object_catena: CatenaZipper, args: RendererArguments) -> CatenaZipper:
    """Attaches a direct object.

    Args:
        verb_catena: Verb catena, possibly with other things attached to it.
        object_catena: Direct object to attach.
        args: Rest of the RendererArguments.

    Returns:
        Verb Catena with the direct object attached to it.
    """
    verb_catena = norwegian.attach_object_to_predicate(verb_catena, object_catena, c.direct_object, FunctionConfig.new())
    return verb_catena


@register("syntactic_role_function", c.indirect_object, langs.nb_)
def srf_iobj_nb(verb_catena: CatenaZipper, object_catena: CatenaZipper, args: RendererArguments) -> CatenaZipper:
    """Attaches an indirect object.

    Args:
        verb_catena: Verb catena, possibly with other things attached to it.
        object_catena: Indirect object to attach.
        args: Rest of the RendererArguments.

    Returns:
        Verb Catena with the indirect object attached to it.
    """
    verb_catena = norwegian.attach_object_to_predicate(verb_catena, object_catena, c.indirect_object, FunctionConfig.new())
    return verb_catena


@register("inflection_function", langs.nb_)
def inflection_function_nb(input_catena: CatenaZipper, args: RendererArguments) -> CatenaZipper:
    """Simply calls inflect_for_tense_aspect_mood.

    Args:
        input_catena: Catena to be inflected.
        args: Rest of the RendererArguments.

    Returns:
        Inflected Catena.

    Todo:
        move following if block somewhere else
        make action-handling functions propagate args.language
    """
    if not input_catena.has_rel(c.subject):
        det_subject = retrieve_sense(cno.det_subject, args)
        input_catena = input_catena.attach_leftmost(det_subject, c.subject)
    return norwegian.inflect_for_tense_aspect_mood(input_catena, args.config)


def process_scope_outputs(base_catena: CatenaZipper, outputs: udironi.ScopeArgumentMapping, config: FunctionConfig) -> CatenaZipper:
    """Processes scope outputs, possibly attaching them to a base Catena.

    Args:
        base_catena: Catena to which other arguments are being attached.
        outputs: Mapping of scope argument types to lists of scope arguments.
        config: Additional function configuration.

    Returns:
        Input Catena with scope outputs added to it.

    Todo:
        Perhaps this should discard an output from the dict once it is attached to the catena?
    """
    for key in outputs:
        if is_constructor_subtype(key, c.spatial_relation) or is_constructor_subtype(key, c.subordination):
            for entry in outputs[key]:
                base_catena = norwegian.attach_adverbial_to_predicate(base_catena, entry, config)
    return base_catena


@register("scope_output_processor", langs.nb_)
def sop_nb(input_catena: CatenaZipper, scope_outputs: udironi.ScopeArgumentMapping, config: FunctionConfig) -> CatenaZipper:
    """Handles any arguments not covered by a syntactic role function.

    Args:
        input_catena: Catena to which other arguments are being attached.
        scope_outputs: Mapping of scope argument types to lists of scope arguments.
        config: Additional function configuration.

    Returns:
        Input Catena with scope outputs added to it.
    """
    return process_scope_outputs(input_catena, scope_outputs, config)
