"""Holds all renderers for Finnish."""

from tfsl import langs
from udiron.langs import import_language

from ninai.graph import add_to_fallback_chain, calque, cognate

import_language("fi")
add_to_fallback_chain(langs.fi_, "substantive", cognate.cognate)
add_to_fallback_chain(langs.fi_, "substantive", calque.calque)
