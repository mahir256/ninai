"""Holds all renderers for Hindustani."""

from tfsl import langs
from udiron.langs import import_language

from ninai.graph import add_to_fallback_chain, calque, cognate

import_language("hindustani")
add_to_fallback_chain(langs.hi_, "substantive", cognate.cognate)
add_to_fallback_chain(langs.hi_, "substantive", calque.calque)
