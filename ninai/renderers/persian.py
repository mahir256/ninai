"""Holds all renderers for Persian."""

from typing import TYPE_CHECKING

import tfsl.interfaces as tfsli
import udiron.base.constants as c
import udiron.langs.persian as fa
import udiron.langs.persian.constants as cfa
from tfsl import L, Language, langs
from udiron import C_, CatenaConfig, CatenaZipper, FunctionConfig
from udiron.base.utils import get_wanted_rep

import ninai.base.interfaces as i
from ninai.graph import add_to_fallback_chain, calque, cognate, transcribe
from ninai.graph.client import standalone_filtering
from ninai.renderers import register
from ninai.renderers.mul import (add_full_stop_if_needed, get_listener_length,
                                 retrieve_sense)

if TYPE_CHECKING:
    from ninai import Context, RendererArguments

add_to_fallback_chain(langs.new_persian_aran_, "substantive", transcribe.transcribe)
add_to_fallback_chain(langs.new_persian_aran_, "substantive", cognate.cognate)
add_to_fallback_chain(langs.new_persian_aran_, "substantive", calque.calque)


@register("sense_refine", langs.new_persian_aran_)
def sense_refine_fa(paths: i.CandidatePathSet, language_in: Language, catena_config: CatenaConfig, function_config: FunctionConfig) -> i.SenseRefinementOutput:
    """Checks for each sense whether there is a lemma in the sense's lexeme in the given language.

    Only those senses where this is true are passed on.

    Args:
        paths: Initial set of sense paths.
        language_in: Language being rendered into.
        catena_config: Initial configuration for the output Catena.
        function_config: Additional function configuration.

    Returns:
        Refined set of sense paths and any configuration that the output Catena needs to have.
    """
    revised_candidates = {}
    for sense, path in paths.items():
        try:
            get_wanted_rep(L(tfsli.get_lid_string(sense)), language_in)
        except KeyError:
            continue
        revised_candidates[sense] = path
    revised_candidates = standalone_filtering(revised_candidates, language_in, function_config)
    return revised_candidates, catena_config


@register("renderer", c.speaker, langs.new_persian_aran_)
def speaker_renderer_fa(args: "RendererArguments") -> i.RendererOutput:
    """Should return the pronoun 'man' or a synonym thereof.

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.
    """
    return retrieve_sense(cfa.man_i, args), args.config


@register("renderer", c.listener, langs.new_persian_aran_)
def listener_renderer_fa(args: "RendererArguments") -> i.RendererOutput:
    """Should return the pronoun 'tu' or a synonym thereof.

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.
    """
    if get_listener_length(args) > 1:
        return retrieve_sense(cfa.shumo_you, args), args.config
    return retrieve_sense(cfa.tu_thou, args), args.config


@register("renderer", c.classificational_construction, langs.new_persian_aran_)
def classificational_renderer_fa(args: "RendererArguments") -> i.RendererOutput:
    """Should produce a classificational sentence.

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.
    """
    subject = args.core(c.subject)
    attribute = args.core(c.class_item)
    copula = retrieve_sense(cfa.ast_copula, args)
    inflected_sentence = fa.build_copular_sentence(subject, attribute, copula, args.config)
    return inflected_sentence, args.config


@register("renderer", c.attributional_construction, langs.new_persian_aran_)
def attributional_renderer_fa(args: "RendererArguments") -> i.RendererOutput:
    """Should produce an attributional sentence.

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.
    """
    subject = args.core(c.subject)
    attribute = args.core(c.attributive)
    copula = retrieve_sense(cfa.ast_copula, args)
    inflected_sentence = fa.build_copular_sentence(subject, attribute, copula, args.config)
    return inflected_sentence, args.config


@register("renderer", c.possessive, langs.new_persian_aran_)
def possessive_renderer_fa(args: "RendererArguments") -> i.RendererOutput:
    """Should produce a possessive sentence.

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.
    """
    possessor = args.core(c.possessor)
    possessed = args.core(c.possessed)
    verb = retrieve_sense(cfa.doshtan_keep, args)

    inflected_sentence = verb.attach_leftmost(possessor, c.subject)
    inflected_sentence = inflected_sentence.attach_left_of_root(possessed, c.direct_object)
    inflected_sentence = fa.inflect_for_tense_aspect_mood(inflected_sentence, args.config)

    return inflected_sentence, args.config


@register("renderer", c.appertentive_construction, langs.new_persian_aran_)
def appertentive_renderer_fa(args: "RendererArguments") -> i.RendererOutput:
    """Should produce an appertentive sentence.

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.
    """
    possessor = args.core(c.possessor)
    possessed = args.core(c.possessed)
    copula = retrieve_sense(cfa.ast_copula, args)

    az = retrieve_sense(cfa.az_from, args)
    an = retrieve_sense("L1226214-S1", args)
    ezafe = retrieve_sense(cfa.e_ezafe, args).add_to_config(C_.compounding(c.backwards))
    ezafe = fa.compound_zwnj_handling(ezafe)
    new_possessor = possessor.attach_leftmost(ezafe, c.adposition)
    new_possessor = an.attach_right_of_root(new_possessor, c.nom_attribute)
    new_possessor = new_possessor.attach_left_of_root(az, c.adposition)

    inflected_sentence = fa.build_copular_sentence(possessed, new_possessor, copula, args.config)

    return inflected_sentence, args.config


@register("renderer", c.hyparctic_construction, langs.new_persian_aran_)
def hyparctic_renderer_fa(args: "RendererArguments") -> i.RendererOutput:
    """Should produce a hyparctic sentence.

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.
    """
    topic = args.core(c.topic)
    hast = retrieve_sense(cfa.hast_copula, args)

    inflected_sentence = topic.attach_right_of_root(hast, c.auxiliary_verb)
    inflected_sentence = fa.inflect_for_tense_aspect_mood(inflected_sentence, args.config)

    return inflected_sentence, args.config


@register("post_hook", c.predicate, langs.new_persian_aran_)
@register("post_hook", c.classificational_construction, langs.new_persian_aran_)
@register("post_hook", c.attributional_construction, langs.new_persian_aran_)
@register("post_hook", c.possessive, langs.new_persian_aran_)
@register("post_hook", c.appertentive_construction, langs.new_persian_aran_)
@register("post_hook", c.hyparctic_construction, langs.new_persian_aran_)
def action_post_hook_fa(output: CatenaZipper, language: Language, context: "Context", config: FunctionConfig) -> i.PostHookOutput:
    """Simply adds an ending punctuation mark.

    Args:
        output: Original output.
        language: Language being rendered into.
        context: Context of this rendering step.
        config: Additional function configuration.

    Returns:
        Original output with punctuation added, and unmodified FunctionConfig.
    """
    return add_full_stop_if_needed(output, language, context, config), config


@register("initial_catena_processor", langs.new_persian_aran_)
def icp_persian(verb_catena: CatenaZipper) -> CatenaZipper:
    """Performs some initial processing of the verb Catena.

    Args:
        verb_catena: Verb Catena to process.

    Returns:
        Processed verb Catena.
    """
    if verb_catena.haswbstatement(c.instance_of, c.verbo_nominal_syntagma):
        verb_catena = verb_catena.expand()
    return verb_catena


@register("syntactic_role_function", c.subject, langs.new_persian_aran_)
def srf_subj_persian(verb_catena: CatenaZipper, subject_catena: CatenaZipper, args: "RendererArguments") -> CatenaZipper:
    """Attaches a subject.

    Args:
        verb_catena: Verb catena, possibly with other things attached to it.
        subject_catena: Subject to attach.
        args: Rest of the RendererArguments.

    Returns:
        Verb Catena with the subject attached to it.
    """
    verb_catena = fa.attach_argument_to_predicate(verb_catena, subject_catena, c.subject, FunctionConfig.new())
    return verb_catena


@register("syntactic_role_function", c.direct_object, langs.new_persian_aran_)
def srf_obj_persian(verb_catena: CatenaZipper, object_catena: CatenaZipper, args: "RendererArguments") -> CatenaZipper:
    """Attaches a direct object after marking it with the accusative suffix.

    Args:
        verb_catena: Verb catena, possibly with other things attached to it.
        object_catena: Object to attach.
        args: Rest of the RendererArguments.

    Returns:
        Verb Catena with the object attached to it.
    """
    verb_catena = fa.attach_argument_to_predicate(verb_catena, object_catena, c.direct_object, FunctionConfig.new())
    return verb_catena


@register("inflection_function", langs.new_persian_aran_)
def inflection_function_persian(input_catena: CatenaZipper, args: "RendererArguments") -> CatenaZipper:
    """Simply calls inflect_for_tense_aspect_mood.

    Args:
        input_catena: Catena to be inflected.
        args: Rest of the RendererArguments.

    Returns:
        Inflected Catena.
    """
    return fa.inflect_for_tense_aspect_mood(input_catena, args.config)
