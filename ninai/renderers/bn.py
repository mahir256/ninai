"""Holds all renderers for Bengali."""

from tfsl import langs
from udiron.langs import import_language

from ninai.graph import add_to_fallback_chain, calque, cognate

import_language("bn")
add_to_fallback_chain(langs.bn_, "substantive", cognate.cognate)
add_to_fallback_chain(langs.bn_, "substantive", calque.calque)
