"""Holds renderers for various modifier constructors that may be applied to all languages."""

from datetime import datetime
from typing import TYPE_CHECKING, Dict, Optional, Protocol

import udiron.base.constants as c
from tfsl import Language, langs
from udiron import CatenaZipper
from udiron.base.functionconfig import (C_, EntityKey, FunctionConfig,
                                        ItemList, StrKey)
from udiron.langs import mul

import ninai.base.interfaces as i
from ninai.base.context import local_top_level, push_tracking
from ninai.graph.client import find_sense
from ninai.renderers import register

if TYPE_CHECKING:
    from ninai import Constructor, Context, RendererArguments
    from ninai.base.framing import Framing


def retrieve_sense(concept: str, args: "RendererArguments") -> CatenaZipper:
    """Retrieves the lexical equivalent for a provided concept.

    Args:
        concept: Concept to find an equivalent for.
        args: Current RendererArguments.

    Returns:
        Catena representing a (usually nominal) concept in the provided language, with information about the Constructor seeking it within.
    """
    found_sense = find_sense(concept, args.language, args.config)
    found_sense = push_tracking(found_sense, args.context)
    return found_sense


def typical_framing_handling(constructor_type: str, key_in: StrKey, val_in: str) -> None:
    """Defines and registers methods for a Constructor as follows:

    - On a pre-hook, adds a particular key-value pair to the FunctionConfig.
    - On rendering, if there are any arguments to the Constructor, then the first of these is returned,
      otherwise the provided FunctionConfig is returned with the added key-value pair.
    - On a post-hook, if the provided CatenaZipper is not empty, then the key-value pair is unset from the provided FunctionConfig,
      otherwise the provided CatenaZipper and FunctionConfig are passed unchanged.

    Args:
        constructor_type: Constructor type to register renderer and hooks for.
        key_in: FunctionConfig StrKey to set.
        val_in: Value to set for that StrKey.
    """
    @register("renderer", constructor_type, langs.mul_)
    def tfh_renderer_mul(args: "RendererArguments") -> i.RendererOutput:
        """Typical framing handling renderer.

        Args:
            args: Renderer arguments.

        Returns:
            CatenaZipper produced by the renderer and any modified configuration.
        """
        if args.scope_args:
            return next(v[0] for (k, v) in args.scope_args.items()), args.config
        return CatenaZipper([]), args.config.set(key_in, val_in)

    @register("pre_hook", constructor_type, langs.mul_)
    def tfh_pre_hook_mul(constructor: "Constructor", language: Language, context: "Context", framing: "Framing", config: FunctionConfig) -> i.PreHookOutput:
        """Simply sets an attribute in the provided FunctionConfig.

        Args:
            constructor: Not used here.
            language: Not used here.
            context: Not used here.
            framing: Not used here.
            config: Additional function configuration.

        Returns:
            Unchanged Constructor and Context but also modified FunctionConfig.
        """
        return constructor, context, config.set(key_in, val_in)

    @register("post_hook", constructor_type, langs.mul_)
    def tfh_post_hook_mul(output: CatenaZipper, language: Language, context: "Context", config: FunctionConfig) -> i.PostHookOutput:
        """Simply unsets the attribute set in the FunctionConfig by the pre-hook.

        Args:
            output: Not used here.
            language: Not used here.
            context: Not used here.
            config: Additional function configuration.

        Returns:
            Unchanged output and modified FunctionConfig.
        """
        if not output.empty():
            return output, config.unset(key_in)
        return output, config


@register("renderer", c.timestamp_computing, langs.mul_)
def configtimestamp_renderer_mul(args: "RendererArguments") -> i.RendererOutput:
    """Renderer for the ConfigTimestamp Constructor.

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.

    Raises:
        ValueError: if the first two arguments to the ConfigTimestamp are not both strings.
    """
    key = args.other_args[0]
    timestamp = args.other_args[1]
    if isinstance(key, str) and isinstance(timestamp, str):
        return CatenaZipper([]), args.config.set(EntityKey(key), datetime.fromisoformat(timestamp))
    raise ValueError("First two arguments to ConfigTimestamp not both strings")


@register("renderer", c.configuration_option, langs.mul_)
def configstr_renderer_mul(args: "RendererArguments") -> i.RendererOutput:
    """Renderer for the ConfigStr Constructor.

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.

    Raises:
        ValueError: if the first two arguments to the ConfigStr are not both strings.
    """
    key = args.other_args[0]
    value = args.other_args[1]
    if isinstance(key, str) and isinstance(value, str):
        return CatenaZipper([]), args.config.set(StrKey(key), value)
    raise ValueError("First two arguments to ConfigStr not both strings")


def get_listener_length(args: "RendererArguments") -> int:
    """Returns the number of listeners defined in a RendererArguments.

    Args:
        args: RendererArguments in question.

    Returns:
        Number of listeners in args.
    """
    try:
        listeners = args.framing.get_from_rc(c.listener)
    except KeyError:
        return 1
    if isinstance(listeners, ItemList):
        return listeners.length()
    return 1

# TODO: make overrides


class FinalPunctuationFunction(Protocol):
    """Function that adds final punctuation to a Catena."""

    def __call__(self, catena_in: CatenaZipper, config_in: Optional[FunctionConfig] = None) -> CatenaZipper:
        """Adds final punctuation to a Catena.

        Args:
            catena_in: Catena to be punctuated.
            config_in: Additional function configuration.

        Returns:
            Appropriately punctuated Catena.
        """


__full_stop_overrides__: Dict[Language, FinalPunctuationFunction] = {}


def set_full_stop_override(lang: Language, function: FinalPunctuationFunction) -> None:
    """Sets a final punctuation function.

    Args:
        lang: Language to set a final punctuation function for.
        function: The function in question.
    """
    __full_stop_overrides__[lang] = function


def add_full_stop_if_needed(output: CatenaZipper, language: Language, context: "Context", config_in: FunctionConfig) -> CatenaZipper:
    """Adds a full stop to a catena if the catena is the output from rendering a top-level constructor.

    Args:
        output: Current output from the rendering process.
        language: Language being rendered into.
        context: Current context of this rendering step.
        config_in: Additional function configuration.

    Returns:
        CatenaZipper reflecting the addition of end punctuation.
    """
    if local_top_level(context):
        if config_in.get(C_.emphasis) is not None:
            punctuation_function = mul.exclamation_mark
        else:
            try:
                punctuation_function = __full_stop_overrides__[language]
            except KeyError:
                punctuation_function = mul.full_stop
        output = punctuation_function(output, config_in)
    return output
