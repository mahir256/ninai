"""Holds functions for rendering Constructors in different languages.

This module also imports the full contents of __init__.py.
"""

import importlib
from typing import TYPE_CHECKING

from ninai.renderers.__mappings__ import (
    find_binominal_compound_calque,
    find_constructorify,
    find_inflection_function,
    find_initial_catena_processor,
    find_main_renderer,
    find_post_hook,
    find_pre_hook,
    find_scope_output_processor,
    find_sense_refine,
    find_syntactic_role_function,
    find_thematic_inflection_addition,
    find_thematic_relationless_processor,
    find_transcribe,
    get_arg_pre_hook,
    get_arg_post_hook,
    register,
)

if TYPE_CHECKING:
    from types import ModuleType


def import_language(name: str) -> "ModuleType":
    """Imports a language's renderers.

    Args:
        name: Name of the subfolder to draw these renderers from.

    Returns:
        Module for a language's renderers.

    Todo:
        see if __getattr__ needs some changes
    """
    importlib.import_module("ninai.constructors")
    renderers = importlib.import_module("." + name, __name__)
    return renderers


__all__ = [
    "find_binominal_compound_calque",
    "find_constructorify",
    "find_inflection_function",
    "find_initial_catena_processor",
    "find_main_renderer",
    "find_post_hook",
    "find_pre_hook",
    "find_scope_output_processor",
    "find_sense_refine",
    "find_syntactic_role_function",
    "find_thematic_inflection_addition",
    "find_thematic_relationless_processor",
    "find_transcribe",
    "get_arg_pre_hook",
    "get_arg_post_hook",
    "import_language",
    "register",
]
