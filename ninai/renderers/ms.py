"""Holds all renderers for Malay."""

from tfsl import langs
from udiron.langs import import_language

from ninai.graph import add_to_fallback_chain, calque, cognate

import_language("ms")
add_to_fallback_chain(langs.ms_, "substantive", cognate.cognate)
add_to_fallback_chain(langs.ms_, "substantive", calque.calque)
