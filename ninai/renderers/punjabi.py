"""Holds all renderers for Punjabi."""

from tfsl import langs
from udiron.langs import import_language

from ninai.graph import add_to_fallback_chain, calque, cognate

import_language("punjabi")
add_to_fallback_chain(langs.pa_, "substantive", cognate.cognate)
add_to_fallback_chain(langs.pa_, "substantive", calque.calque)
