"""Defines some Constructor types for spatial relations.

This set is likely to grow as more complex relationships
encoded by particular adpositions and other grammatical devices
in other languages are identified.
"""

import udiron.base.constants as c

from ninai import set_argument_filter
from ninai.base.argumentfilters import ArgumentFilterOptions

set_argument_filter(
    ArgumentFilterOptions(
        c.spatial_relation, [], [c.complement],
        """Base Constructor type for spatial relations. This should not be used directly.""",
    ),
)

set_argument_filter(
    ArgumentFilterOptions(
        c.locative, [c.spatial_relation], [],
        """Refers to being in/at (generically) a place.

        >>> Action("Q6537379",
        ...     Role(Speaker(), "Q392648"),
        ...     LocativeRel(Concept('Q22698')))
        ... # I walk in the park.
        """,
    ),
)

set_argument_filter(
    ArgumentFilterOptions(
        c.ablative, [c.spatial_relation], [],
        """Refers to moving from (generically) a place.

        >>> Action("Q6537379",
        ...     Role(Speaker(), "Q392648"),
        ...     AblativeRel(Concept('Q22698')))
        ... # I walk from the park.
        """,
    ),
)


set_argument_filter(
    ArgumentFilterOptions(
        c.allative, [c.spatial_relation], [],
        """Refers to moving to (generically) a place.

        >>> Action("Q6537379",
        ...     Role(Speaker(), "Q392648"),
        ...     AllativeRel(Concept('Q22698')))
        ... # I walk to the park.
        """,
    ),
)


set_argument_filter(
    ArgumentFilterOptions(
        c.inessive, [c.spatial_relation], [],
        """Refers to being inside a place.

        >>> Action("Q6537379",
        ...     Role(Speaker(), "Q392648"),
        ...     InessiveRel(Concept('Q22698')))
        ... # I walk inside the park.

        * ⓘ Q119239595 § 5.3.3.2.a 🗏 286
        """,
    ),
)
set_argument_filter(
    ArgumentFilterOptions(
        c.illative, [c.spatial_relation], [],
        """Refers to moving into a place.

        >>> Action("Q6537379",
        ...     Role(Speaker(), "Q392648"),
        ...     IllativeRel(Concept('Q22698')))
        ... # I walk into the park.

        * ⓘ Q119239595 § 5.3.3.2.a 🗏 286
        """,
    ),
)

set_argument_filter(
    ArgumentFilterOptions(
        c.inelative, [c.spatial_relation], [],
        """Refers to moving from inside a place.

        >>> Action("Q6537379",
        ...     Role(Speaker(), "Q392648"),
        ...     InelativeRel(Concept('Q22698')))
        ... # I walk from inside the park.
        """,
    ),
)
