"""Defines some Constructor types for particular constructions usually expressed with subordination."""

import udiron.base.constants as c

from ninai import set_argument_filter
from ninai.base.argumentfilters import ArgumentFilterOptions

set_argument_filter(
    ArgumentFilterOptions(
        c.subordination, [], [c.predicate],
        """Base Constructor type for subordinate relations. This should not be used directly.""",
    ),
)

set_argument_filter(
    ArgumentFilterOptions(
        c.simultaneity, [c.subordination], [],
        """Indicates that one statement is happening at the same time as another.

        >>> Attribution(Speaker(), Concept(Q(59863338)), Contemporaneity(...)) # I am large while ...
        """,
    ),
)

set_argument_filter(
    ArgumentFilterOptions(
        "Q99632598", [c.subordination], [],
        """Indicates that one statement is happening after another.

        >>> Attribution(Speaker(), Concept(Q(59863338)), Succession(...)) # I am large after ...
        """,
    ),
)

set_argument_filter(
    ArgumentFilterOptions(
        "Q52063259", [c.subordination], [],
        """Indicates that one statement is happening at the same time as another.

        >>> Attribution(Speaker(), Concept(Q(59863338)), Precedence(...)) # I am large before ...
        """,
    ),
)
