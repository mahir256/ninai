"""Holds the Setf Constructor and related functions.

(This is named after the Common Lisp function.)
"""

from typing import List

import udiron.base.constants as c
from udiron.base.functionconfig import EntityKey, StrKey

import ninai.base.interfaces as i
from ninai import Constructor, Framing, set_argument_filter
from ninai.base.argumentfilters import ArgumentFilterOptions
from ninai.base.framing import MethodKey


def setf_argument_filter(constructor_type: str, core_argument_list: List[str], *args: i.ConstructorArgument) -> Constructor:
    """Argument filter used only with Setf.

    Args:
        constructor_type: Name of the Constructor type.
        core_argument_list: List of names of core arguments.
        args: Actual arguments to the Constructor.

    Returns:
        Constructor for an identifier.
    """
    return Constructor(constructor_type, constructor_type + "_" + Constructor.next_id(), {}, {}, [], [args[0], args[1], args[2]])


set_argument_filter(
    ArgumentFilterOptions(
        c.system_configuration, [], [],
        """Sets a value in the containing RefContext's Framing.

        This is currently only valid as an argument to a RefContext.
        """,
        argument_filter=setf_argument_filter,
    ),
)


def process_setf(arg: Constructor, framing: Framing) -> Framing:
    """Processes a Setf command.

    Args:
        arg: The Setf command itself.
        framing: Current rendering Framing for this context.

    Returns:
        Appropriately modified Framing.

    Raises:
        ValueError: if the types of the Setf arguments are not expected.
    """
    target = arg.other_arguments[0]
    key = arg.other_arguments[1]
    value = arg.other_arguments[2]
    if not (isinstance(target, str) and isinstance(key, (StrKey, MethodKey, EntityKey))):
        raise ValueError("Unexpected types of arguments to Setf")

    framing = framing.add_attribute(target, key, value)
    return framing
