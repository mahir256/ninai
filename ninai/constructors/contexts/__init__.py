"""Holds all Constructors of direct relevance to RefContext."""

import importlib


for module in ["backref", "declaration", "refcontext", "setf", "setq"]:
    importlib.import_module(".".join(["ninai.constructors.contexts", module]))
