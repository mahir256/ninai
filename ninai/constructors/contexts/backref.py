"""Holds the Backref class."""

from typing import List

import udiron.base.constants as c
from tfsl import Item, Lexeme, langs
from udiron import zip_lexeme, CatenaZipper

import ninai.base.interfaces as i
from ninai import Constructor, RendererArguments, set_argument_filter
from ninai.base.argumentfilters import ArgumentFilterOptions
from ninai.base.context import ContextEntry
from ninai.graph import catena_from_item
from ninai.renderers import register


def backref_argument_filter(constructor_type: str, core_argument_list: List[str], *args: i.ConstructorArgument) -> Constructor:
    """Argument filter used only with Backref.

    Args:
        constructor_type: Name of the Constructor type.
        core_argument_list: List of names of core arguments.
        args: Actual arguments to the Constructor.

    Returns:
        Constructor for an identifier.

    Raises:
        ValueError: if the argument to Backref is not a string.
    """
    for arg in args:
        if isinstance(arg, str):
            return Constructor(constructor_type, arg, {}, {}, [], [arg])
    raise ValueError("no string found for Backref")


@register("renderer", c.backreference, langs.mul_)
def backref_renderer_mul(args: RendererArguments) -> i.RendererOutput:
    """Renderer of the Backref Constructor.

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.

    Raises:
        ValueError: if the identifier doesn't exist or if the object being referred to doesn't resolve to a CatenaZipper.

    Todo:
        move all of this to pre-hook?
    """
    language = args.language
    identifier = args.other_args[0]
    current_framing = args.framing

    if isinstance(identifier, str):
        target_object = current_framing.get_from_rc(identifier)
    else:
        raise ValueError("Identifier is not a string")

    if isinstance(target_object, Constructor):
        target_object_type, target_object_id, _, _, _, _ = target_object  # Constructor unpacked
        new_context = args.context.push(
            ContextEntry(
                c.backreference, "identifier", target_object_type, target_object_id, [],
            ),
        )
        renderer = target_object.run_renderer(language, new_context, current_framing, args.config)
        if renderer.paused_phase is None:
            return renderer.output, renderer.arguments.config
        raise ValueError(f"Rendering backreference constructor paused at {renderer.progress}")
    elif isinstance(target_object, Item):
        return catena_from_item(target_object, language, args.config), args.config
    elif isinstance(target_object, Lexeme):
        return zip_lexeme(target_object), args.config
    elif isinstance(target_object, CatenaZipper):
        return target_object, args.config
    raise ValueError(f"Backref reference of type {type(target_object)} not resolvable to CatenaZipper")


set_argument_filter(
    ArgumentFilterOptions(
        c.backreference, [], [],
        """Refers back to an object defined in the containing RefContext.

        As such, this is currently only valid as an argument to a RefContext.
        """,
        argument_filter=backref_argument_filter,
    ),
)
