"""Holds the Declaration Constructor and related functions."""

from typing import List

import udiron.base.constants as c

import ninai.base.interfaces as i
from ninai import Constructor, set_argument_filter
from ninai.base.argumentfilters import ArgumentFilterOptions


def declaration_argument_filter(constructor_type: str, core_argument_list: List[str], *args: i.ConstructorArgument) -> Constructor:
    """Argument filter used only with Declaration.

    Args:
        constructor_type: Name of the Constructor type.
        core_argument_list: List of names of core arguments.
        args: Actual arguments to the Constructor.

    Returns:
        Constructor for an identifier.
    """
    return Constructor(constructor_type, constructor_type + "_" + Constructor.next_id(), {}, {}, [], [args[0], args[1]])


set_argument_filter(
    ArgumentFilterOptions(
        c.declaration, [], [],
        """Assigns a new name to an object in the containing RefContext.

        This is currently only valid as an argument to a RefContext.
        """,
        argument_filter=declaration_argument_filter,
    ),
)


def process_declaration(arg: Constructor, references: i.RefContextObjectMapping) -> i.RefContextObjectMapping:
    """Processes a Declaration command.

    Args:
        arg: The Declaration command itself.
        references: Current set of context mappings of names to objects.

    Returns:
        RefContextObjectMapping with a new entry.

    Raises:
        ValueError: if the object name in the Declaration is not a string.
    """
    name = arg.other_arguments[0]
    initial_value = arg.other_arguments[1]
    if not isinstance(name, str):
        raise ValueError("Object name in Declaration is not a string")

    references[name] = initial_value
    return references
