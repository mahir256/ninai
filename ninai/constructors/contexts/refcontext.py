"""Holds the RefContext class and a number of other classes which make sense only when a RefContext is rendered."""

from datetime import datetime
from typing import TYPE_CHECKING, List, NamedTuple, Optional, Union

import tfsl.interfaces as tfsli
import udiron.base.constants as c
from tfsl import CoordinateValue, Item, Statement, langs
from udiron import CatenaZipper
from udiron.base.functionconfig import C_, ItemList

import ninai.base.interfaces as i
from ninai import Constructor, Framing, RendererArguments, set_argument_filter
from ninai.base.argumentfilters import ArgumentFilterOptions
from ninai.base.constructorinterfaces import is_constructor_subtype
from ninai.base.context import ContextEntry
from ninai.base.interfaces import NinaiError
from ninai.base.pausedrenderersupply import PausedRendererSupply
from ninai.constructors.contexts.declaration import process_declaration
from ninai.constructors.contexts.setf import process_setf
from ninai.constructors.contexts.setq import process_setq
from ninai.renderers import register

if TYPE_CHECKING:
    from ninai.base.constructorrenderer import ConstructorRenderer


def refcontext_argument_filter(constructor_type: str, core_argument_list: List[str], *args: i.ConstructorArgument) -> Constructor:
    """Argument filter for constructors, assuming they were not declared with a custom argument filter.

    Args:
        constructor_type: Name of the Constructor type.
        core_argument_list: List of names of core arguments.
        args: Actual arguments to the Constructor.

    Returns:
        Constructor for an identifier.
    """
    identifier: str = constructor_type + "_" + Constructor.next_id()
    scope_arguments: i.ScopeArguments = []
    core_arguments: i.CoreArguments = {}
    child_identifiers: i.ChildIdentifiersMapping = {}
    command_list: List[Union[Constructor, Framing]] = []

    for argument in args:
        if isinstance(argument, Constructor):
            if is_constructor_subtype(argument.constructor_type, "Identifier"):
                identifier = argument.identifier
            else:
                command_list.append(argument)
        elif isinstance(argument, Framing):
            command_list.append(argument)
    return Constructor(constructor_type, identifier, child_identifiers, core_arguments, scope_arguments, command_list)


def build_speaker() -> Item:
    """Constructs an Item representing the speaker of the utterance.

    Returns:
        Empty Item whose coordinates are those of the Wikimedia Foundation headquarters.
    """
    new_speaker = Item()
    new_speaker.statements += Statement(tfsli.Pid("P625"), CoordinateValue(37.78922, -122.40342, 0.00001))
    return new_speaker


def build_listener_list() -> ItemList:
    """Constructs a list with a single Item representing one listener of the utterance.

    Returns:
        List with an empty Item whose coordinates are those of the Wikimedia Foundation headquarters, slightly offset from that of build_speaker.
    """
    default_listener = Item()
    default_listener.statements += Statement(tfsli.Pid("P625"), CoordinateValue(37.78923, -122.40343, 0.00001))
    return ItemList([default_listener])


def set_up_basic_framing() -> Framing:
    """Sets a number of 'expected' attributes at the start of a RefContext.

    Returns:
        Framing reflecting these 'expected' attributes.
    """
    basic_framing = Framing.new()
    now = datetime.now()
    basic_framing = basic_framing.set(C_.now, now)
    basic_framing = basic_framing.set(C_.occurrence_time, now)
    basic_framing = basic_framing.set(C_.reference_time, now)
    return basic_framing


class RefContextRenderer(NamedTuple):
    """Encapsulator of methods that perform the main RefContext rendering step.

    Attributes:
        arguments: Arguments for this RefContext.
        references: Objects instantiated in this RefContext.
        command_index: List of commands to be run in this RefContext.
        statements: Rendered output Constructors.
        pause_cause: Exception from the RefContext renderer should it raise one.
    """
    arguments: RendererArguments
    references: Optional[i.RefContextObjectMapping] = None
    command_index: int = -1
    statements: CatenaZipper = CatenaZipper([])
    pause_cause: Optional["ConstructorRenderer"] = None

    def print_problem(self) -> str:
        """Prints the particular problem that caused a RefContextRenderer to pause.

        Returns:
            Human-readable version of the exception that caused rendering this RefContext to pause.
        """
        if self.pause_cause is None:
            return "No problems encountered"
        return self.pause_cause.print_problem()

    def clone(
        self,
        arguments: Optional[RendererArguments] = None,
        references: Optional[i.RefContextObjectMapping] = None,
        command_index: Optional[int] = None,
        statements: Optional[CatenaZipper] = None,
        pause_cause: Optional["ConstructorRenderer"] = None,
    ) -> "RefContextRenderer":
        """Clones a RefContextRenderer while making modifications to some of its fields.

        If most inputs to this function are omitted or None, then the corresponding fields of the RefContextRenderer
        are preserved in the output.
        The sole exception to this is 'pause_cause', which is always set to the (possibly default) value of this function.

        Args:
            arguments: Arguments for this RefContext.
            references: Objects instantiated in this RefContext.
            command_index: List of commands to be run in this RefContext.
            statements: Rendered output Constructors.
            pause_cause: Exception from the RefContext renderer should it raise one.

        Returns:
            Modified RefContextRenderer.
        """
        old_arguments, old_references, old_command_index, old_statements, _ = self
        return RefContextRenderer(
            old_arguments if arguments is None else arguments,
            old_references if references is None else references,
            old_command_index if command_index is None else command_index,
            old_statements if statements is None else statements,
            pause_cause,
        )

    def __call__(self, resume_supply: Optional[PausedRendererSupply]) -> "RefContextRenderer":
        """Renders the Constructors within the RefContext.

        Args:
            resume_supply: Information provided to the RefContextRenderer when resuming it.

        Returns:
            RefContextRenderer reflecting the outputs from rendering contained Constructors.

        Raises:
            TypeError: if a command is encountered that cannot be processed.
        """
        arguments, current_references, command_index, statements, _ = self
        if current_references is None:
            current_references = {
                c.speaker: build_speaker(),
                c.listener: build_listener_list(),
            }
        commands = arguments.other_args
        language = arguments.language
        basic_framing = arguments.framing
        _, current_config = PausedRendererSupply.handle_supply(None, arguments.config, resume_supply)

        for (idx, command) in enumerate(commands):
            if idx < command_index:
                continue
            if isinstance(command, Framing):
                basic_framing = basic_framing.merge(command)
                continue
            elif not isinstance(command, Constructor):
                raise TypeError(f"Can't process {type(command)} in RefContext")

            constructor_type, constructor_id, _, _, _, _ = command  # Constructor unpacked
            if is_constructor_subtype(constructor_type, c.declaration):
                current_references = process_declaration(command, current_references)
            elif is_constructor_subtype(constructor_type, c.name_binding):
                current_references = process_setq(command, current_references)
            elif is_constructor_subtype(constructor_type, c.system_configuration):
                basic_framing = process_setf(command, basic_framing)
            else:
                revised_framing = basic_framing.push_rcobjectmapping(current_references)
                new_context = arguments.context.push(
                    ContextEntry(
                        c.context, c.command, constructor_type, constructor_id, [],
                    ),
                )

                renderer = command.run_renderer(language, new_context, revised_framing, current_config)
                if renderer.paused_phase is not None:
                    return self.clone(
                        pause_cause=renderer, command_index=idx,
                        arguments=arguments.clone(framing=basic_framing),
                        references=current_references, statements=statements,
                    )
                else:
                    output = renderer.output

                if statements.empty():
                    statements = output
                else:
                    statements = statements.attach_rightmost(output, c.sentence)

        return self.clone(command_index=-1, references=current_references, statements=statements)


@register("renderer", c.context, langs.mul_)
def refcontext_renderer_mul(args: RendererArguments) -> i.RendererOutput:
    """Renders a RefContext.

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.

    Raises:
        NinaiError: if something pauses the RefContext rendering process.
        ValueError: if a resume supply cannot be retrieved despite a renderer being retrievable.
    """
    if isinstance(args.other_args[0], NinaiError):
        renderer = args.other_args[0].cause
        if isinstance(args.other_args[1], PausedRendererSupply):
            resume_supply = args.other_args[1]
        else:
            raise ValueError(f"Resume supply not retrievable even though {c.context} renderer retrieved")
    else:
        basic_framing = set_up_basic_framing()
        basic_framing = basic_framing.merge(args.framing)
        args = args.clone(framing=basic_framing)
        renderer = RefContextRenderer(args)
        resume_supply = None

    renderer = renderer(resume_supply)

    if renderer.pause_cause is not None:
        raise NinaiError(renderer.print_problem(), renderer)
    else:
        # TODO: export resultant RefContext references somehow
        return renderer.statements, renderer.arguments.config


set_argument_filter(
    ArgumentFilterOptions(
        c.context, [], [c.command],
        """Sets up an environment which is injected into contained Constructors at render time.""",
        argument_filter=refcontext_argument_filter, other_options=["config_insulator"],
    ),
)
