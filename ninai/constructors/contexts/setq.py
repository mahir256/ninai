"""Holds the Setq Constructor and related functions.

(This is named after the Common Lisp function.)
"""

import re
from typing import List, NamedTuple

import tfsl.interfaces as tfsli
import udiron.base.constants as c
from tfsl import Item, ItemValue, Lexeme, LexemeForm, LexemeSense, Statement
from tfsl.languages import get_first_lang

import ninai.base.constructorinterfaces as ci
import ninai.base.interfaces as i
from ninai import Constructor, set_argument_filter
from ninai.base.argumentfilters import ArgumentFilterOptions
from ninai.base.constructorzipper import zip_constructor

addlabel_regex = re.compile(r"L([a-z-]+)")


class QSCommands(NamedTuple):
    """List of commands in the format expected by quickstatements.toolforge.org.

    Attributes:
        commands: The actual list of commands.

    Todo:
        make this accessible from an import of ninai.constructors
    """
    commands: List[str]


def setq_argument_filter(constructor_type: str, core_argument_list: List[str], *args: i.ConstructorArgument) -> Constructor:
    """Argument filter used only with Setq.

    Args:
        constructor_type: Name of the Constructor type.
        core_argument_list: List of names of core arguments.
        args: Actual arguments to the Constructor.

    Returns:
        Constructor for an identifier.
    """
    return Constructor(constructor_type, constructor_type + "_" + Constructor.next_id(), {}, {}, [], [args[0], args[1]])


set_argument_filter(
    ArgumentFilterOptions(
        c.name_binding, [], [],
        """Performs an operation on an object in the containing RefContext.

        This is currently only valid as an argument to a RefContext.
        """,
        argument_filter=setq_argument_filter,
    ),
)


def process_setq_pid(references: i.RefContextObjectMapping, lhs: str, pid: tfsli.Pid, value: str) -> i.RefContextObjectMapping:
    """Processes a Setq command involving setting a property on a Wikibase entity.

    Args:
        references: Current set of context mappings of names to objects.
        lhs: Key into references for the entity to be modified.
        pid: Property to be added to the entity.
        value: Value of the statement using that property.

    Returns:
        RefContextObjectMapping reflecting a newly added statement on an entity within.

    Raises:
        TypeError: if trying to add a statement to a non-Wikibase entity.
    """
    lhs_object = references[lhs]
    if not isinstance(lhs_object, (Item, Lexeme, LexemeForm, LexemeSense)):
        raise TypeError("Attempting to add a statement to a non-Wikibase entity")

    if tfsli.is_entityid(value):
        lhs_object += Statement(pid, ItemValue(value))
    else:
        lhs_object += Statement(pid, value)

    references[lhs] = lhs_object
    return references


def process_setq_label(references: i.RefContextObjectMapping, lhs: str, text: str, langcode: str) -> i.RefContextObjectMapping:
    """Processes a Setq command involving setting a label on a Wikibase entity.

    Args:
        references: Current set of context mappings of names to objects.
        lhs: Key into references for the Item to be modified.
        text: Text of the label to be set.
        langcode: Language code of the label to be set.

    Returns:
        RefContextObjectMapping reflecting a newly set label on an Item within.

    Raises:
        TypeError: if a label is attempted to be added to a non-item.
    """
    lhs_object = references[lhs]
    if not isinstance(lhs_object, Item):
        raise TypeError("Attempting to add a label to a non-item")

    label = text @ get_first_lang(langcode)
    lhs_object = lhs_object.add_label(label)

    references[lhs] = lhs_object
    return references


def wrap_constructor(references: i.RefContextObjectMapping, lhs: str, target_identifier: str, wrapping: str) -> i.RefContextObjectMapping:
    """Wraps a constructor with the provided argument filter.

    Args:
        references: Current set of context mappings of names to objects.
        lhs: Key into references for the Constructor in which something will be wrapped.
        target_identifier: Identifier for the sub-Constructor to be wrapped.
        wrapping: Type of the Constructor whose argument filter will be applied.

    Returns:
        RefContextObjectMapping reflecting the Constructor wrapping.

    Raises:
        TypeError: if the Constructor is attempted to be wrapped with a non-Constructor.
    """
    lhs_object = references[lhs]
    if not isinstance(lhs_object, Constructor):
        raise TypeError("Attempting to wrap with a Constructor something that is not a Constructor")

    toplevel_constructor = zip_constructor(lhs_object)
    target_constructor = toplevel_constructor.find(target_identifier)

    wrapper = ci.__constructor_argument_filters__[wrapping]
    target_constructor = target_constructor.wrap(wrapper)

    arg, _ = target_constructor.root()
    references[lhs] = arg
    return references


def process_setq(arg: Constructor, references: i.RefContextObjectMapping) -> i.RefContextObjectMapping:
    """Processes a Setq command.

    Args:
        arg: The Setq command itself.
        references: Current set of context mappings of names to objects.

    Returns:
        RefContextObjectMapping with a modified entry.

    Raises:
        ValueError: if the types of the Setq arguments are not as expected.
    """
    lhs = arg.other_arguments[0]
    operands = arg.other_arguments[1]
    if not (isinstance(lhs, str) and isinstance(operands, QSCommands)):
        raise ValueError("Unexpected types of arguments to Setq")

    operation = operands.commands[0]
    if tfsli.is_pid(operation):
        value = operands.commands[1]
        return process_setq_pid(references, lhs, operation, value)
    elif langmatches := addlabel_regex.match(operation):
        text = operands.commands[1]
        langcode = langmatches.group(1)
        return process_setq_label(references, lhs, text, langcode)
    elif operation == "wrap":
        target_identifier, wrapping = operands[1:]
        return wrap_constructor(references, lhs, target_identifier, wrapping)
    return references
