"""Defines some Constructor types for cross-lingual pronominals."""

import udiron.base.constants as c

from ninai import set_argument_filter
from ninai.base.argumentfilters import ArgumentFilterOptions

set_argument_filter(
    ArgumentFilterOptions(
        c.speaker, [], [],
        """Refers to the speaker.

        ["Q124317331", ["Q2324479"], ["Q151885", "Q59863338"]] → I am large.
        """,
    ),
)

set_argument_filter(
    ArgumentFilterOptions(
        c.listener, [], [],
        """Refers to a listener.

        ["Q124317331", ["Q108606601"], ["Q151885", "Q59863338"]] → You are large.
        """,
    ),
)
