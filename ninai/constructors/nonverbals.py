"""Defines some Constructor types for non-verbal predicates."""

import udiron.base.constants as c

from ninai import set_argument_filter
from ninai.base.argumentfilters import ArgumentFilterOptions

set_argument_filter(
    ArgumentFilterOptions(
        c.duonominal_construction, [], [],
        """Constructor superclass for situations when two nominals are placed in correspondence. This should not be used directly.

        * ⓘ Q124316703 § 2 🗏 2
        """,
    ),
)

set_argument_filter(
    ArgumentFilterOptions(
        c.equational_construction, [c.duonominal_construction], [],
        """Directly equates two nominals.

        ["Q124316867", ["Q2324479"], ["Q151885", "Q5"]] → I am a human (and no one else is).

        * ⓘ Q124316709 § 5.1.2.1.2 🗏 80
        """,
    ),
)

set_argument_filter(
    ArgumentFilterOptions(
        c.classificational_construction, [c.duonominal_construction], [c.subject, c.class_item],
        """Places one nominal in a category represented by the other.

        ["Q124316841", ["Q2324479"], ["Q151885", "Q5"]] → I am a human [irrespective of whether others are].

        * ⓘ Q124316709 § 5.1.2.1.2 🗏 80
        """,
    ),
)

set_argument_filter(
    ArgumentFilterOptions(
        c.characterizational_construction, [c.equational_construction], [c.topic, c.focus],
        """Places a value in a category represented by a variable.

        ["Q124316841", ["Q2324479"], ["Q151885", "L1142241-S1"]] → # I am the devil's aunt.

        * ⓘ Q124316709 § 5.1.2.1.3 🗏 82
        """,
    ),
)

set_argument_filter(
    ArgumentFilterOptions(
        c.specificational_construction, [c.equational_construction], [c.focus, c.topic],
        """Assigns, to a category represented by a variable, a value.

         ["Q124316841", ["Q151885", "L1142241-S1"], ["Q2324479"]] → # The devil's aunt is me.

        * ⓘ Q124316709 § 5.1.2.1.3 🗏 82
        """,
    ),
)

set_argument_filter(
    ArgumentFilterOptions(
        c.attributional_construction, [], [c.subject, c.attributive],
        """Ascribes an attribute to a subject.

        ["Q124317331", ["Q2324479"], ["Q151885", "Q3142"]] → I am red.

        * ⓘ Q124316703 § 3 🗏 4
        """,
    ),
)

set_argument_filter(
    ArgumentFilterOptions(
        c.locative_construction, [], [c.subject, c.location],
        """Constructor superclass for situations in which the main information content is the existence of a subject. This should not be used directly.

        * ⓘ Q124316703 § 4 🗏 6 ‟ Such clauses are often simply called "locative clauses", but there is another type that also contains a locative phrase and a locatum but in which the locatum is not a definite subject: the existential construction.
        """,
    ),
)

set_argument_filter(
    ArgumentFilterOptions(
        c.predlocative_construction, [c.locative_construction], [],
        """Assigns some spatio-temporal relation to a subject.

        ["Q124317387", ["Q2324479"], ["Q202412", ["Q151885", "Q22698"]]] → I am in the park.

        * ⓘ Q124316709 § 5.1.3.2.1 🗏 94
        """,
    ),
)

set_argument_filter(
    ArgumentFilterOptions(
        c.existential_construction, [c.locative_construction], [],
        """Notes that a particular subject exists in a location.

        ["Q124317425", ["Q151885", "Q571"], ["Q202412", ["Q151885", "Q22698"]]] → There is a book in the park.

        * ⓘ Q124316709 § 5.1.3.2.2 🗏 96
        """,
    ),
)

set_argument_filter(
    ArgumentFilterOptions(
        c.possessive, [], [c.possessor, c.possessed],
        """Constructor superclass for situations in which the main information content is the possession of something by something else. This should not be used directly.

        * ⓘ Q124316703 § 5 🗏 7 ‟ Just as "locative" is not specific enough to designate predlocative clauses (because existential clauses are about location, too), "possessive” is not specific enough, because both appertentive and predpossessive clauses are about possession.
        """,
    ),
)

set_argument_filter(
    ArgumentFilterOptions(
        c.predpossessive_construction, [c.possessive], [],
        """Assigns possessor status to a subject and possessed status to an object.

        ["Q124317525", ["Q2324479"], ["Q151885", "Q22698"]] → I have a park.

        * ⓘ Q124316703 § 5 🗏 7
        """,
    ),
)

set_argument_filter(
    ArgumentFilterOptions(
        c.appertentive_construction, [c.possessive], [],
        """Notes that a particular object is possessed by a subject.

        ["Q124317525", ["Q151885", "Q22698"], ["Q2324479"]] → The park is mine.

        * ⓘ Q124316709 § 5.1.3.3 🗏 100
        """,
    ),
)

set_argument_filter(
    ArgumentFilterOptions(
        c.hyparctic_construction, [], [c.topic],
        """Notes that a particular subject exists (without regard to location).

        ["Q124317561", ["Q151885", "Q22698"]] → There is a park.

        * ⓘ Q124316703 § 12 🗏 19
        """,
    ),
)
