"""Definition of the Action constructor and separate handling for rendering one."""

from typing import TYPE_CHECKING, Tuple

import tfsl.interfaces as tfsli
import udiron.base.constants as c
import udiron.base.interfaces as udironi
from tfsl import L, ItemValue, Language, Statement, langs
from udiron.base.functionconfig import C_

import ninai.base.interfaces as i
import ninai.base.utility as u
import ninai.graph.client as graph
from ninai.base.argumentfilters import (ArgumentFilterOptions,
                                        passthrough_argument_filter,
                                        set_argument_filter)
from ninai.base.context import push_tracking
from ninai.renderers import (find_inflection_function,
                             find_initial_catena_processor,
                             find_scope_output_processor,
                             find_syntactic_role_function,
                             find_thematic_inflection_addition,
                             find_thematic_relationless_processor, register)

if TYPE_CHECKING:
    from udiron import CatenaZipper, FunctionConfig

    from ninai import Context
    from ninai.base.rendererarguments import RendererArguments


def get_thematic_relation_spec(predicate_catena: "CatenaZipper") -> i.ThematicRelationSpec:
    """Obtains the thematic relations specified on the sense of a particular predicate catena.

    Args:
        predicate_catena: Current predicate Catena.

    Returns:
        Mapping from semantic argument roles to the corresponding P9971 statements.
    """
    root_config = predicate_catena.get_config()
    if root_config.has(C_.thematic_relation):
        target_sense = root_config.get(C_.thematic_relation)
        if target_sense and tfsli.is_lsid(target_sense):
            thematic_relation_stmts = L(target_sense)[target_sense][c.has_thematic_relation]
        else:
            thematic_relation_stmts = predicate_catena.get_sense()[c.has_thematic_relation]
    else:
        thematic_relation_stmts = predicate_catena.get_sense()[c.has_thematic_relation]
    thematic_relation_spec: i.ThematicRelationSpec = {}
    for stmt in thematic_relation_stmts:
        thematic_role_item = stmt.value
        if thematic_role_item is False:
            thematic_relation_spec["None"] = stmt
        elif isinstance(thematic_role_item, ItemValue):
            thematic_role = thematic_role_item.id
            thematic_relation_spec[thematic_role] = stmt
    return thematic_relation_spec


def create_predicate_catena(argument: str, language: Language, context: "Context", config_in: "FunctionConfig") -> Tuple["CatenaZipper", i.ThematicRelationSpec]:
    """Creates a Catena based on the item/lexeme/sense passed in as the Action's predicate.

    Args:
        argument: Identifier for a concept.
        language: Language in which to find a lexeme for that concept.
        context: Current rendering context.
        config_in: Additional function configuration.

    Returns:
        Catena for a predicate and a mapping from roles to P9971 statements.
    """
    id_to_find = u.get_id_to_find(argument)

    predicate_catena = graph.find_predicate(id_to_find, language, config_in)
    predicate_catena = push_tracking(predicate_catena, context)

    return predicate_catena, get_thematic_relation_spec(predicate_catena)


def run_initial_catena_processor(predicate_catena: "CatenaZipper", current_language: Language) -> "CatenaZipper":
    """Takes the Catena for a given predicate and processes it before attaching anything else to it.

    This often involves splitting it into its components (via P5238),
    but other more complex operations may take place here as well.

    Args:
        predicate_catena: Current predicate catena.
        current_language: Language being rendered into.

    Returns:
        Initially processed Catena.
    """
    try:
        initial_catena_processor = find_initial_catena_processor(current_language)
    except KeyError:
        return predicate_catena
    return initial_catena_processor(predicate_catena)


def run_thematic_inflection_addition(current_catena: "CatenaZipper", current_language: Language, stmt: Statement) -> "CatenaZipper":
    """For a given thematic relation added as P9971 on a predicate, and given an argument with that thematic relation, adds the corresponding inflectional markings for the thematic relation to that argument.

    These markings often are just the P5713 values qualifying P9971, which are passed in as extra arguments,
    but other more complex operations may be applied here as well.

    Args:
        current_catena: Argument in question.
        current_language: Language in which rendering takes place.
        stmt: P9971 statement object for an argument.

    Returns:
        Inflected argument.
    """
    grammatical_feature_reqs = u.get_qualifier_values(stmt, c.requires_grammatical_feature)
    try:
        thematic_inflection_addition = find_thematic_inflection_addition(current_language)
    except KeyError:
        return current_catena.add_inflections(grammatical_feature_reqs)
    return thematic_inflection_addition(current_catena, grammatical_feature_reqs)


def run_syntactic_role_function(
    predicate_catena: "CatenaZipper", current_catena: "CatenaZipper",
    current_language: Language,
    args: "RendererArguments",
    stmt: Statement,
) -> "CatenaZipper":
    """Attaches an argument to a predicate with a particular syntactic role.

    Often it is enough to just perform the attachment without introducing anything else new,
    but other more complex operations may be applied here as well.

    Args:
        predicate_catena: Predicate in question.
        current_catena: Argument in question.
        current_language: Language in which rendering takes place.
        args: Renderer arguments.
        stmt: P9971 statement object for an argument.

    Returns:
        CatenaZipper with an argument possibly attached to it.

    Raises:
        ValueError: if the 'object has role' qualifier checked is novalue or somevalue.
    """
    syntactic_role_qualifier_value = stmt[c.object_has_role][0].value
    if not isinstance(syntactic_role_qualifier_value, ItemValue):
        raise ValueError("Invalid P3831 on thematic relation statement")

    syntactic_role = syntactic_role_qualifier_value.id
    try:
        syntactic_role_handler = find_syntactic_role_function(syntactic_role, current_language)
    except KeyError:
        return predicate_catena
    return syntactic_role_handler(predicate_catena, current_catena, args)


def run_inflection_function(predicate_catena: "CatenaZipper", args: "RendererArguments") -> "CatenaZipper":
    """Inflects the predicate according to the information provided in the framing and scope.

    Args:
        predicate_catena: Current predicate catena.
        args: Current arguments to the renderer.

    Returns:
        Inflected predicate Catena.
    """
    try:
        inflection_function = find_inflection_function(args.language)
    except KeyError:
        return predicate_catena
    return inflection_function(predicate_catena, args)


def run_scope_output_processor(
    predicate_catena: "CatenaZipper", current_language: Language,
    scope: udironi.ScopeArgumentMapping, config: "FunctionConfig",
) -> "CatenaZipper":
    """Adds any extra provided arguments to the predicate.

    Often this simply dispatches another method depending on the type of the extra argument,
    but other more complex operations may be applied here as well.

    Args:
        predicate_catena: Current predicate catena.
        current_language: Language being rendered into.
        scope: Current scope arguments.
        config: Additional function configuration.

    Returns:
        Predicate with scope arguments added.
    """
    try:
        scope_output_processor = find_scope_output_processor(current_language)
    except KeyError:
        return predicate_catena
    return scope_output_processor(predicate_catena, scope, config)


def run_thematic_relationless_processor(predicate_catena: "CatenaZipper", current_language: Language) -> "CatenaZipper":
    """Modifies a predicate in the event that there are explicitly no thematic relations for it (that is, specified on the sense as P9971 "no value").

    Args:
        predicate_catena: Current predicate catena.
        current_language: Language being rendered into.

    Returns:
        Predicate reflecting the non-presence of a semantic argument.
    """
    try:
        thematic_relationless_processor = find_thematic_relationless_processor(current_language)
    except KeyError:
        return predicate_catena
    return thematic_relationless_processor(predicate_catena)


def process_thematic_rel(current_language: Language, args: "RendererArguments", predicate_catena: "CatenaZipper", thematic_rel: str, stmt: Statement) -> "CatenaZipper":
    """For a given thematic relation statement, add the argument with that thematic relation to the predicate.

    Args:
        current_language: Language being rendered into.
        args: Current arguments to the renderer.
        predicate_catena: Current predicate catena.
        thematic_rel: Specific semantic argument role.
        stmt: Statement describing that semantic argument.

    Returns:
        Predicate reflecting the addition of (an indication of) a semantic argument.
    """
    if thematic_rel == "None":
        return run_thematic_relationless_processor(predicate_catena, current_language)

    try:
        current_catena: "CatenaZipper" = next(arg for arg in args.scope(c.thematic_relation) if arg.get_config().get(C_.thematic_relation) == thematic_rel)
    except (KeyError, IndexError, AttributeError, StopIteration):
        return predicate_catena

    current_catena = run_thematic_inflection_addition(current_catena, current_language, stmt)

    predicate_catena = run_syntactic_role_function(predicate_catena, current_catena, current_language, args, stmt)

    return predicate_catena


def get_predicate_string(args: "RendererArguments") -> str:
    """Returns a string representing the predicate from the provided RendererArguments.

    Args:
        args: Renderer arguments.

    Returns:
        Aforementioned string.

    Raises:
        ValueError: if the provided Action predicate is not a string.
    """
    predicate_string = args.other_args[0]
    if not isinstance(predicate_string, str):
        raise ValueError("Provided Action predicate is not a string")
    return predicate_string


@register("renderer", c.predicate, langs.mul_)
def action_renderer_mul(args: "RendererArguments") -> i.RendererOutput:
    """Renderer of the Action constructor.

    At the moment the only qualifiers on 'has thematic relation' that are examined are
    'requires grammatical feature' and 'object has role', and even with the latter only
    the first value is taken.

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.
    """
    current_language = args.language

    predicate_string = get_predicate_string(args)

    predicate_catena, thematic_relations = create_predicate_catena(predicate_string, current_language, args.context, args.config)

    predicate_catena = run_initial_catena_processor(predicate_catena, current_language)

    for thematic_rel, stmt in thematic_relations.items():
        predicate_catena = process_thematic_rel(current_language, args, predicate_catena, thematic_rel, stmt)

    predicate_catena = run_inflection_function(predicate_catena, args)

    predicate_catena = run_scope_output_processor(predicate_catena, current_language, args.scope_args, args.config)

    return predicate_catena, args.config


set_argument_filter(
    ArgumentFilterOptions(
        c.predicate, [], [],
        """Represents a predicate, either directly or indirectly specified.

        ["Q179080", "L741213-S4", ["Q613930", ["Q2324479"], "Q1242505"], ["Q613930", ["Q151885", "Q35831"], "Q109566760"]] → I enjoy sunshine.

        The predicate can be directly specified, if a lexeme sense is provided as the first argument,
        or indirectly specified, if an item is provided and a sense is linked via P9970 to it.
        """,
        argument_filter=passthrough_argument_filter, other_options=["unfiltered_framing"],
    ),
)

set_argument_filter(
    ArgumentFilterOptions(
        c.thematic_relation, [], ["entity"],
        """Represents a role played by an entity with respect to a predicate.

        Note that the first argument must be the entity
        and the second argument must be the role.
        """,
    ),
)


@register("renderer", c.thematic_relation, langs.mul_)
def role_renderer_mul(args: "RendererArguments") -> i.RendererOutput:
    """Returns the entity with an entry in its Catena configuration specifying that it fills a particular role.

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.

    Raises:
        ValueError: if the semantic argument specified is not an item ID.
    """
    role = args.other_args[0]
    if isinstance(role, str):
        modified_entity = args.core("entity").add_to_config(C_.thematic_relation(role))
        return modified_entity, args.config
    raise ValueError("Semantic argument specified is not an item ID")
