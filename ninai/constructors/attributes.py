"""Defines some Constructor types for particular constructions usually expressed as adverbials."""

import udiron.base.constants as c

from ninai import set_argument_filter
from ninai.base.argumentfilters import ArgumentFilterOptions

set_argument_filter(
    ArgumentFilterOptions(
        c.adverbial, [], [c.predicate],
        """Base Constructor type for adverbial relations. This should not be used directly.""",
    ),
)
