"""Holds nearly all constructors, except for `Identifier and InflectionSignal <ninai.base.argumentfilters>`_"""

import importlib


importlib.import_module("ninai.base.argumentfilters")
for module in ["action", "attributes", "concept", "configurations", "contexts", "nominals", "nonverbals", "pronominals", "spatials", "subordinates"]:
    importlib.import_module(".".join(["ninai.constructors", module]))
