"""Defines some Constructor types for cross-lingual pronominals."""

import udiron.base.constants as c

from ninai import set_argument_filter
from ninai.base.argumentfilters import ArgumentFilterOptions

set_argument_filter(
    ArgumentFilterOptions(
        c.timestamp_computing, [], [],
        """Sets a value in the containing Constructor's FunctionConfig as a timestamp.

        >>> Attribution(Speaker(), Concept(Q(59863338)),
        ...   ConfigTimestamp("Q7805404", "1809-02-12"))
        ... # I am large [on February 12, 1809].
        """,
    ),
)

set_argument_filter(
    ArgumentFilterOptions(
        c.configuration_option, [], [],
        """Sets a value in the containing Constructor's FunctionConfig as a string.

        >>> Attribution(
        ...   Speaker(ConfigStr("Q690940", "Q146786")),
        ...   Concept(Q(59863338)))
        ... # I [with an indication to treat that word as plural] am large.
        """,
    ),
)
