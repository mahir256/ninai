"""Basic definition of the Concept.

Most functionality of interest to the handling of Concepts resides in ninai.graph.server and ninai.graph.client.
"""

import logging
from typing import TYPE_CHECKING

import udiron.base.constants as c
from tfsl import langs
from udiron import C_

import ninai.base.interfaces as i
import ninai.base.utility as u
import ninai.graph.client as graph
from ninai.base.argumentfilters import (ArgumentFilterOptions,
                                        passthrough_argument_filter,
                                        set_argument_filter)
from ninai.base.context import push_tracking
from ninai.renderers import register

if TYPE_CHECKING:
    from ninai.base.rendererarguments import RendererArguments

logger = logging.getLogger(__name__)


@register("renderer", c.concept, langs.mul_)
def concept_renderer_mul(args: "RendererArguments") -> i.RendererOutput:
    """Provides a Catena representing an entity in a particular language.

    Args:
        args: Renderer arguments.

    Returns:
        CatenaZipper produced by the renderer and any modified configuration.
    """
    try:
        attribution_override = args.scope("Attribution")[0]
        return attribution_override, args.config
    except (AttributeError, IndexError):
        pass

    entity_id = args.other_args[0]
    id_to_find = u.get_id_to_find(entity_id)

    out = graph.find_sense(id_to_find, args.language, args.config)
    out = push_tracking(out, args.context)

    logger.info("Start tree for %s", entity_id)
    for catena in out.catenae():
        if (shortest_path_tree := catena.get_config().get(C_.shortest_path_tree)) is not None:
            logger.info(shortest_path_tree.strip())
    logger.info("End tree for %s", entity_id)

    return out, args.config


set_argument_filter(
    ArgumentFilterOptions(
        c.concept, [], [],
        """Represents a nominal or verbal linked to the provided Wikidata item or lexeme sense.""",
        argument_filter=passthrough_argument_filter, other_options=["unfiltered_framing"],
    ),
)
