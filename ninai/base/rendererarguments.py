"""Holds the RendererArguments class.

Each Constructor's main Renderer is provided a RendererArguments as its only argument.
The first three parts are generally filled as different steps in the Constructor rendering process
(such as the pre-hook and core and scope argument rendering)
produce outputs that must be used in the main Renderer.
"""

from typing import TYPE_CHECKING, NamedTuple, Optional, Sequence

import udiron.base.interfaces as udironi
from udiron import CatenaConfig, CatenaZipper, FunctionConfig

import ninai.base.interfaces as i
import ninai.base.utility as u

if TYPE_CHECKING:
    from tfsl import Language

    from ninai.base.context import Context
    from ninai.base.framing import Framing


class RendererArguments(NamedTuple):
    """Holds the outputs from rendering core and scope arguments, as well as the language into which things are rendered, the current framing (for those renderers that use it), and a mapping of strings to non-Catena objects.

    Attributes:
        args: Core argument mapping.
        scope_args: Scope argument mapping.
        other_args: List of other arguments.
        language: Language of the Constructor being rendered.
        context: The Context in which to render the Constructor.
        framing: Framing to be used with the Constructor being rendered.
        config: The specific function configuration applicable to rendering the Constructor.
    """
    args: udironi.CoreArgumentMapping
    scope_args: udironi.ScopeArgumentMapping
    other_args: i.ConstructorArguments
    language: "Language"
    context: "Context"
    framing: "Framing"
    config: FunctionConfig

    def clone(
        self,
        args: Optional[udironi.CoreArgumentMapping] = None,
        scope_args: Optional[udironi.ScopeArgumentMapping] = None,
        other_args: Optional[i.ConstructorArguments] = None,
        language: Optional["Language"] = None,
        context: Optional["Context"] = None,
        framing: Optional["Framing"] = None,
        config: Optional[FunctionConfig] = None,
    ) -> "RendererArguments":
        """Clones a RendererArguments while making modifications to some of its fields.

        If any input to this function is omitted or None, then the corresponding field of the ConstructorRenderer
        is preserved in the output.

        Args:
            args: Core argument mapping.
            scope_args: Scope argument mapping.
            other_args: List of other arguments.
            language: Language of the Constructor being rendered.
            context: The Context in which to render the Constructor.
            framing: Framing to be used with the Constructor being rendered.
            config: The specific function configuration applicable to rendering the Constructor.

        Returns:
            Modified RendererArguments.
        """
        old_args, old_scope_args, old_other_args, old_language, old_context, old_framing, old_config = self
        return RendererArguments(
            old_args if args is None else args,
            old_scope_args if scope_args is None else scope_args,
            old_other_args if other_args is None else other_args,
            old_language if language is None else language,
            old_context if context is None else context,
            old_framing if framing is None else framing,
            old_config if config is None else config,
        )

    def push(self, key: str, value: CatenaZipper) -> "RendererArguments":
        """Adds a rendered core argument to the core argument mapping.

        Args:
            key: The name to which the provided rendered core argument is to be assigned.
            value: The rendered core argument in question.

        Returns:
            RendererArguments with the provided rendered core argument added.
        """
        new_argument_dict = self.args.copy()
        new_argument_dict.update({key: value})
        return self.clone(args=new_argument_dict)

    def push_scope(self, argument_name: str, argument_catena: CatenaZipper) -> "RendererArguments":
        """Adds a rendered scope argument to the scope argument mapping.

        Args:
            argument_name: The name to which the provided rendered scope argument is to be assigned.
            argument_catena: The rendered scope argument in question.

        Returns:
            RendererArguments with the provided rendered scope argument added.
        """
        new_scope_dict: udironi.ScopeArgumentMapping = {**self.scope_args}
        if argument_name in new_scope_dict:
            new_scope_dict[argument_name].append(argument_catena)
        else:
            new_scope_dict = {**new_scope_dict, **{argument_name: [argument_catena]}}
        return self.clone(scope_args=new_scope_dict)

    def push_other(self, value: i.ConstructorArgument) -> "RendererArguments":
        """Adds an other argument to the list of other arguments.

        Args:
            value: Other argument.

        Returns:
            RendererArguments with the provided other argument added.
        """
        return self.clone(other_args=[*self.other_args, value])

    def core(self, attr: str) -> CatenaZipper:
        """Retrieves a core argument based on the provided string key.

        Args:
            attr: The specific key to which the core argument was assigned.

        Returns:
            Core argument assigned to the provided key.

        Raises:
            AttributeError: if there is no core argument to which the given key maps.
        """
        try:
            return self.args[attr]
        except KeyError as exception:
            raise AttributeError(f"No core argument is mapped to key {attr}") from exception

    def scope(self, attr: str) -> Sequence[CatenaZipper]:
        """Retrieves a scope argument based on the provided string key.

        Args:
            attr: The specific key to which the scope argument was assigned.

        Returns:
            Scope argument assigned to the provided key.

        Raises:
            AttributeError: if there is no scope argument to which the given key maps.
        """
        try:
            return self.scope_args[attr]
        except KeyError as exception:
            raise AttributeError(f"No scope argument is mapped to key {attr}") from exception

    def merge_scope_dicts(self) -> "RendererArguments":
        """Merges the scope mappings of any core arguments' CatenaConfigs (if they exist) into this RendererArguments's scope mapping.

        Returns:
            RendererArguments with scope arguments obtained from any core arguments' CatenaConfigs.
        """
        resultant_arguments = self.args.copy()
        resultant_dict = self.scope_args
        dicts_to_merge = []
        for (arg_name, catena) in self.args.items():
            current_config, current_scope_args, current_tracking = catena.get_config()
            if current_scope_args is not None:
                dicts_to_merge.append(current_scope_args)
                new_config = CatenaConfig.new(current_config, {}, current_tracking)
                resultant_arguments[arg_name] = catena.modify(config=new_config)
        resultant_dict = u.cat_argument_dicts(resultant_dict, *dicts_to_merge)
        return self.clone(args=resultant_arguments, scope_args=resultant_dict)
