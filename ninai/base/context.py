"""Holder for the Context class."""

from typing import List, NamedTuple, Optional, Tuple

import udiron.base.constants as c
from udiron import CatenaTrackingEntry, CatenaZipper

import ninai.base.constructorinterfaces as ci


class ContextEntry(NamedTuple):
    """An entry in a Context.

    Attributes:
        parent_constructor: The type of the parent of the current constructor being rendered.
        parent_role: The name of an argument to the parent constructor, or None if the current_constructor is a scope argument.
        current_constructor: The type of the current constructor being rendered.
        current_identifier: The identifier of the current constructor being rendered.
        current_classes: A (possibly empty) list of classes attached to the constructor.
    """
    parent_constructor: Optional[str]
    parent_role: Optional[str]
    current_constructor: str
    current_identifier: str
    current_classes: List[str]

    def __repr__(self) -> str:
        """Produces string representation of the ContextEntry.

        Returns:
            String representation of the ContextEntry.
        """
        return f"({self.parent_constructor}:{self.parent_role} > {self.current_constructor}#{self.current_identifier}{'.'.join(self.current_classes)})"

    def to_catenatrackingentry(self) -> CatenaTrackingEntry:
        """Converts a Context into a CatenaTrackingEntry.

        Returns:
            CatenaTrackingEntry with the current constructor type and identifier therein.
        """
        return CatenaTrackingEntry(self.current_constructor, self.current_identifier)


class Context(NamedTuple):
    """Tracks the current position of the constructor being rendered relative to the root.

    Sort of like a call stack.

    Attributes:
        context: The actual list of ContextEntries.
    """
    context: List[ContextEntry]

    @classmethod
    def start_new(
        cls,
        parent_constructor: Optional[str],
        parent_role: Optional[str],
        current_constructor: str,
        current_identifier: str,
        current_classes: List[str],
    ) -> "Context":
        """Builds a single-entry Context with the arguments provided.

        Args:
            parent_constructor: Type of the parent Constructor.
            parent_role: Role of the current Constructor with respect to its parent.
            current_constructor: Type of the current Constructor.
            current_identifier: Identifier of the current Constructor.
            current_classes: Classes of the current Constructor.

        Returns:
            Context with a single ContextEntry (whose fields are filled by the argument to this function).
        """
        return Context([
            ContextEntry(
                parent_constructor, parent_role, current_constructor, current_identifier, current_classes,
            ),
        ])

    def push(self, entry: ContextEntry) -> "Context":
        """Adds an entry to the Context.

        Args:
            entry: Entry to add to the Context.

        Returns:
            Context with the new entry as the last element.
        """
        return Context(self.context + [entry])

    def pop(self) -> Tuple[ContextEntry, "Context"]:
        """Removes the most recent entry from the Context and returns it and the remainder.

        Returns:
            The last ContextEntry in the Context, and the Context with said ContextEntry removed.

        Raises:
            IndexError: if the Context is empty.
        """
        if self.context:
            return self.context[-1], Context(self.context[:-1])
        raise IndexError("Context is empty")

    def is_empty(self) -> bool:
        """Checks for a nonzero length of the Context.

        Returns:
            Whether the Context is empty.
        """
        return not self.context

    def top(self) -> ContextEntry:
        """Shortcut to the last entry in the Context.

        Returns:
            Last ContextEntry in the Context.
        """
        return self.context[-1]

    def __repr__(self) -> str:
        """Produces string representation of the Context.

        Returns:
            String representation of the Context.
        """
        return " >> ".join(str(entry) for entry in self.context)


def refcontext_top_level(context: Context) -> bool:
    """Checks the top-level status of the given Context.

    Args:
        context: Context to check.

    Returns:
        Whether the context passed in represents a top-level command in a RefContext.
    """
    try:
        context_top, rest_of_context = context.pop()
    except IndexError:
        return True
    parent_type, parent_argument_name, _, _, _ = context_top
    is_refcontext_command = parent_type == c.context and parent_argument_name == c.command
    is_backref_which_is_refcontext_command = False
    try:
        context_second, _ = rest_of_context.pop()
        second_parent_type, second_parent_argument_name, _, _, _ = context_second
        is_backref_which_is_refcontext_command = (parent_type == c.backreference and parent_argument_name == "identifier") and (second_parent_type == c.context and second_parent_argument_name == c.command)
    except IndexError:
        pass
    return is_refcontext_command or is_backref_which_is_refcontext_command


def local_top_level(context: Context) -> bool:
    """Checks the top-level status of the given Context.

    Args:
        context: Context to check.

    Returns:
        Whether the context passed in represents a sort of "top-level" in the constructor tree.

    Raises:
        ValueError: if the top of the context has an empty parent constructor but a non-empty parent role.
    """
    context_top = context.top()
    if context_top[0] is None:
        if context_top[1] is None:
            return True
        raise ValueError("Somehow filling a parent role absent a parent constructor")
    elif ci.is_constructor_subtype(context_top[0], c.signal):
        _, remaining_context = context.pop()
        return local_top_level(remaining_context)
    return refcontext_top_level(context)


def context_resubstitution(context: Context, constructor_type: str, identifier: str) -> Context:
    """Substitutes the Constructor information at the top of the provided Context with that of the provided Constructor.

    Args:
        context: Context to be modified.
        constructor_type: Type of the influencing Constructor.
        identifier: Identifier of the influencing Constructor.

    Returns:
        Context with an appropriately modified top entry.
    """
    (parent_constructor, parent_role, _, _, _), remainder = context.pop()
    new_context_entry = ContextEntry(parent_constructor, parent_role, constructor_type, identifier, [])
    new_context = remainder.push(new_context_entry)
    return new_context


def push_tracking(catena: CatenaZipper, context: Context) -> CatenaZipper:
    """Adds information about the top of the provided Context to the provided CatenaZipper.

    Args:
        catena: Catena to add information to.
        context: Source Context for the new tracking information.

    Returns:
        CatenaZipper with a new CatenaTrackingEntry in the root's CatenaConfig.
    """
    top_entry = context.top().to_catenatrackingentry()
    return catena.add_to_config(top_entry)
