"""Collection of utility functions for use across other parts of Ninai.

Note that the registry of functions in mappings, as well as any custom type
aliases or exceptions, should reside in ninai.base.interfaces.
"""

from typing import Callable, List, Optional, Tuple, Union

import tfsl.interfaces as tfsli
import udiron.base.constants as c
import udiron.base.interfaces as udironi
import udiron.base.utils as u
from tfsl import (L, Q, Item, ItemLike, ItemValue, Language, Lexeme,
                  LexemeSense, Statement)
from udiron import CatenaConfig, CatenaZipper
from udiron.base.functionconfig import C_, FunctionConfig

import ninai.base.interfaces as i


def get_id_to_find(entity: object) -> str:
    """Obtains some sort of an ID from a provided object.

    Args:
        entity: Entity from which an ID is to be found.

    Returns:
        ID of an object to be searched for in a sense graph.

    Raises:
        ValueError: if a entity that is searched does not have an ID.
        TypeError: if a string provided to this function cannot actually be found in a sense graph.
        NotImplementedError: if an ID to find can't be obtained from the given entity.
    """
    if isinstance(entity, str):
        if tfsli.is_qid(entity) or tfsli.is_lsid(entity):
            return entity
        elif tfsli.is_lid(entity):
            current_lsid = L(entity).get_senses()[0].id
            if current_lsid is None:
                raise ValueError(f"{entity} somehow does not have a sense ID")
            return current_lsid
        raise TypeError(f"{entity} cannot be turned into something to be found in a sense graph")
    elif isinstance(entity, (ItemValue, LexemeSense)):
        current_entityid = entity.id
        if current_entityid is None:
            raise ValueError(f"{entity} somehow does not have an entity ID")
        return current_entityid
    elif isinstance(entity, Item):
        current_qid = entity.id
        if current_qid is None:
            raise ValueError(f"{entity} somehow does not have an item ID")
        return current_qid
    elif isinstance(entity, Lexeme):
        current_lid = entity.get_senses()[0].id
        if current_lid is None:
            raise ValueError(f"{entity} somehow does not have a lexeme ID")
        return current_lid
    else:
        raise NotImplementedError(f"Cannot get id of {type(entity)}")


def is_desired_gender(sense: tfsli.LSid, desired_gender: tfsli.Qid) -> bool:
    """Checks the grammatical gender of a lexeme sense.

    Args:
        sense: Lexeme sense to be checked.
        desired_gender: Qid for a gender to be checked.

    Returns:
        Whether the lexeme for the provided sense has the desired grammatical gender.
    """
    lex = L(sense)
    return lex.haswbstatement(c.grammatical_gender, desired_gender)


def is_desired_pos(sense: tfsli.LSid, desired_pos: tfsli.Qid) -> bool:
    """Checks the part of speech of a lexeme sense.

    Args:
        sense: Lexeme sense to be checked.
        desired_pos: Qid for a part of speech to be checked.

    Returns:
        Whether the lexeme of the provided sense is the desired part of speech.
    """
    lex = L(sense)
    return lex.category == desired_pos


def cat_argument_dicts(*argument_dicts: Optional[udironi.ScopeArgumentMapping]) -> udironi.ScopeArgumentMapping:
    """* ☞ Information: Merely a convenience wrapper around udiron.base.utility.cat_argument_dicts.

    Args:
        argument_dicts: (possibly empty) List of ScopeArgumentMappings.

    Returns:
        Merger of the provided ScopeArgumentMappings.
    """
    return u.cat_argument_dicts(*argument_dicts)


def propagate_scope_outputs(catena: CatenaZipper, scope_args: udironi.ScopeArgumentMapping) -> CatenaZipper:
    """Packages scope outputs for upward propagation to the renderer of another Constructor.

    Args:
        catena: CatenaZipper in which propagated scope outputs are to be stored.
        scope_args: Scope argument mapping.

    Returns:
        Agjusted CatenaZipper.
    """
    new_config = CatenaConfig.new(scope_args=scope_args)
    return catena.add_to_config(new_config)


def originating_constructor(catena: CatenaZipper) -> str:
    """Retrieves information from the CatenaTracking of a provided Catena.

    Args:
        catena: Catena to find an originator in.

    Returns:
        Type of the Constructor that generated this Catena.

    Todo:
        rewrite/remove depending on how tracking changes
    """
    _, _, current_tracking = catena.get_config()
    return current_tracking.root().constructor_type


def get_qualifier_values(stmt: Statement, qualifier: tfsli.Pid) -> List[tfsli.Qid]:
    """Retrieves qualifiers from a statement.

    Args:
        stmt: Statement to retrieve qualifiers from.
        qualifier: Qualifier property to retrieve.

    Returns:
        List of values for a particular qualifier of a given statement.

    Todo:
        This should ideally be agnostic to the type of the values, but right now a Wikibase entity with an id is assumed.
    """
    return [claim.get_itemvalue().get_qid() for claim in stmt[qualifier]]


def get_gender_stmts(subject: Union[tfsli.Qid, Item]) -> List[Statement]:
    """Retrieves gender statements from an entity.

    Args:
        subject: Item, either as a tfsl.Item or as a Qid, which potentially has P21 statements.

    Returns:
        P21 values of a given subject item.
    """
    current_object: ItemLike
    if isinstance(subject, (Item, Q)):
        current_object = subject
        return current_object[c.sex_or_gender]
    else:
        current_object = Q(subject)
        return current_object[c.sex_or_gender]


def has_desired_property_value(desired_property: tfsli.Pid, desired_value: str) -> Callable[[tfsli.LSid, i.CandidatePathNode], bool]:
    """Constructors filtration functions that check for property-value pairs.

    Args:
        desired_property: Desired property ID.
        desired_value: Desired value (assuming said value is a subclass of str).

    Returns:
        Filter function for filter_candidate_paths which checks that the provided sense has the desired property-value pair.
    """
    if desired_value == c.nonexistent_entity:
        def filter_function(sense: tfsli.LSid, path: i.CandidatePathNode) -> bool:
            """Checks whether a sense has no value for a property.

            Args:
                sense: Sense to check.
                path: (Currently unused.)

            Returns:
                Whether the sense has no value for the property.
            """
            current_sense = L(sense)[sense]
            return not current_sense.haswbstatement(desired_property, desired_value)
    else:
        def filter_function(sense: tfsli.LSid, path: i.CandidatePathNode) -> bool:
            """Checks whether a sense has a value for a property.

            Args:
                sense: Sense to check.
                path: (Currently unused.)

            Returns:
                Whether the sense has a particular property-value pair.
            """
            current_sense = L(sense)[sense]
            return current_sense.haswbstatement(desired_property, desired_value)
    return filter_function


def has_desired_language(desired_language: Language) -> Callable[[tfsli.LSid, i.CandidatePathNode], bool]:
    """Constructs language-specific filter functions.

    Args:
        desired_language: Language to be filtered for.

    Returns:
        Filter function for filter_candidate_paths which checks that the provided sense-path pair has the desired language.
    """
    def filter_function(sense: tfsli.LSid, path: i.CandidatePathNode) -> bool:
        """Filters for a particular path language.

        Args:
            sense: Sense in question.
            path: Path to that sense from a concept.

        Returns:
            Whether the path language is the desired one.
        """
        return path["lang"] == desired_language.item
    return filter_function


def get_shortest_candidate_paths(candidate_paths: i.CandidatePathSet) -> i.CandidatePathSet:
    """Filters the candidate path set for all senses with the shortest paths.

    Args:
        candidate_paths: Current set of paths to draw from.

    Returns:
        Those paths with the shortest length.
    """
    if candidate_paths:
        min_path_length = min([len(v["path"]) for k, v in candidate_paths.items()])
        revised_candidate_paths = {k: v for k, v in candidate_paths.items() if len(v["path"]) == min_path_length}
        return revised_candidate_paths
    return candidate_paths


def get_desired_paths(candidate_paths: i.CandidatePathSet, filter_function: Callable[[tfsli.LSid, i.CandidatePathNode], bool]) -> i.CandidatePathSet:
    """Helper for filter_candidate_paths below.

    Args:
        candidate_paths: Current set of paths to draw from.
        filter_function: Function to filter paths.

    Returns:
        All paths that satisfy the filter function.
    """
    return {k: v for k, v in candidate_paths.items() if filter_function(k, v)}


def filter_candidate_paths(candidate_paths: i.CandidatePathSet, filter_function: Callable[[tfsli.LSid, i.CandidatePathNode], bool]) -> i.CandidatePathSet:
    """Passes set of candidate paths through a filter function.

    Args:
        candidate_paths: Current set of paths to draw from.
        filter_function: Function to filter paths.

    Returns:
        All candidate paths where the target sense satisfies the provided filter function.
    """
    if obtained_candidate_paths := get_desired_paths(candidate_paths, filter_function):
        candidate_paths = obtained_candidate_paths
    return candidate_paths


def filter_for_location_of_sense_usage(candidate_paths: i.CandidatePathSet, config: FunctionConfig) -> i.CandidatePathSet:
    """Passes set of candidate paths through a filter function.

    Args:
        candidate_paths: Set of paths from an entity to lexeme senses.
        config: FunctionConfig from which a desired language style is chosen.

    Returns:
        All candidate paths where the location of usage of the senses are the location listed in config (if in fact it is listed there at all).
    """
    if desired_location := config.get(C_.location_of_sense_usage):
        filter_function = has_desired_property_value(c.location_of_sense_usage, desired_location)
        return filter_candidate_paths(candidate_paths, filter_function)
    else:
        return candidate_paths


def filter_for_language_style(candidate_paths: i.CandidatePathSet, config: FunctionConfig) -> i.CandidatePathSet:
    """Passes set of candidate paths through a filter function.

    Args:
        candidate_paths: Set of paths from an entity to lexeme senses.
        config: FunctionConfig from which a desired language style is chosen.

    Returns:
        All candidate paths where the language style of the senses are the style listed in config (if in fact it is listed there at all).
    """
    if desired_style := config.get(C_.language_style):
        filter_function = has_desired_property_value(c.language_style, desired_style)
        return filter_candidate_paths(candidate_paths, filter_function)
    else:
        return candidate_paths


def filter_for_part_of_speech(candidate_paths: i.CandidatePathSet, new_config: CatenaConfig, config_in: FunctionConfig) -> Tuple[i.CandidatePathSet, CatenaConfig]:
    """Passes set of candidate paths through a filter function.

    Args:
        candidate_paths: Set of paths from an entity to lexeme senses.
        new_config: CatenaConfig which may contain a part of speech to filter for.
        config_in: FunctionConfig from which a desired language style is chosen.

    Returns:
        All candidate paths where the lexemes of the senses are the part of speech listed in config_in (if in fact it is listed there at all).
    """
    if (desired_pos := config_in.get(C_.part_of_speech)) is not None:
        desired_pos = tfsli.Qid(desired_pos)
        new_config = new_config.set(C_.part_of_speech, desired_pos)
        candidate_paths = {k: v for (k, v) in candidate_paths.items() if is_desired_pos(k, desired_pos)}
    return candidate_paths, new_config
