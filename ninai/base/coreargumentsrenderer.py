"""Holds the CoreArgumentsRenderer class."""

from typing import TYPE_CHECKING, NamedTuple, Optional

from udiron import C_

import ninai.base.interfaces as i
from ninai.base.context import ContextEntry
from ninai.base.coreargumentprocessor import CoreArgumentProcessor

if TYPE_CHECKING:
    from ninai.base.constructor import Constructor
    from ninai.base.pausedrenderersupply import PausedRendererSupply
    from ninai.base.rendererarguments import RendererArguments


class CoreArgumentsRenderer(NamedTuple):
    """Renders all core arguments of a Constructor.

    Attributes:
        constructor: The Constructor to render core arguments for.
        arguments: Holds the actual core arguments to render and the outputs from those renderings.
        argument_index: Indicates which of the arguments has just been rendered.
        pause_cause: Exception from a core arguments processor should it raise one.
    """
    constructor: "Constructor"
    arguments: "RendererArguments"
    argument_index: int
    pause_cause: Optional[CoreArgumentProcessor]

    def print_problem(self) -> str:
        """Prints the particular problem that caused a CoreArgumentProcessor to pause.

        Returns:
            Human-readable version of the exception that caused core argument rendering to pause.
        """
        if self.pause_cause is None:
            return "No problems encountered"
        return self.pause_cause.print_problem()

    @classmethod
    def new(
        cls,
        constructor: "Constructor",
        arguments: "RendererArguments",
        index: int,
    ) -> "CoreArgumentsRenderer":
        """Essentially a custom constructor for the CoreArgumentsRenderer class.

        Args:
            constructor: The Constructor to render core arguments for.
            arguments: Holds the actual core arguments to render and the outputs from those renderings.
            index: Indicates which of the arguments has just been rendered.

        Returns:
            New CoreArgumentsRenderer.
        """
        return cls(constructor, arguments, index, None)

    def clone(
        self,
        constructor: Optional["Constructor"] = None,
        arguments: Optional["RendererArguments"] = None,
        index: Optional[int] = None,
        pause_cause: Optional[CoreArgumentProcessor] = None,
    ) -> "CoreArgumentsRenderer":
        """Clones a CoreArgumentsRenderer while making modifications to some of its fields.

        If any input to this function is omitted or None, then the corresponding field of the CoreArgumentsRenderer
        is preserved in the output.

        Args:
            constructor: The Constructor to render core arguments for.
            arguments: Holds the actual core arguments to render and the outputs from those renderings.
            index: Indicates which of the arguments has just been rendered.
            pause_cause: Exception from a core arguments processor should it raise one.

        Returns:
            New CoreArgumentsRenderer.
        """
        old_constructor, old_arguments, old_index, old_pause_cause = self
        return CoreArgumentsRenderer(
            old_constructor if constructor is None else constructor,
            old_arguments if arguments is None else arguments,
            old_index if index is None else index,
            old_pause_cause if pause_cause is None else pause_cause,
        )

    def __call__(self, resume_supply: Optional["PausedRendererSupply"] = None) -> "CoreArgumentsRenderer":
        """Renders the core arguments.

        Args:
            resume_supply: Information provided to the CoreArgumentsRenderer when resuming it.

        Returns:
            CoreArgumentsRenderer reflecting the outputs from rendering core arguments.
        """
        constructor_in, new_arguments, paused_index, pause_cause = self
        constructor_type, _, _, core_arguments, _, _ = constructor_in  # Constructor unpacked
        context_in = new_arguments.context
        new_config = new_arguments.config

        for idx, (core_argument, current_argument) in enumerate(core_arguments.items()):
            if idx < paused_index:
                continue

            if idx == paused_index and pause_cause is not None:
                processor = pause_cause
            else:
                current_argument_type, current_argument_id, _, _, _, _ = current_argument  # Constructor unpacked
                new_context = context_in.push(
                    ContextEntry(
                        constructor_type, core_argument, current_argument_type, current_argument_id, [],
                    ),
                )
                processor = CoreArgumentProcessor(
                    i.CoreArgumentProcessingPhase.PRE_HOOK,
                    current_argument, new_arguments.clone(
                        context=new_context, config=new_config.set(C_.origin_of_speech, new_context.top().to_catenatrackingentry()),
                    ),
                )

            processor = processor.render(resume_supply=resume_supply)

            _, _, _, pause_state, output = processor
            if pause_state is not None:
                return self.clone(pause_cause=processor, index=idx)
            else:
                if not output.empty():
                    new_arguments = new_arguments.push(core_argument, output)

        new_arguments = new_arguments.clone(config=new_config)
        return self.clone(arguments=new_arguments)
