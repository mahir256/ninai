"""Mappings to functions, custom type aliases, and custom exceptions for use in Ninai.

* ⌨ Implementation detail: If Wikifunctions functions end up being generic, then
    the Protocols defined here may end up being the actual types
    of the functions defined in Ninai that satisfy their type signatures.
"""

import datetime
from enum import IntEnum
from typing import (TYPE_CHECKING, Any, Dict, Iterator, List, Literal,
                    NamedTuple, Protocol, Sequence, Tuple, TypedDict, Union)

import tfsl.interfaces as tfsli
import udiron.base.interfaces as udironi
from tfsl import (Item, Language, Lexeme, LexemeForm, LexemeLike, LexemeSense,
                  LexemeSenseLike, Statement)
from udiron import CatenaConfig, CatenaZipper, FunctionConfig

if TYPE_CHECKING:
    from typing_extensions import TypeAlias
    from udiron.base.functionconfig import ItemList

    from ninai.base.constructor import Constructor
    from ninai.base.context import Context
    from ninai.base.framing import Framing
    from ninai.base.pausedrenderersupply import PausedRendererSupply
    from ninai.base.rendererarguments import RendererArguments

# The interfaces below do not rely on any Ninai types.


class CandidatePathNode(TypedDict):  # due to XMLRPC serialization, must remain a TypedDict
    """Information about the path in the sense graph from a given concept to other concepts linked to it.

    Attributes:
        lang: Language of the sense in question.
        path: Edges from a source concept to the sense in question.
    """
    lang: tfsli.Qid
    path: List[Tuple[tfsli.Pid, Union[tfsli.Qid, tfsli.LSid]]]


CandidatePathSet = Dict[tfsli.LSid, CandidatePathNode]

ChildIdentifiersMapping: "TypeAlias" = Dict[str, Tuple[Tuple[str, ...], str]]
CoreArguments: "TypeAlias" = Dict[str, "Constructor"]
ScopeArguments: "TypeAlias" = List["Constructor"]

ThematicRelationSpec: "TypeAlias" = Dict[str, Statement]


class SenseFindingFallback(Protocol):
    """Function called to produce Catenae when no suitable senses in a given target language are available.

    'Suitable senses' refers to those that are connected to a given concept by a path of property links.
    """

    def __call__(self, sense: str, lang: Language, config: FunctionConfig, paths: CandidatePathSet, /) -> Iterator[CatenaZipper]:
        """Runs a SenseFindingFallback.

        Args:
            sense: Sense to start with.
            lang: Language to render into.
            config: Current function configuration.
            paths: Current set of paths to candidate senses.

        Returns:
            Catenae reflecting the results of running fallback mechanisms.

        Todo:
            for all protocols, find a way to check observance of a protocol without needing the args/returns documentation portions
        """

# The interfaces below rely on at least one Ninai type and thus *must* use forward references for those types.


NonConstructorTypeArguments: "TypeAlias" = Union[
    str, datetime.datetime,
    # defined in tfsl
    Item, Lexeme, LexemeForm, LexemeSense,
    # defined in udiron
    FunctionConfig, "ItemList",
    # defined in Ninai
    "Framing", "NinaiError", "PausedRendererSupply",
]
ConstructorArgument: "TypeAlias" = Union["Constructor", NonConstructorTypeArguments]
ConstructorArguments: "TypeAlias" = Sequence[ConstructorArgument]


class ArgumentFilterBasis(Protocol):
    """Function which can be turned into an argument filter for a given Constructor."""

    def __call__(self, constructor_type: str, core_argument_list: List[str], *args: ConstructorArgument) -> "Constructor":
        """Builds a Constructor by filtering the provided arguments.

        Args:
            constructor_type: Name of the type of the new Constructor.
            core_argument_list: Names of arguments which are expected to be supplied in the argument list.
            args: Any number of arguments to supply to the new Constructor.

        Returns:
            New Constructor.
        """


class ArgumentFilter(Protocol):
    """Argument filter for a given Constructor.

    See ninai.base.argumentfilters for more information.
    """

    def __call__(self, *args: ConstructorArgument) -> "Constructor":
        """Builds a Constructor by filtering the provided arguments.

        Args:
            args: Any number of arguments to supply to the new Constructor.

        Returns:
            New Constructor.
        """


RefContextObjectMapping = Dict[str, ConstructorArgument]

ScopeOutput = Tuple["RendererArguments", FunctionConfig]
CoreArgumentType = Union["Constructor", Language, str]
CoreArgumentsOutput = Tuple["RendererArguments", FunctionConfig]
ArgProcessOutput = Tuple["RendererArguments", FunctionConfig]

ArgRendererOutput = Tuple[CatenaZipper, FunctionConfig]

ArgPreHookOutput = Tuple["Constructor", FunctionConfig]


class ArgPreHook(Protocol):
    """Makes adjustments to, or extracts information from, an argument Constructor before it is rendered.

    Used in CoreArgumentProcessor.run_arg_pre_hook.
    """

    def __call__(self, constructor_in: "Constructor", language: Language, context: "Context", framing: "Framing", config: FunctionConfig) -> ArgPreHookOutput:
        """Runs an ArgPreHook.

        Args:
            constructor_in:
            language:
            context:
            framing:
            config:

        Returns:
            (Possibly modified) Constructor and configuration therefor.
        """


ArgPostHookOutput = Tuple[CatenaZipper, FunctionConfig]


class ArgPostHook(Protocol):
    """Typically performs some clean-up related to the actions of the corresponding argument pre-hook.

    Used in CoreArgumentProcessor.run_arg_post_hook.
    """

    def __call__(self, catena: CatenaZipper, language: Language, context: "Context", config: FunctionConfig) -> ArgPostHookOutput:
        """Runs an ArgPostHook.

        Args:
            catena: Rendered argument.
            language: Language being rendered into.
            context: Current context of this argument rendering.
            config: Current configuration for this argument.

        Returns:
            Possibly modified rendered argument and FunctionConfig.
        """


PreHookOutput = Tuple["Constructor", "Context", FunctionConfig]


class PreHook(Protocol):
    """Performs some initial modifications to the Constructor itself, returning as output a Constructor.

    If no modifications end up being performed (that is, if the output is the same as the input),
    then the step is over; otherwise, the pre-hook associated with that Constructor's type is run,
    and a similar check is performed on the result of that pre-hook, and so on.
    ("Meta-Constructors" could be set up in this way by defining appropriate pre-hooks that yield
    Constructors of entirely different types than those of the Constructors initially provided.)

    Used in ConstructorRenderer.run_pre_hook.
    """

    def __call__(self, constructor: "Constructor", language: Language, context: "Context", framing: "Framing", config: FunctionConfig) -> PreHookOutput:
        """Runs a PreHook.

        Args:
            constructor: Constructor to run the pre-hook for.
            language: Language being rendered into.
            context: Current context for the Constructor.
            framing: Current framing for this rendering process.
            config: Current configuration for the Constructor.

        Returns:
            (Possibly new) Constructor, as well as a Context and FunctionConfig, both possibly modified.
        """


PostHookOutput = Tuple[CatenaZipper, FunctionConfig]


class PostHook(Protocol):
    """Performs some post-processing steps to the Catena returned by the main renderer.

    Used in ConstructorRenderer.run_post_hook.
    """

    def __call__(self, catena: CatenaZipper, language: Language, context: "Context", config: FunctionConfig) -> PostHookOutput:
        """Runs the PostHook.

        Args:
            catena: Original output.
            language: Language being rendered into.
            context: Context of this rendering step.
            config: Additional function configuration.

        Returns:
            Possibly modified output and FunctionConfig.
        """


RendererOutput = Tuple[CatenaZipper, FunctionConfig]


class Renderer(Protocol):
    """Produces a single Catena and potentially modified configuration from other Catenae and configurations output from rendering core and scope arguments.

    This must be defined for a particular Constructor type in a particular language before
    a Constructor of that type can be rendered in that language.

    It is here that most calls to Udiron syntax tree editors originate, and thus where most
    Catenae are generated, combined, and modified in the Constructor rendering process.

    Used by ConstructorMainRenderer.
    """

    def __call__(self, args: "RendererArguments") -> RendererOutput:
        """Runs the Renderer.

        Args:
            args: Current RendererArguments.

        Returns:
            Output Catena and modified config.
        """


class SyntacticRoleFunction(Protocol):
    """Function that adds a semantic argument to a predicate Catena."""

    def __call__(self, base: CatenaZipper, argument: CatenaZipper, args: "RendererArguments") -> CatenaZipper:
        """Adds a particular semantic argument to a Catena.

        Args:
            base: Current predicate Catena.
            argument: Argument to be added.
            args: Current RendererArguments.

        Returns:
            Predicate reflecting the added argument.
        """


SenseRefinementOutput = Tuple[CandidatePathSet, CatenaConfig]


class SenseRefinement(Protocol):
    """Function that filters a set of candidate paths for certain characteristics."""

    def __call__(self, paths: CandidatePathSet, language_in: Language, catena_config: CatenaConfig, function_config: FunctionConfig) -> SenseRefinementOutput:
        """Filters a candidate path set for different characteristics.

        Args:
            paths: Initial set of sense paths.
            language_in: Language being rendered into.
            catena_config: Initial configuration for the output Catena.
            function_config: Additional function configuration.

        Returns:
            Refined set of sense paths and any configuration that the output Catena needs to have.
        """


class Constructorify(Protocol):
    """Takes a Catena (usually derived from a Wikidata compound lexeme) and converts it into a (usually approximate) abstract content representation."""

    def __call__(self, catena: CatenaZipper) -> "Constructor":
        """Runs the Constructorify.

        Args:
            catena: Catena containing a lexeme to be transformed into a Constructor.

        Returns:
            Constructor approximating the content of that lexeme.
        """


class Transcribe(Protocol):
    """Takes a lexeme, a sense of that lexeme, and a transcription statement and returns a pseudo-lexeme containing an appropriate transcription of the lexeme's lemma into the target language."""

    def __call__(self, base_lex: LexemeLike, sense: LexemeSenseLike) -> Iterator[CatenaZipper]:
        """Runs the Transcribe.

        Args:
            base_lex: Lexeme containing a target sense for a concept.
            sense: The target sense in question.

        Returns:
            CatenaZipper reflecting the transcription of a term from another language.
        """


class BinominalCompoundCalque(Protocol):
    """Takes a head, a modifier, and a relation between the two and returns a new CatenaZipper combining the two."""

    def __call__(self, base: CatenaZipper, attribute: CatenaZipper, target_rel: tfsli.Qid) -> CatenaZipper:
        """Runs the BinominalCompoundCalque.

        Args:
            base: Base of the compound.
            attribute: Modifier of the compound.
            target_rel: Syntactic relationship between base and modifier.

        Returns:
            Compound combining base and attribute.
        """


class InitialCatenaProcessor(Protocol):
    """Performs some preliminary processing on a predicate Catena before doing anything else with it."""

    def __call__(self, base: CatenaZipper) -> CatenaZipper:
        """Runs the InitialCatenaProcessor.

        Args:
            base: Predicate Catena.

        Returns:
            Preliminarily processed predicate.
        """


class ThematicInflectionAddition(Protocol):
    """Adds inflections to a semantic argument reflecting its syntactic role in a sentence."""

    def __call__(self, base: CatenaZipper, inflections: List[tfsli.Qid]) -> CatenaZipper:
        """Runs the ThematicInflectionAddition.

        Args:
            base: The semantic argument in question.
            inflections: The inflections to apply to it.

        Returns:
            Inflected semantic argument.
        """


class InflectionFunction(Protocol):
    """Inflects a predicate for any number of grammatical details (tense, aspect, mood, person, number, or something else)."""

    def __call__(self, base: CatenaZipper, args: "RendererArguments") -> CatenaZipper:
        """Runs the InflectionFunction.

        Args:
            base: The predicate in question.
            args: Arguments for the predicate renderer.

        Returns:
            Inflected predicate.
        """


class ScopeOutputProcessor(Protocol):
    """Attaches any rendered scope arguments to a predicate."""

    def __call__(self, base: CatenaZipper, scope: udironi.ScopeArgumentMapping, config: FunctionConfig) -> CatenaZipper:
        """Runs the ScopeOutputProcessor.

        Args:
            base: The predicate in question.
            scope: The scope arguments in question.
            config: Additional configuration.

        Returns:
            Predicate with scope arguments added to it.
        """


class ThematicRelationlessProcessor(Protocol):
    """Modifier of a predicate in the event there are explicitly no semantic arguments for it.

    This may be used to satisfy the Extended Projection Principle for some languages.
    """

    def __call__(self, base: CatenaZipper) -> CatenaZipper:
        """Runs the ThematicRelationlessProcessor.

        Args:
            base: Catena representing a predicate.

        Returns:
            Modified predicate.
        """


class NinaiError(Exception):
    """Base exception type for Ninai-specific issues."""

    def __init__(self, message: str, cause: Any) -> None:
        """Builds a NinaiError.

        Args:
            message: Short message indicating the underlying exception.
            cause: Whatever caused this exception to be raised.
        """
        super().__init__()
        self.message = message
        self.cause = cause


class RenderingPhase(IntEnum):
    """Indicator of the current phase of the rendering process.

    Attributes:
        PRE_HOOK: Pre-hook
        SCOPE: Scope argument and config rendering
        CORE_ARGS: Argument rendering
        MAIN: Main rendering
        POST_HOOK: Post-hook
        FINISH: After post-hook
    """
    PRE_HOOK = 1
    SCOPE = 2
    CORE_ARGS = 3
    MAIN = 4
    POST_HOOK = 5
    FINISH = 6


class CoreArgumentProcessingPhase(IntEnum):
    """Indicator of the current phase of rendering of a single core argument.

    Attributes:
        PRE_HOOK: Argument pre-hook
        MAIN: Argument main rendering
        POST_HOOK: Argument post-hook
        FINISH: After argument post-hook
    """
    PRE_HOOK = 1
    MAIN = 2
    POST_HOOK = 3
    FINISH = 4


class PreHookOperator(NamedTuple):
    """Container for a pre hook, used in the NinaiOperator tagged union.

    Attributes:
        operator: The pre hook in question.
        operator_type: Tag for the type of operator in question.
    """
    operator: PreHook
    operator_type: Literal[RenderingPhase.PRE_HOOK] = RenderingPhase.PRE_HOOK


class MainRendererOperator(NamedTuple):
    """Container for a main renderer, used in the NinaiOperator tagged union.

    Attributes:
        operator: The main renderer in question.
        operator_type: Tag for the type of operator in question.
    """
    operator: Renderer
    operator_type: Literal[RenderingPhase.MAIN] = RenderingPhase.MAIN


class PostHookOperator(NamedTuple):
    """Container for a post hook, used in the NinaiOperator tagged union.

    Attributes:
        operator: The post hook in question.
        operator_type: Tag for the type of operator in question.
    """
    operator: PostHook
    operator_type: Literal[RenderingPhase.POST_HOOK] = RenderingPhase.POST_HOOK


RenderingPhaseStateOperator = Union[PreHookOperator, MainRendererOperator, PostHookOperator]

NinaiOperator = Union[
    PreHookOperator,
    MainRendererOperator,
    PostHookOperator,
]

CoreArgumentProcessingPhaseStateFunctionType = Union[ArgPreHook, ArgPostHook]
CoreArgumentProcessingPhaseStateOperator = Union[
    Tuple[Literal[CoreArgumentProcessingPhase.PRE_HOOK], ArgPreHook],
    Tuple[Literal[CoreArgumentProcessingPhase.POST_HOOK], ArgPostHook],
]
