"""Holds the Constructor class, including the render() and run_renderer() methods."""

from itertools import count
from textwrap import indent
from typing import TYPE_CHECKING, NamedTuple, Optional, Tuple, Union

from udiron import CatenaZipper, FunctionConfig

import ninai.base.interfaces as i
from ninai.base.constructorrenderer import ConstructorRenderer
from ninai.base.context import Context
from ninai.base.framing import Framing
from ninai.base.rendererarguments import RendererArguments

constructor_id_source = count()

if TYPE_CHECKING:
    from tfsl import Language


class Constructor(NamedTuple):
    """Primary unit of abstract semantic information in Ninai.

    (Most information related to this class is found in the overall documentation
    for this module.)

    Attributes:
        constructor_type: Type of Constructor this is.
        identifier: Identifier assigned to this Constructor.
        child_identifiers: Mapping of identifiers of (arguments of) Constructors to the paths needed to be followed in order to reach them.
        core_arguments: Mapping of core argument names to Constructors.
        scope_arguments: List of other Constructor arguments provided that have not been mapped to core argument names.
        other_arguments: List of arguments not handled by other steps.
    """
    constructor_type: str
    identifier: str
    child_identifiers: i.ChildIdentifiersMapping
    core_arguments: i.CoreArguments
    scope_arguments: i.ScopeArguments
    other_arguments: i.ConstructorArguments

    @classmethod
    def new(
        cls,
        constructor_type: Optional[str] = None,
        identifier: Optional[str] = None,
        child_identifiers: Optional[i.ChildIdentifiersMapping] = None,
        core_arguments: Optional[i.CoreArguments] = None,
        scope_arguments: Optional[i.ScopeArguments] = None,
        other_arguments: Optional[i.ConstructorArguments] = None,
    ) -> "Constructor":
        """Essentially a custom constructor for the Constructor class.

        Args:
            constructor_type: Type of Constructor this is.
            identifier: Identifier assigned to this Constructor.
            child_identifiers: Mapping of identifiers of (arguments of) Constructors to the paths needed to be followed in order to reach them.
            core_arguments: Mapping of core argument names to Constructors.
            scope_arguments: List of other Constructor arguments provided that have not been mapped to core argument names.
            other_arguments: List of arguments not handled by other steps.

        Returns:
            New Constructor.
        """
        return Constructor(
            constructor_type or "",
            identifier or "",
            child_identifiers or {},
            core_arguments or {},
            scope_arguments or [],
            other_arguments or [],
        )

    @classmethod
    def next_id(cls) -> str:
        """Supplies an integer to be suffixed to the end of a Constructor name, used as an identifier whenever one has not been supplied.

        Returns:
            ID for use in a Constructor, in the event no ID has been specified at Constructor creation time.
        """
        return str(next(constructor_id_source))

    def __call__(self, language: "Language") -> str:
        """Renders a Constructor to a Catena, and renders that to a string.

        Args:
            language: Language to render the Constructor in.

        Returns:
            String output from the rendering process.
        """
        output = self.render(language)
        if isinstance(output, CatenaZipper):
            return str(output)
        return str(output.print_problem())

    def __str__(self) -> str:
        """Produces a representation of a constructor intended for printing to a terminal (not a serialization).

        Returns:
            String representation of a Constructor.
        """
        out_str = f"({self.constructor_type} #{self.identifier}"

        core_argument_strs = []
        for name, argument in self.core_arguments.items():
            core_argument_strs.append(f"{name}={str(argument)}")
        if core_argument_strs:
            core_argument_str = indent(",\n".join(core_argument_strs), "  ")
            out_str = out_str + "\n" + core_argument_str

        scope_argument_strs = []
        for argument in self.scope_arguments:
            scope_argument_strs.append(str(argument))
        if scope_argument_strs:
            scope_argument_str = indent(",\n".join(scope_argument_strs), "  ")
            if core_argument_strs:
                out_str = out_str + ","
            out_str = out_str + "\n" + scope_argument_str

        if self.other_arguments:
            nc_argument_str = indent(",\n".join(str(arg) for arg in self.other_arguments), "  ")
            if core_argument_strs or scope_argument_strs:
                out_str = out_str + ","
            out_str = out_str + "\n" + nc_argument_str
        return out_str + ")"

    def render(
        self,
        language: "Language",
        context: Optional[Context] = None,
        framing: Optional[Framing] = None,
    ) -> Union[CatenaZipper, "ConstructorRenderer"]:
        """Renders (or attempts to render) a Constructor.

        Args:
            language: The Language into which this Constructor is rendered.
            context: The Context of this Constructor rendering.
            framing: The Framing from which base configuration information for this rendering is retrieved.

        Returns:
            Either the rendered output (if rendering finished without exceptions) or the Constructor renderer (if there was an exception).
        """
        renderer = self.run_renderer(language, context, framing)
        progress, *_, output_catenazipper = renderer
        if progress != i.RenderingPhase.FINISH:
            print(renderer.print_problem())
            return renderer
        else:
            return output_catenazipper

    def initialize_context(self, context: Optional[Context]) -> Context:
        """Either returns context (if that argument is not None), or a new Context (if that argument is None).

        Args:
            context: Context provided to a call to Constructor.run_renderer.

        Returns:
            Some Context object.
        """
        if context is None:
            constructor_type, constructor_id, _, _, _, _ = self  # Constructor unpack
            return Context.start_new(None, None, constructor_type, constructor_id, [])
        return context

    @staticmethod
    def initialize_framing_config(
            language: "Language",
            init_context: Context,
            framing: Optional[Framing],
            config: Optional[FunctionConfig],
    ) -> Tuple[Framing, FunctionConfig]:
        """Sets up a Framing and FunctionConfig based on the provided arguments.

        Args:
            language: Language into which a Constructor is rendered.
            init_context: Context returned from a call to Constructor.initialize_context.
            framing: Framing from which base configuration information for a rendering is retrieved.
            config: Separately specified function configuration for a rendering.

        Returns:
            Framing and FunctionConfig set up for use in a RendererArguments.
        """
        if framing is None:
            current_framing = Framing.new()
            current_config = FunctionConfig.new() if config is None else config
        else:
            current_framing = framing
            filtered_framing = current_framing.filter(init_context, language)
            current_config = config | filtered_framing
        return current_framing, current_config

    def run_renderer(
        self,
        language: "Language",
        context: Optional[Context] = None,
        framing: Optional[Framing] = None,
        config: Optional[FunctionConfig] = None,
    ) -> ConstructorRenderer:
        """Renders a Constructor.

        Args:
            language: Language into which this Constructor is rendered.
            context: Context of this Constructor rendering.
            framing: Framing from which base configuration information for this rendering is retrieved.
            config: Separately specified function configuration for this rendering.

        Returns:
            ConstructorRenderer called with these arguments, whether completed or paused.
        """
        init_context = self.initialize_context(context)

        init_framing, init_config = Constructor.initialize_framing_config(language, init_context, framing, config)

        new_arguments = RendererArguments({}, {}, [], language, init_context, init_framing, init_config)

        return ConstructorRenderer(i.RenderingPhase.PRE_HOOK, self, new_arguments).render()
