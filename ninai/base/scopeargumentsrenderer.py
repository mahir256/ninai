"""Holds the ScopeArgumentsRenderer class."""

from typing import TYPE_CHECKING, NamedTuple, Optional

from udiron import C_, FunctionConfig

import ninai.base.constructorinterfaces as ci
from ninai.base.context import ContextEntry

if TYPE_CHECKING:
    from ninai.base.constructor import Constructor
    from ninai.base.constructorrenderer import ConstructorRenderer
    from ninai.base.pausedrenderersupply import PausedRendererSupply
    from ninai.base.rendererarguments import RendererArguments


class ScopeArgumentsRenderer(NamedTuple):
    """Renders all scope arguments.

    Attributes:
        constructor: The Constructor being rendered.
        arguments: The scope arguments to be rendered.
        argument_index: Indicates which of the arguments has just been rendered.
        pause_cause: Exception from a scope argument's renderer should it raise one.
    """
    constructor: "Constructor"
    arguments: "RendererArguments"
    argument_index: int
    pause_cause: Optional["ConstructorRenderer"]

    def print_problem(self) -> str:
        """Prints the particular problem that caused a ScopeArgumentProcessor to pause.

        Returns:
            Human-readable version of the exception that caused the scope argument rendering to pause.
        """
        if self.pause_cause is None:
            return "No problems encountered"
        return self.pause_cause.print_problem()

    @classmethod
    def new(
        cls,
        constructor: "Constructor",
        arguments: "RendererArguments",
        index: int,
    ) -> "ScopeArgumentsRenderer":
        """Essentially a custom constructor for the ScopeArgumentsRenderer class.

        Args:
            constructor: The Constructor to render scope arguments for.
            arguments: Holds the actual scope arguments to render and the outputs from those renderings.
            index: Indicates which of the arguments has just been rendered.

        Returns:
            New ScopeArgumentsRenderer.
        """
        return cls(constructor, arguments, index, None)

    def clone(
        self,
        constructor: Optional["Constructor"] = None,
        arguments: Optional["RendererArguments"] = None,
        index: Optional[int] = None,
        pause_cause: Optional["ConstructorRenderer"] = None,
    ) -> "ScopeArgumentsRenderer":
        """Clones a CoreArgumentsRenderer while making modifications to some of its fields.

        If any input to this function is omitted or None, then the corresponding field of the CoreArgumentsRenderer
        is preserved in the output.

        Args:
            constructor: The Constructor being rendered.
            arguments: The scope arguments to be rendered.
            index: Indicates which of the arguments has just been rendered.
            pause_cause: Exception from a scope argument's renderer should it raise one.

        Returns:
            Modified ScopeArgumentsRenderer.
        """
        old_constructor, old_arguments, old_index, old_pause_cause = self
        return ScopeArgumentsRenderer(
            old_constructor if constructor is None else constructor,
            old_arguments if arguments is None else arguments,
            old_index if index is None else index,
            old_pause_cause if pause_cause is None else pause_cause,
        )

    def __call__(self, resume_supply: Optional["PausedRendererSupply"] = None) -> "ScopeArgumentsRenderer":
        """Renders the scope arguments.

        Args:
            resume_supply: Information provided to the ScopeArgumentsRenderer when resuming it.

        Returns:
            ScopeArgumentsRenderer reflecting the outputs from rendering scope arguments.
        """
        constructor_in, arguments_in, paused_index, paused_cause = self
        language_in, context_in, framing_in, config_in = arguments_in.language, arguments_in.context, arguments_in.framing, arguments_in.config
        _, _, constructor_type, _, _ = context_in.top()

        for idx, scope_object in enumerate(constructor_in.scope_arguments):
            scope_object_type, scope_object_identifier, _, _, _, _ = scope_object  # Constructor unpacked
            if idx > paused_index:
                new_context_scope = context_in.push(
                    ContextEntry(
                        constructor_type, None, scope_object_type, scope_object_identifier, [],
                    ),
                )

                if scope_object_type in ci.__config_insulators__:
                    scope_config = FunctionConfig.new()  # FIXME: push config_in to framing_in?
                else:
                    scope_config = config_in
            elif idx == paused_index:
                new_context_scope = context_in.push(
                    ContextEntry(
                        constructor_type, None, scope_object_type, scope_object_identifier, [],
                    ),
                )
                scope_config = config_in
            else:
                continue

            if paused_cause is not None and idx == paused_index:
                scope_state = paused_cause.render(resume_supply)
            else:
                scope_config = scope_config.set(C_.origin_of_speech, new_context_scope.top().to_catenatrackingentry())
                scope_state = scope_object.run_renderer(language_in, new_context_scope, framing_in, scope_config)

            _, _, arguments_out, paused_phase, scope_output = scope_state
            scope_config = arguments_out.config
            if paused_phase is not None:
                return self.clone(pause_cause=scope_state, index=idx)

            if not scope_output.empty():
                arguments_in = arguments_in.push_scope(scope_object_type, scope_output)

            if scope_object_type not in ci.__config_insulators__ and scope_config is not None:
                config_in = config_in.merge(scope_config)  # FIXME: pop out FunctionConfig from framing_in?

        for argument in constructor_in.other_arguments:
            arguments_in = arguments_in.push_other(argument)
        arguments_in = arguments_in.clone(config=config_in)

        return self.clone(arguments=arguments_in)
