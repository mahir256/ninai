"""Holds the ConstructorMainRenderer class."""

from typing import TYPE_CHECKING, List, NamedTuple, Optional

import udiron.base.interfaces as udironi
from udiron import C_, CatenaZipper, FunctionConfig

import ninai.base.constructorinterfaces as ci
import ninai.base.interfaces as i
from ninai.base.pausedrenderersupply import PausedRendererSupply
from ninai.renderers import find_main_renderer

if TYPE_CHECKING:
    from tfsl import Language

    from ninai.base.constructor import Constructor
    from ninai.base.context import Context
    from ninai.base.framing import Framing
    from ninai.base.rendererarguments import RendererArguments


class ConstructorMainRenderer(NamedTuple):
    """Encapsulator of methods that perform the main Constructor rendering step.

    Attributes:
        constructor: Actual Constructor being rendered.
        arguments: Current set of arguments used in this step of the rendering process.
        renderer: Main renderer used with this Constructor.
        output: Output from that main renderer, if it proceeds without raising an exception.
        pause_cause: Exception from that main renderer should it raise one.
    """
    constructor: "Constructor"
    arguments: "RendererArguments"
    renderer: Optional[i.Renderer] = None
    output: Optional[CatenaZipper] = None
    pause_cause: Optional[Exception] = None

    def print_problem(self) -> str:
        """Prints the particular problem that caused a ConstructorMainRenderer to pause.

        Returns:
            Human-readable version of the exception that caused the main Constructor rendering step to pause.
        """
        if self.pause_cause is None:
            return "No problems encountered"
        elif isinstance(self.pause_cause, i.NinaiError):
            return str(self.pause_cause.message)
        return str(self.arguments.context) + " - " + str(type(self.pause_cause)) + ": " + str(self.pause_cause)

    @classmethod
    def new(
        cls,
        constructor: "Constructor",
        arguments: "RendererArguments",
        index: int,
    ) -> "ConstructorMainRenderer":
        """Essentially a custom constructor for the ConstructorMainRenderer class.

        Args:
            constructor: Constructor to be rendered.
            arguments: Constructor arguments.
            index: Unused, exists to align with other RenderingPhaseEncapsulators.

        Returns:
            New ConstructorMainRenderer.
        """
        return cls(constructor, arguments)

    def clone(
        self,
        pause_cause: Optional[Exception] = None,
        constructor: Optional["Constructor"] = None,
        arguments: Optional["RendererArguments"] = None,
        renderer: Optional[i.Renderer] = None,
        output: Optional[CatenaZipper] = None,
    ) -> "ConstructorMainRenderer":
        """Clones a ConstructorMainRenderer while making modifications to some of its fields.

        If any input to this function is omitted or None, then the corresponding field of the ConstructorMainRenderer
        is preserved in the output.
        The sole exception to this is pause_cause, which is always set to the (possibly default) value of the respective input.

        Args:
            pause_cause: Exception raised during the main rendering process.
            constructor: Constructor being rendered.
            arguments: Constructor arguments.
            renderer: Main renderer function.
            output: Output from the main renderer function.

        Returns:
            New ConstructorMainRenderer.
        """
        old_constructor, old_arguments, old_renderer, old_output, _ = self
        return ConstructorMainRenderer(
            old_constructor if constructor is None else constructor,
            old_arguments if arguments is None else arguments,
            old_renderer if renderer is None else renderer,
            old_output if output is None else output,
            pause_cause,
        )

    def __call__(self, resume_supply: Optional[PausedRendererSupply] = None) -> "ConstructorMainRenderer":
        """Runs the main rendering step of a Constructor.

        Args:
            resume_supply: Any information provided if resuming a paused rendering step.

        Returns:
            Renderer reflecting any changes made during this step.
        """
        constructor_in, paused_arguments, paused_renderer, _, pause_cause = self
        _, _, other_args, language_in, context_in, framing_in, config_in = paused_arguments
        constructor_type, _, _, _, _, _ = constructor_in  #: Constructor unpacked

        if isinstance(pause_cause, i.NinaiError) and not isinstance(pause_cause.cause, Exception):
            to_prepend: List[i.ConstructorArgument]
            if resume_supply is not None:
                to_prepend = [pause_cause, resume_supply]
            else:
                to_prepend = [pause_cause, PausedRendererSupply()]
            new_other_args = to_prepend + list(other_args)
            paused_arguments = paused_arguments.clone(other_args=new_other_args)

        if paused_renderer is None:
            paused_operator, paused_config = PausedRendererSupply.handle_supply(None, config_in, resume_supply)
        else:
            paused_operator, paused_config = PausedRendererSupply.handle_supply(i.MainRendererOperator(paused_renderer), config_in, resume_supply)
        if paused_operator is not None:
            paused_renderer, _ = paused_operator

        intermedium = self.set_up_renderer(constructor_type, language_in, paused_renderer)
        if intermedium.renderer is None:
            return intermedium
        else:
            current_renderer = intermedium.renderer

        current_arguments = paused_arguments.merge_scope_dicts()
        current_config = self.set_up_config(constructor_type, language_in, context_in, framing_in, paused_config)
        current_config = current_config.set(C_.origin_of_speech, context_in.top().to_catenatrackingentry())
        current_arguments = current_arguments.clone(config=current_config)

        try:
            new_output, new_config = current_renderer(current_arguments)
        except (udironi.UdironError, i.NinaiError) as exception:
            return self.clone(pause_cause=exception, renderer=current_renderer)
        else:
            current_arguments = current_arguments.clone(config=new_config)
            return self.clone(arguments=current_arguments, output=new_output)

    def set_up_renderer(
        self,
        constructor_type: str,
        language: "Language",
        paused_renderer: Optional[i.Renderer],
    ) -> "ConstructorMainRenderer":
        """Finds a renderer for the Constructor in the given language, or uses one that was found at the time this encapsulator was paused.

        Args:
            constructor_type: Type of the Constructor being rendered.
            language: Language into which the Constructor is being rendered.
            paused_renderer: If resuming the ConstructorMainRenderer, any Renderer that may have been provided at resumption time.

        Returns:
            ConstructorMainRenderer with the 'renderer' field possibly set.
        """
        if paused_renderer is None:
            try:
                current_renderer = find_main_renderer(constructor_type, language)
            except KeyError as exception:
                return self.clone(pause_cause=exception)
        else:
            current_renderer = paused_renderer
        return self.clone(renderer=current_renderer)

    def set_up_config(self, constructor_type: str, language: "Language", context: "Context", framing: "Framing", current_config: FunctionConfig) -> FunctionConfig:
        """Adds information from the framing with the current constructor-language-context combination to the current FunctionConfig.

        Args:
            constructor_type: Type of the current constructor.
            language: Current language being rendered into.
            context: Current context of current rendering.
            framing: Current framing of current rendering.
            current_config: FunctionConfig entries already set by this point.

        Returns:
            New FunctionConfig for running the main renderer.
        """
        if constructor_type not in ci.__unfiltered_framing_constructors__:
            filtered_framing = framing.filter(context, language)
            returned_config = filtered_framing.merge(current_config)
        else:
            returned_config = current_config
        return returned_config
