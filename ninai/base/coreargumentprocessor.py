"""Holds the CoreArgumentProcessor class."""

from typing import TYPE_CHECKING, NamedTuple, Optional

from tfsl import langs
from udiron import CatenaZipper

import ninai.base.interfaces as i
from ninai.renderers import get_arg_post_hook, get_arg_pre_hook

if TYPE_CHECKING:
    from ninai.base.constructor import Constructor
    from ninai.base.constructorrenderer import ConstructorRenderer
    from ninai.base.pausedrenderersupply import PausedRendererSupply
    from ninai.base.rendererarguments import RendererArguments


class CoreArgumentProcessor(NamedTuple):
    """Holds the current rendering state of a particular Constructor core argument.

    Attributes:
        progress: Indicates the current phase of the core argument rendering process.
        constructor: The actual core argument being rendered.
        arguments: Contains the language, context, framing, and config for this CoreArgumentProcessor.
        pause_state: If a part the core argument rendering process must be paused, information saved from the part of the process that triggered the pause.
        output: If the core argument post-hook must be paused, then the Catena returned from running render() on the core argument.
    """
    progress: i.CoreArgumentProcessingPhase
    constructor: "Constructor"
    arguments: "RendererArguments"
    pause_state: Optional["ConstructorRenderer"] = None
    output: CatenaZipper = CatenaZipper([])

    def print_problem(self) -> str:
        """Prints the particular problem that caused a CoreArgumentProcessor to pause.

        Returns:
            Human-readable version of the exception that caused the core argument processing to pause.
        """
        if self.pause_state is None:
            return "No problems encountered"
        return self.pause_state.print_problem()

    def clone(
        self,
        progress: Optional[i.CoreArgumentProcessingPhase] = None,
        constructor: Optional["Constructor"] = None,
        arguments: Optional["RendererArguments"] = None,
        pause_state: Optional["ConstructorRenderer"] = None,
        output: Optional[CatenaZipper] = None,
    ) -> "CoreArgumentProcessor":
        """Clones a CoreArgumentProcessor while making modifications to some of its fields.

        If any input to this function is omitted or None, then the corresponding field of the CoreArgumentProcessor
        is preserved in the output.

        Args:
            progress: Rendering progress indicator.
            constructor: Constructor being rendered.
            arguments: Constructor arguments.
            pause_state: Rendering phase that has been paused, whether successfully or otherwise.
            output: Output from the argument renderer (and possibly argument post-hook).

        Returns:
            Modified CoreArgumentProcessor.
        """
        old_progress, old_constructor, old_arguments, old_pause_state, old_output = self
        return CoreArgumentProcessor(
            old_progress if progress is None else progress,
            old_constructor if constructor is None else constructor,
            old_arguments if arguments is None else arguments,
            old_pause_state if pause_state is None else pause_state,
            old_output if output is None else output,
        )

    def run_phase(
        self,
        phase: i.CoreArgumentProcessingPhase,
        resume_supply: Optional["PausedRendererSupply"] = None,
    ) -> "CoreArgumentProcessor":
        """Runs a particular core argument rendering phase.

        Args:
            phase: Particular phase to run.
            resume_supply: Information provided to the CoreArgumentProcessor when resuming it.

        Returns:
            CoreArgumentProcessor after having run the provided phase.
        """
        if phase is i.CoreArgumentProcessingPhase.PRE_HOOK:
            return self.run_arg_pre_hook(resume_supply)
        elif phase is i.CoreArgumentProcessingPhase.MAIN:
            return self.render_argument(resume_supply)
        elif phase is i.CoreArgumentProcessingPhase.POST_HOOK:
            return self.run_arg_post_hook(resume_supply)
        else:
            return self

    def render(
        self,
        resume_supply: Optional["PausedRendererSupply"] = None,
    ) -> "CoreArgumentProcessor":
        """Proceeds through the phases of core argument processing, starting with the current progress.

        Args:
            resume_supply: Information provided to the CoreArgumentProcessor when resuming it.

        Returns:
            CoreArgumentProcessor after having gone through as many phases as possible without pausing.
        """
        processor = self

        progress, *_ = processor

        for current_phase in i.CoreArgumentProcessingPhase:
            if progress < current_phase:
                processor = processor.run_phase(current_phase)
            elif progress == current_phase:
                processor = processor.run_phase(current_phase, resume_supply)
            if processor.pause_state is not None:
                break
        return processor

    def run_arg_pre_hook(self, resume_supply: Optional["PausedRendererSupply"] = None) -> "CoreArgumentProcessor":
        """If a pre hook is defined for a particular argument in a language, runs that.

        Args:
            resume_supply: Information provided to the CoreArgumentProcessor when resuming it.

        Returns:
            CoreArgumentProcessor reflecting any changes due to an argument pre-hook.
        """
        _, current_argument, arguments, _, _ = self
        _, _, _, language, context, framing, argument_config = arguments  # RendererArguments unpacked
        constructor_type, core_argument, *_ = context.top()  # ContextEntry unpacked
        if not (isinstance(constructor_type, str) and isinstance(core_argument, str)):
            return self
        try:
            try:
                arg_pre_hook = get_arg_pre_hook(constructor_type, core_argument, language)
            except KeyError:
                arg_pre_hook = get_arg_pre_hook(constructor_type, core_argument, langs.mul_)
        except KeyError:
            return self
        new_argument, new_config = arg_pre_hook(current_argument, language, context, framing, argument_config)
        return self.clone(
            progress=i.CoreArgumentProcessingPhase.MAIN,
            constructor=new_argument,
            arguments=self.arguments.clone(config=new_config),
        )

    def render_argument(self, resume_supply: Optional["PausedRendererSupply"] = None) -> "CoreArgumentProcessor":
        """Renders an argument of this constructor.

        Args:
            resume_supply: Information provided to the CoreArgumentProcessor when resuming it.

        Returns:
            CoreArgumentProcessor reflecting changes due to the current argument being rendered.
        """
        _, current_argument, arguments, pause_state, _ = self
        _, _, _, language, context, framing, argument_config = arguments  # RendererArguments unpacked

        if pause_state is not None:
            rendered = pause_state.render(resume_supply)
        else:
            rendered = current_argument.run_renderer(language, context, framing, argument_config)

        _, _, new_arguments, paused_phase, new_output = rendered
        if paused_phase is not None:
            return self.clone(progress=i.CoreArgumentProcessingPhase.MAIN, pause_state=pause_state)
        else:
            return self.clone(progress=i.CoreArgumentProcessingPhase.POST_HOOK, output=new_output, arguments=new_arguments)

    def run_arg_post_hook(self, resume_supply: Optional["PausedRendererSupply"] = None) -> "CoreArgumentProcessor":
        """If a post hook is defined for a particular argument in a language, runs that.

        Args:
            resume_supply: Information provided to the CoreArgumentProcessor when resuming it.

        Returns:
            CoreArgumentProcessor reflecting any changes due to an argument post-hook.
        """
        _, _, arguments, _, output = self
        _, _, _, language, context, _, argument_config = arguments  # RendererArguments unpacked
        constructor_type, core_argument, *_ = context.top()  # ContextEntry unpacked

        if not (isinstance(constructor_type, str) and isinstance(core_argument, str)):
            return self
        try:
            try:
                arg_post_hook = get_arg_post_hook(constructor_type, core_argument, language)
            except KeyError:
                arg_post_hook = get_arg_post_hook(constructor_type, core_argument, langs.mul_)
        except KeyError:
            return self
        new_output, new_config = arg_post_hook(output, language, context, argument_config)
        return self.clone(
            progress=i.CoreArgumentProcessingPhase.FINISH,
            output=new_output,
            arguments=self.arguments.clone(config=new_config),
        )
