"""Holds structures that contain names of Constructors' core arguments and the inheritance graph among Constructors."""

from typing import TYPE_CHECKING, Any, Dict, Generator, List, Set

if TYPE_CHECKING:
    import ninai.base.interfaces as i
    from ninai.base.constructor import Constructor

__constructor_argument_filters__: Dict[str, "i.ArgumentFilter"] = {}
__constructor_core_arguments__: Dict[str, List[str]] = {}
__constructor_docstrings__: Dict[str, str] = {}
__constructor_type_hierarchy__: Dict[str, List[str]] = {}
__config_insulators__: Set[str] = set()
__unfiltered_framing_constructors__: Set[str] = set()


def get_supertypes(subtype: str) -> Generator[str, None, None]:
    """Gets the supertypes of a Constructor type.

    Args:
        subtype: Constructor type name.

    Yields:
        The provided type's supertypes in a depth-first order.
    """
    current_list = [subtype]
    already_retrieved = set()
    while current_list:
        subtype, *current_list = current_list
        current_list = __constructor_type_hierarchy__.get(subtype, []) + current_list
        if subtype not in already_retrieved:
            already_retrieved.add(subtype)
            yield subtype


def is_constructor_subtype(subtype: str, supertype: str) -> bool:
    """Checks the subtyping relationship between two Constructor types.

    Args:
        subtype: Potential Constructor subtype name.
        supertype: Potential Constructor supertype name.

    Returns:
        Whether the potential subtype is in fact a subtype of the potential supertype.
    """
    return any(candidate == supertype for candidate in get_supertypes(subtype))


def merge_identifier_dicts(child_identifier_dict: "i.ChildIdentifiersMapping", parent_identifier_dict: "i.ChildIdentifiersMapping", role: str) -> "i.ChildIdentifiersMapping":
    """Merges the ChildIdentifiersMappings of two Constructors.

    Args:
        child_identifier_dict: The child identifiers mapping of a child Constructor.
        parent_identifier_dict: The child identifiers mapping of a parent Constructor.
        role: The role of the child Constructor relative to the parent Constructor.

    Returns:
        Merger of the two child identifier mappings.
    """
    new_identifier_dict: "i.ChildIdentifiersMapping" = parent_identifier_dict.copy()
    for key, (path, child) in child_identifier_dict.items():
        new_identifier_dict[key] = ((role,) + path, child)
    return new_identifier_dict


def deserialize_constructor(entry: List[Any]) -> "Constructor":
    """Takes a representation of a Constructor as lists of strings and returns a Constructor therefrom.

    Args:
        entry: Abstract content representation.

    Returns:
        Constructor for that abstract content.
    """
    if isinstance(entry, str):
        return entry
    elif isinstance(entry, list):
        af_to_find = entry[0]
        argument_filter = __constructor_argument_filters__[af_to_find]
        deserialized_arguments = []
        for subentry in entry[1:]:
            deserialized_arguments.append(deserialize_constructor(subentry))

        constructor = argument_filter(*deserialized_arguments)
        return constructor
