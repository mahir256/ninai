"""Holds the ConstructorRenderer class."""

from typing import TYPE_CHECKING, NamedTuple, Optional, Union

from udiron import C_, CatenaZipper, FunctionConfig

import ninai.base.interfaces as i
from ninai.base.constructormainrenderer import ConstructorMainRenderer
from ninai.base.coreargumentsrenderer import CoreArgumentsRenderer
from ninai.base.scopeargumentsrenderer import ScopeArgumentsRenderer
from ninai.renderers import find_post_hook, find_pre_hook

if TYPE_CHECKING:
    from tfsl import Language
    from typing_extensions import TypeAlias

    from ninai.base.constructor import Constructor
    from ninai.base.context import Context
    from ninai.base.framing import Framing
    from ninai.base.pausedrenderersupply import PausedRendererSupply
    from ninai.base.rendererarguments import RendererArguments

RenderingPhaseEncapsulator: "TypeAlias" = Union[ConstructorMainRenderer, CoreArgumentsRenderer, ScopeArgumentsRenderer]


class ConstructorRenderer(NamedTuple):
    """Holds the current rendering state of a particular Constructor.

    Attributes:
        progress: Indicator of the current phase of the Constructor rendering process.
        constructor: Actual Constructor being rendered.
        arguments: Current set of arguments used in different parts of the rendering process.
        paused_phase: If a part of the Constructor rendering process must be paused, information saved from the part of the process that triggered the pause.
        output: Catena returned from running run_main_renderer(), whether or not the post-hook is paused.
    """
    progress: i.RenderingPhase
    constructor: "Constructor"
    arguments: "RendererArguments"
    paused_phase: Optional[RenderingPhaseEncapsulator] = None
    output: CatenaZipper = CatenaZipper([])

    def print_problem(self) -> str:
        """Prints the particular problem that caused a ConstructorRenderer to pause.

        Returns:
            Human-readable version of the exception that caused the Constructor rendering to pause.
        """
        if self.paused_phase is None:
            return "No problems encountered"
        return self.paused_phase.print_problem()

    def clone(
        self,
        progress: Optional[i.RenderingPhase] = None,
        constructor: Optional["Constructor"] = None,
        arguments: Optional["RendererArguments"] = None,
        paused_phase: Optional[RenderingPhaseEncapsulator] = None,
        output: Optional[CatenaZipper] = None,
    ) -> "ConstructorRenderer":
        """Clones a ConstructorRenderer while making modifications to some of its fields.

        If most inputs to this function are omitted or None, then the corresponding fields of the ConstructorRenderer
        are preserved in the output.
        The sole exception to this is paused_phase, which is always set to the (possibly default) value of the respective input.

        Args:
            progress: Rendering progress indicator.
            constructor: Constructor being rendered.
            arguments: Constructor arguments.
            paused_phase: Rendering phase that has been paused, whether successfully or otherwise.
            output: Output from the main renderer (and possibly post-hook).

        Returns:
            New ConstructorRenderer.
        """
        old_progress, old_constructor, old_arguments, _, old_output = self
        return ConstructorRenderer(
            old_progress if progress is None else progress,
            old_constructor if constructor is None else constructor,
            old_arguments if arguments is None else arguments,
            paused_phase,
            old_output if output is None else output,
        )

    def __call__(self, resume_supply: Optional["PausedRendererSupply"] = None) -> "ConstructorRenderer":
        """Shorthand for ConstructorRenderer.render.

        Args:
            resume_supply: Information provided to the ConstructorRenderer when resuming it.

        Returns:
            The output CatenaZipper from rendering the Constructor.
        """
        return self.render(resume_supply)

    def run_phase(
        self,
        phase: i.RenderingPhase,
        resume_supply: Optional["PausedRendererSupply"] = None,
    ) -> "ConstructorRenderer":
        """Runs a particular Constructor rendering phase.

        Args:
            phase: Particular phase to run.
            resume_supply: Information provided to the ConstructorRenderer when resuming it.

        Returns:
            ConstructorRenderer after having run the provided phase.
        """
        if phase is i.RenderingPhase.PRE_HOOK:
            return self.run_pre_hook(resume_supply)
        elif phase is i.RenderingPhase.SCOPE:
            return self.render_scope(resume_supply)
        elif phase is i.RenderingPhase.CORE_ARGS:
            return self.render_core_arguments(resume_supply)
        elif phase is i.RenderingPhase.MAIN:
            return self.run_main_renderer(resume_supply)
        elif phase is i.RenderingPhase.POST_HOOK:
            return self.run_post_hook(resume_supply)
        else:
            return self

    def render(self, resume_supply: Optional["PausedRendererSupply"] = None) -> "ConstructorRenderer":
        """Proceeds through the phases of Constructor rendering, starting with the current progress.

        Args:
            resume_supply: Information provided to the ConstructorRenderer when resuming it.

        Returns:
            ConstructorRenderer after having gone through as many phases as possible without pausing.
        """
        renderer = self

        progress, *_ = renderer

        for current_phase in i.RenderingPhase:
            if progress < current_phase:
                renderer = renderer.run_phase(current_phase)
            elif progress == current_phase:
                renderer = renderer.run_phase(current_phase, resume_supply)
            if progress <= current_phase and renderer.paused_phase is not None:
                break
        return renderer

    @staticmethod
    def run_constructor_pre_hook(
            constructor: "Constructor",
            language: "Language",
            context: "Context",
            framing: "Framing",
            config: FunctionConfig,
    ) -> i.PreHookOutput:
        """If a pre hook is defined for the constructor in a language, runs that.

        Args:
            constructor: Constructor being rendered.
            language: Language being rendered.
            context: Context of the rendering step.
            framing: Framing of the entire rendering task.
            config: Current configuration at the start of the rendering process.

        Returns:
            New Constructor, new Context, and new FunctionConfig from running pre-hook (or the corresponding input arguments if no pre-hook was found).
        """
        constructor_type, _, _, _, _, _ = constructor  # Constructor unpacked
        try:
            pre_hook = find_pre_hook(constructor_type, language)
        except KeyError:
            return constructor, context, config
        else:
            config = config.set(C_.origin_of_speech, context.top().to_catenatrackingentry())
            return pre_hook(constructor, language, context, framing, config)

    def run_and_repeat_pre_hook(self) -> i.PreHookOutput:
        """Runs the pre-hook(s) for a Constructor.

        If the output Constructor from running a pre-hook is different from the input Constructor to that hook,
        then the pre-hook for *that* Constructor is run, and so on until the input and output Constructors are identical.

        Returns:
            New Constructor, new Context, and new FunctionConfig from running pre-hook(s).
        """
        _, old_constructor, arguments, _, _ = self
        old_language, old_context, old_framing, old_config = arguments.language, arguments.context, arguments.framing, arguments.config

        new_constructor, new_context, new_config = ConstructorRenderer.run_constructor_pre_hook(old_constructor, old_language, old_context, old_framing, old_config)
        while new_constructor != old_constructor:
            old_constructor = new_constructor
            new_constructor, new_context, new_config = ConstructorRenderer.run_constructor_pre_hook(old_constructor, old_language, new_context, old_framing, new_config)

        return new_constructor, new_context, new_config

    def run_pre_hook(self, resume_supply: Optional["PausedRendererSupply"] = None) -> "ConstructorRenderer":
        """Finds and runs the pre-hook for the current Constructor if it exists.

        Args:
            resume_supply: Information provided to the ConstructorRenderer when resuming it.

        Returns:
            ConstructorRenderer after running the pre-hook (if it exists).
        """
        new_constructor, new_context, new_config = self.run_and_repeat_pre_hook()

        new_arguments = self.arguments.clone(context=new_context, config=new_config)
        return self.clone(progress=i.RenderingPhase.SCOPE, constructor=new_constructor, arguments=new_arguments)

    def render_scope(self, resume_supply: Optional["PausedRendererSupply"] = None) -> "ConstructorRenderer":
        """Renders all scope arguments for the current Constructor.

        Args:
            resume_supply: Information provided to the ConstructorRenderer when resuming it.

        Returns:
            ConstructorRenderer after rendering all scope arguments.
        """
        if isinstance(self.paused_phase, ScopeArgumentsRenderer):
            scope_renderer = self.paused_phase
        else:
            scope_renderer = ScopeArgumentsRenderer.new(self.constructor, self.arguments, -1)

        scope_renderer = scope_renderer(resume_supply)

        _, arguments_out, _, paused_cause = scope_renderer
        if paused_cause is not None:
            return self.clone(progress=i.RenderingPhase.SCOPE, paused_phase=scope_renderer)
        else:
            return self.clone(progress=i.RenderingPhase.CORE_ARGS, arguments=arguments_out)

    def render_core_arguments(self, resume_supply: Optional["PausedRendererSupply"] = None) -> "ConstructorRenderer":
        """Renders all core arguments for the current Constructor.

        Args:
            resume_supply: Information provided to the ConstructorRenderer when resuming it.

        Returns:
            ConstructorRenderer after rendering all core arguments.
        """
        if isinstance(self.paused_phase, CoreArgumentsRenderer):
            core_renderer = self.paused_phase
        else:
            core_renderer = CoreArgumentsRenderer.new(self.constructor, self.arguments, -1)

        core_renderer = core_renderer(resume_supply)

        _, arguments_out, _, paused_cause = core_renderer
        if paused_cause is not None:
            return self.clone(progress=i.RenderingPhase.CORE_ARGS, paused_phase=core_renderer)
        else:
            return self.clone(progress=i.RenderingPhase.MAIN, arguments=arguments_out)

    def run_main_renderer(self, resume_supply: Optional["PausedRendererSupply"] = None) -> "ConstructorRenderer":
        """Runs the main renderer for this constructor.

        Args:
            resume_supply: Information provided to the ConstructorRenderer when resuming it.

        Returns:
            ConstructorRenderer after running the main renderer.
        """
        if isinstance(self.paused_phase, ConstructorMainRenderer):
            main_renderer = self.paused_phase
        else:
            main_renderer = ConstructorMainRenderer.new(self.constructor, self.arguments, -1)

        main_renderer = main_renderer(resume_supply)

        _, current_arguments, _, new_output, paused_cause = main_renderer
        if paused_cause is not None:
            return self.clone(progress=i.RenderingPhase.MAIN, paused_phase=main_renderer)
        else:
            return self.clone(progress=i.RenderingPhase.POST_HOOK, arguments=current_arguments, output=new_output)

    def run_post_hook(self, resume_supply: Optional["PausedRendererSupply"] = None) -> "ConstructorRenderer":
        """Finds and runs the post-hook for the current Constructor if it exists.

        Args:
            resume_supply: Information provided to the ConstructorRenderer when resuming it.

        Returns:
            ConstructorRenderer after running the post-hook (if it exists).
        """
        _, constructor_in, arguments, _, new_output = self
        language_in, context_in, new_config = arguments.language, arguments.context, arguments.config
        constructor_type, _, _, _, _, _ = constructor_in  # Constructor unpacked

        try:
            post_hook = find_post_hook(constructor_type, language_in)
        except KeyError:
            return self.clone(progress=i.RenderingPhase.FINISH)
        else:
            new_config = new_config.set(C_.origin_of_speech, context_in.top().to_catenatrackingentry())
            new_output, new_config = post_hook(new_output, language_in, context_in, new_config)
            return self.clone(progress=i.RenderingPhase.FINISH, arguments=arguments.clone(config=new_config), output=new_output)
