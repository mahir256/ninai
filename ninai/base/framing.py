"""Holds the Framing class.

Todo:
    .Name! = Constructor type and subtypes
    .Name1 .Name2 = Constructor within Constructor (Name2)
    .Name1>.Name2 = Constructor parent of Constructor (Name2)
"""

import re
from itertools import chain
from typing import (TYPE_CHECKING, Any, Dict, List, Literal, NamedTuple,
                    Optional, Tuple, overload)

from udiron.base.functionconfig import (Entity, EntityKey, FunctionConfig,
                                        StrKey)

import ninai.base.interfaces as i

if TYPE_CHECKING:
    from tfsl import Language

    from ninai.base.context import Context


class MethodKey(NamedTuple):
    """Represents a key in a Framing mapped to a Ninai Operator.

    Attributes:
        key: The key in question.
        val: Type of the value in question.
    """
    key: str
    val: Literal["method"] = "method"

    def __call__(self, arg: i.NinaiOperator) -> "Framing":
        """Generates a single-entry Framing.

        Args:
            arg: Entity to set in the Framing.

        Returns:
            Framing with one entry with the key set to the provided argument.
        """
        return Framing({}, {}, {self.key: arg}, FramingDict.new(), [])


class FramingKey(NamedTuple):
    """Key into a Framing.

    Attributes:
        constructor_type: Type of Constructor selected for.
        constructor_id: Identifier of Constructor selected for.
        lang: Rendering language being selected for.
        flags: Other flags controlling what is selected for.
    """
    constructor_type: str
    constructor_id: str
    lang: Tuple[str, str]
    flags: Tuple[str, ...]

    @classmethod
    def from_selector(cls, selector: str) -> "FramingKey":
        """Builds a FramingKey from a selector with the following components:

        - .Name = Constructor type
        - #Name = Constructor ID
        - :lang(Q9027) = when rendering constructor in language (item) Q9027
        - :lang(bn) = when rendering constructor in language (code) bn
        - :lang(Q9027bn) = when rendering constructor in language (item, code) (Q9027, bn)
        - :root = when rendering Constructor at top level
        - :empty = when rendering Constructor with no arguments (core, scope, other)
        - :empty(c|s|n) = when rendering Constructor with no (core, scope, other) arguments

        Args:
            selector: Shorthand for what should be looked for.

        Returns:
            Complex object representing that selector.
        """
        separated_selector = re.split(r"([.!#:])", selector)

        constructor_type = ""
        constructor_id = ""
        lang = ("", "")
        flags: List[str] = []

        component_being_read = ""

        for component in separated_selector:
            if component == "#":
                component_being_read = "id"
            elif component == ".":
                component_being_read = "type"
            elif component == ":":
                component_being_read = "other"
            elif component_being_read == "id":
                constructor_id = component
                component_being_read = ""
            elif component_being_read == "subclasses":
                constructor_type = constructor_type + "!"
                component_being_read = ""
            elif component_being_read == "type":
                constructor_type = component
                component_being_read = ""
            elif component_being_read == "other":
                if component.startswith("lang"):
                    lang = process_selector_lang(component)
                elif component == "root":
                    flags.append("r")
                elif component.startswith("empty"):
                    component_remainder = component[5:]
                    if "c" in component_remainder:
                        flags.append("c")
                    if "s" in component_remainder:
                        flags.append("s")
                    if "n" in component_remainder:
                        flags.append("s")
                component_being_read = ""

        return FramingKey(constructor_type, constructor_id, lang, tuple(flags))


class FramingDict(NamedTuple):
    """Mapping from keys to different types of objects in a Framing.

    Attributes:
        main: Mapping of framing keys to other Framing objects.
        constructors: Mapping of constructor types to keys with those types.
        ids: Mapping of object identifiers to keys with those identifiers.
        langs: Mapping of languages to keys with those languages.
    """
    main: Dict[FramingKey, "Framing"]
    constructors: Dict[str, List[FramingKey]]
    ids: Dict[str, List[FramingKey]]
    langs: Dict[Tuple[str, str], List[FramingKey]]

    @classmethod
    def new(cls) -> "FramingDict":
        """Essentially a custom constructor for the FramingDict class.

        Returns:
            New FramingDict.
        """
        return FramingDict({}, {}, {}, {})

    def add(self, key: FramingKey, val: "Framing") -> "FramingDict":
        """Adds a new mapping to a FramingDict from a key to a Framing object.

        Args:
            key: Key into the mapping to other Framing objects.
            val: Value for that key.

        Returns:
            FramingDict reflecting the new mapping.
        """
        main, constructors, ids, langs = self
        constructor_type, constructor_id, lang, flags = key
        return FramingDict(
            {**main, key: val},
            {**constructors, constructor_type: constructors.get(constructor_type, []) + [key]},
            {**ids, constructor_id: ids.get(constructor_id, []) + [key]},
            {**langs, lang: langs.get(lang, []) + [key]},
        )

    def get(self, key: FramingKey) -> "Framing":
        """Produces a Framing based on the key provided.

        Args:
            key: Key into the mapping to other Framing objects.

        Returns:
            Either an existing or new Framing object depending on whether the key is mapped or not.
        """
        return self.main.get(key) or Framing.new()

    def merge(self, other: "FramingDict") -> "FramingDict":
        """Merges two FramingDicts.

        Args:
            other: FramingDict to merge into this one.

        Returns:
            Merged FramingDict.
        """
        return FramingDict(
            {**self.main, **other.main},
            {k: list(set(self.constructors.get(k, [])) | set(other.constructors.get(k, []))) for k in set(chain(self.constructors.keys(), other.constructors.keys()))},
            {k: list(set(self.ids.get(k, [])) | set(other.ids.get(k, []))) for k in set(chain(self.ids.keys(), other.ids.keys()))},
            {k: list(set(self.langs.get(k, [])) | set(other.langs.get(k, []))) for k in set(chain(self.langs.keys(), other.langs.keys()))},
        )

    def filter(self, key: FramingKey) -> "Framing":
        """Filters objects in the FramingDict based on the key provided.

        Args:
            key: Key into the Framing.

        Returns:
            Framing filtered for entries matching the key.
        """
        constructor_type, constructor_id, lang, flags = key

        current_key_list = self.constructors.get(constructor_type, []) + self.constructors.get("", [])

        current_key_list = [key for key in current_key_list if key.constructor_id in [constructor_id, ""]]

        current_key_list = [key for key in current_key_list if (lang_matches(key, lang) or key.lang == ("", ""))]

        output_framing = Framing.new()
        for current_key in current_key_list:
            if all(flag in flags for flag in current_key.flags):
                output_framing = output_framing.merge(self.main[current_key])
        return output_framing


def lang_matches(key: FramingKey, lang: Tuple[str, str]) -> bool:
    """Checks a FramingKey against a tuple representing a code-item pair.

    Args:
        key: FramingKey in question.
        lang: Language item ID and language code.

    Returns:
        Whether the language item ID and code match those in the FramingKey.
    """
    key_item, key_code = key.lang
    lang_item, lang_code = lang
    item_and_code_match = key_item == lang_item and key_code == lang_code
    item_matches = key_item == lang_item and key_code == ""
    code_matches = key_item == "" and key_code == lang_code
    return item_and_code_match or item_matches or code_matches


selector_lang_regex = re.compile(r"lang\((Q\d+)?([Qa-z0-9-]+)?\)")


def process_selector_lang(selector_lang: str) -> Tuple[str, str]:
    """Parses a language found within a selector.

    Args:
        selector_lang: Language within a selector.

    Returns:
        Language item ID and language code extracted from the selector.

    Raises:
        ValueError: if the language provided was not in any recognized format.
    """
    selector_lang_match = selector_lang_regex.match(selector_lang)
    if selector_lang_match is not None:
        lang_qid, lang_code = selector_lang_match.group(1, 2)
        if lang_qid is not None or lang_code is not None:
            return lang_qid or "", lang_code or ""
    raise ValueError("Language provided was not an item, a code, or an item followed by a code")


zid_regex = re.compile(r"Z\d+")


class Framing(NamedTuple):
    """General container for configurations to functions.

    Attributes:
        strs: Mapping from strings to strings.
        entities: Mapping from strings to Entities.
        methods: Mapping from strings to Ninai operators.
        framings: Mapping from strings to other Framings.
        rcobjects: Stack of mappings from strings to objects in a RefContext.
    """
    strs: Dict[str, str]
    entities: Dict[str, Entity]
    methods: Dict[str, i.NinaiOperator]
    framings: FramingDict
    rcobjects: List[i.RefContextObjectMapping]

    def clone(
        self,
        strs: Optional[Dict[str, str]] = None,
        entities: Optional[Dict[str, Entity]] = None,
        methods: Optional[Dict[str, i.NinaiOperator]] = None,
        framings: Optional[FramingDict] = None,
        rcobjects: Optional[List[i.RefContextObjectMapping]] = None,
    ) -> "Framing":
        """Clones a Framing while making modifications to some of its fields.

        If any inputs to this function are omitted or None, then the corresponding fields of the Framing
        are preserved in the output.

        Args:
            strs: Mapping from strings to strings.
            entities: Mapping from strings to Entities.
            methods: Mapping from strings to Ninai operators.
            framings: Mapping from strings to other Framings.
            rcobjects: Stack of mappings from strings to objects in a RefContext.

        Returns:
            Modified Framing.
        """
        old_strs, old_entities, old_methods, old_framings, old_rcobjects = self
        return Framing(
            old_strs if strs is None else strs,
            old_entities if entities is None else entities,
            old_methods if methods is None else methods,
            old_framings if framings is None else framings,
            old_rcobjects if rcobjects is None else rcobjects,
        )

    @classmethod
    def new(cls) -> "Framing":
        """Essentially a custom constructor for the Framing class.

        Returns:
            New Framing.
        """
        return cls({}, {}, {}, FramingDict.new(), [])

    @classmethod
    def from_fc(cls, config: FunctionConfig) -> "Framing":
        """Builds a Framing from the contents of a FunctionConfig.

        Args:
            config: FunctionConfig with some StrKeys, EntityKeys, or both.

        Returns:
            Framing with the same StrKeys and EntityKeys as the provided config.
        """
        return cls(config.strs, {**config.entities}, {}, FramingDict.new(), [])

    @overload
    def get(self, key: FramingKey) -> Optional["Framing"]: ...
    @overload
    def get(self, key: EntityKey) -> Optional[Entity]: ...
    @overload
    def get(self, key: MethodKey) -> Optional[i.NinaiOperator]: ...
    @overload
    def get(self, key: StrKey) -> Optional[str]: ...

    def get(self, key: Any) -> Optional[Any]:
        """Gets a key from the top level of the Framing.

        Args:
            key: Key into the top level.

        Returns:
            Value for that key.

        Raises:
            KeyError: if the key provided is not valid for a Framing.
        """
        if isinstance(key, StrKey):
            return self.strs.get(key.key)
        elif isinstance(key, EntityKey):
            return self.entities.get(key.key)
        elif isinstance(key, MethodKey):
            return self.methods.get(key.key)
        elif isinstance(key, FramingKey):
            return self.framings.get(key)
        raise KeyError(f"{type(key)} is not a valid key for a Framing")

    def merge(self, other: "Framing") -> "Framing":
        """Merges the content of one Framing with another.

        Args:
            other: Framing to merge with.

        Returns:
            New Framing reflecting the merger.
        """
        return Framing(
            {**self.strs, **other.strs},
            {**self.entities, **other.entities},
            {**self.methods, **other.methods},
            self.framings.merge(other.framings),
            self.rcobjects + other.rcobjects,
        )

    def push_rcobjectmapping(self, new_references: i.RefContextObjectMapping) -> "Framing":
        """Adds a new object mapping to the current set of references.

        Args:
            new_references: RefContext object mapping to push.

        Returns:
            Framing reflecting the newly pushed object mapping.
        """
        return self.clone(rcobjects=self.rcobjects + [new_references])

    def as_fc(self) -> FunctionConfig:
        """Extracts only the parts of the top level of the Framing mapped by StrKeys and EntityKeys.

        Returns:
            FunctionConfig containing only the string and Entity values.
        """
        return FunctionConfig(self.strs, self.entities)

    @overload
    def set(self, key: EntityKey, value: Entity) -> "Framing": ...
    @overload
    def set(self, key: MethodKey, value: i.NinaiOperator) -> "Framing": ...
    @overload
    def set(self, key: StrKey, value: str) -> "Framing": ...

    def set(self, key: Any, value: Any) -> "Framing":
        """Sets a key in the top level of the Framing.

        Args:
            key: Key to set.
            value: Value to set.

        Returns:
            Framing reflecting the newly set key.
        """
        new_strs, new_entities, new_methods, old_framings, old_rcobjects = self
        if isinstance(key, StrKey):
            new_strs = self.strs.copy()
            new_strs.update({str(key.key): value})
        elif isinstance(key, EntityKey):
            new_entities = self.entities.copy()
            new_entities.update({str(key.key): value})
        elif isinstance(key, MethodKey):
            new_methods = self.methods.copy()
            new_methods.update({str(key.key): value})
        return Framing(new_strs, new_entities, new_methods, old_framings, old_rcobjects)

    @overload
    def add_attribute(self, selector: str, key: EntityKey, value: Entity) -> "Framing": ...
    @overload
    def add_attribute(self, selector: str, key: MethodKey, value: i.NinaiOperator) -> "Framing": ...
    @overload
    def add_attribute(self, selector: str, key: StrKey, value: str) -> "Framing": ...

    def add_attribute(self, selector: str, key: Any, value: Any) -> "Framing":
        """Sets an attribute in a Framing based on a given selector.

        Args:
            selector: Indicator of the context in which the attribute applies.
            key: Attribute key.
            value: Attribute value.

        Returns:
            Framing reflecting the newly set attribute.
        """
        strs, entities, methods, framings, rcobjects = self
        current_framingkey = FramingKey.from_selector(selector)
        current_framing = framings.get(current_framingkey) or Framing.new()
        current_framing = current_framing.set(key, value)
        new_framings = framings.add(current_framingkey, current_framing)
        return Framing(strs, entities, methods, new_framings, rcobjects)

    def get_from_rc(self, key: str) -> i.ConstructorArgument:
        """Obtains a ConstructorArgument from the last RefContextObjectMapping in this Framing.

        Args:
            key: Key into the last RefContextObjectMapping.

        Returns:
            Argument from that RefContextObjectMapping.
        """
        return self.rcobjects[-1][key]

    def filter(self, context: "Context", language: "Language") -> FunctionConfig:
        """Assembles a FunctionConfig based on the information in the Framing and the Context provided.

        Args:
            context: Current context to use in filtering.
            language: Current language to use in filtering.

        Returns:
            FunctionConfig with entries matching the provided context and language.

        Todo:
            scope entries should override the output of this function; provide as extra argument?
        """
        (_, _, constructor_type, constructor_id, _), _ = context.pop()

        first_part = "." + constructor_type if constructor_type != "" else ""
        second_part = "#" + constructor_id if constructor_type != "" else ""
        third_part = ":lang(" + language.item + language.code + ")"

        new_framingkey = FramingKey.from_selector(first_part + second_part + third_part)

        new_config = self.framings.filter(new_framingkey).as_fc()

        return FunctionConfig.new() | self.as_fc() | new_config
