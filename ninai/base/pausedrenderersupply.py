"""Holds the PausedRendererSupply class."""

from typing import NamedTuple, Optional, Tuple

from udiron import FunctionConfig

import ninai.base.interfaces as i


class PausedRendererSupply(NamedTuple):
    """Container for information supplied to the particular step at which a prior Constructor rendering was paused.

    Attributes:
        renderer: Renderer, if any, that is being supplied in the absence of one during the rendering process.
        config: Function configuration, if any, that is being supplied in the absence of one during the rendering process.
    """
    renderer: Optional[i.RenderingPhaseStateOperator] = None
    config: Optional[FunctionConfig] = None

    @staticmethod
    def handle_supply(
        paused_operator: Optional[i.RenderingPhaseStateOperator],
        paused_config: Optional[FunctionConfig],
        resume_supply: Optional["PausedRendererSupply"],
    ) -> Tuple[Optional[i.MainRendererOperator], FunctionConfig]:
        """If there is a provided paused renderer supply and that supply has a renderer, return that renderer.

        Also update the provided FunctionConfig with information from the supply if the supply exists.

        Args:
            paused_operator: A previously saved operator (if there is one).
            paused_config: A previously saved config (if there is one).
            resume_supply: A possibly provided PausedRendererSupply.

        Returns:
            An operator (if there is one) and a FunctionConfig.
        """
        if resume_supply is None:
            if isinstance(paused_operator, i.MainRendererOperator) or paused_operator is None:
                return paused_operator, paused_config or FunctionConfig.new()
            else:
                return None, paused_config or FunctionConfig.new()

        new_operator, new_config = resume_supply
        if new_operator is None and paused_operator is None:
            returned_operator = None
        else:
            if isinstance(new_operator, i.MainRendererOperator):
                returned_operator = new_operator
            elif isinstance(paused_operator, i.MainRendererOperator):
                returned_operator = paused_operator
            else:
                returned_operator = None

        if new_config is not None:
            returned_config = paused_config.merge(new_config) if paused_config is not None else new_config
        else:
            returned_config = paused_config or new_config or FunctionConfig.new()

        return returned_operator, returned_config
