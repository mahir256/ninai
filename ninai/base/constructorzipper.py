"""Holds the ConstructorZipper class and functions related to it.

TODO: propagate one during the rendering process somehow
"""

from typing import TYPE_CHECKING, List, NamedTuple, Tuple

from ninai.base.constructor import Constructor

if TYPE_CHECKING:
    import ninai.base.interfaces as i


class ArgumentLevel(NamedTuple):
    """An entry in a ConstructorZipper.

    Attributes:
        arg: Constructor at a particular level in the hierarchy.
        role: The name of the argument which this constructor fills in its parent.
    """
    arg: Constructor
    role: str


class ConstructorZipper(NamedTuple):
    """Due to the Wikifunctions requirement that all functions be pure functions, this construct has been introduced in order to assist the manipulation of Constructors.

    It is essentially a stack of ArgumentLevel objects that holds the path from
    the root of a particular Constructor to a particular nested Constructor (the 'cursor').
    When the cursor is modified in any way (for instance, when a nested Constructor is wrapped),
    a new ConstructorZipper with all parents adjusted to refer to that new Constructor must also be created
    --a textbook case of persistence.

    Attributes:
        zipper: The actual list of ArgumentLevels.
    """
    zipper: List[ArgumentLevel]

    def find(self, target: str) -> "ConstructorZipper":
        """Finds a particular child Constructor with a particular identifier.

        Args:
            target: Identifier of a child Constructor.

        Returns:
            ConstructorZipper with the same root as this one but with its head at the Constructor with the provided target identifier.
        """
        new_zipper = self.zipper.copy()
        cur_arg, _ = new_zipper[0]
        _, _, child_identifiers, _, _, _ = cur_arg  # Constructor unpack
        path_to_target, _ = child_identifiers[target]
        for step in path_to_target:
            step_suffix = step[6:]
            _, _, _, arguments, scope, _ = cur_arg  # Constructor unpack
            if "scope_" in step:
                new_arg = next(filter(lambda arg: arg[0] == step_suffix, scope))
            else:
                new_arg = arguments[step]
            new_zipper = [ArgumentLevel(new_arg, step)] + new_zipper
            cur_arg = new_arg
        return ConstructorZipper(new_zipper)

    def root(self) -> ArgumentLevel:
        """Shortcut to the last ArgumentLevel in this zipper.

        Returns:
            The Constructor at the root of this zipper.
        """
        return self.zipper[-1]

    def top(self) -> ArgumentLevel:
        """Shortcut to the first ArgumentLevel in this zipper.

        Returns:
            The Constructor at the head of this zipper.
        """
        return self.zipper[0]

    def empty(self) -> bool:
        """Checks for a nonzero length of this zipper.

        Returns:
            Whether this zipper is empty.
        """
        return not self.zipper

    def pop(self) -> Tuple[ArgumentLevel, "ConstructorZipper"]:
        """Removes the first ArgumentLevel from this zipper.

        Returns:
            Constructor at the head of the zipper as well as the resulting zipper when that head is removed.

        Raises:
            IndexError: if the ConstructorZipper is empty.
        """
        if not self.zipper:
            raise IndexError("ConstructorZipper is empty")
        return self.zipper[0], ConstructorZipper(self.zipper[1:])

    def wrap(self, wrapping: "i.ArgumentFilter") -> "ConstructorZipper":
        """Wraps the Constructor at the head of the zipper with the provided argument filter.

        Args:
            wrapping: Argument filter to be used on the zipper head.

        Returns:
            Zipper reflecting the wrapped head Constructor.
        """
        cur_arglevel, higher_levels = self.pop()
        cur_arg, cur_role = cur_arglevel
        new_arg = wrapping(cur_arg)
        new_arglevel = ArgumentLevel(new_arg, cur_role)
        adjusted_parent_list = adjust_parents(higher_levels, cur_arglevel, new_arglevel)
        return ConstructorZipper(adjusted_parent_list)


def zip_constructor(arg: Constructor) -> ConstructorZipper:
    """Creates a ConstructorZipper from the provided Constructor.

    Args:
        arg: Constructor to be zipped.

    Returns:
        Zipped Constructor.
    """
    return ConstructorZipper([ArgumentLevel(arg, "")])


def adjust_parents(higher_parents: ConstructorZipper, original_level: ArgumentLevel, new_level: ArgumentLevel) -> List[ArgumentLevel]:
    """Utility function that recursively modifies all higher entries in a given ConstructorZipper to reflect a change from the original_dependent to the new_dependent.

    Args:
        higher_parents: All parents above the current lowest level.
        original_level: The current lowest level prior to being modified.
        new_level: The current lowest level after being modified.

    Returns:
        List of argument levels reflecting modifications to higher-level entries.
    """
    if higher_parents.empty():
        return [new_level]

    current_parent, higher_parents = higher_parents.pop()

    current_arg, current_role = current_parent
    new_arg, new_role = new_level
    constructor_type, identifier, child_identifiers, core_arguments, scope_arguments, other_arguments = current_arg  # Constructor unpack
    _, _, new_child_identifiers, _, _, new_other_arguments = new_arg  # Constructor unpack
    if "scope_" in new_role:
        revised_role = new_role[6:]
        revised_core_arguments = core_arguments
        revised_scope_arguments = [arg for arg in scope_arguments if arg[0] != revised_role] + [new_arg]
    else:
        revised_core_arguments = core_arguments.copy()
        revised_core_arguments[new_role] = new_arg
        revised_scope_arguments = scope_arguments
    revised_child_identifiers = child_identifiers.copy()
    revised_other_arguments = [*other_arguments, *new_other_arguments]
    for (target, (path, instance)) in new_child_identifiers.items():
        revised_child_identifiers[target] = ((new_role,) + path, instance)
    revised_arg = Constructor(constructor_type, identifier, revised_child_identifiers, revised_core_arguments, revised_scope_arguments, revised_other_arguments)

    revised_parent = ArgumentLevel(revised_arg, current_role)
    return [new_level] + adjust_parents(higher_parents, current_parent, revised_parent)
