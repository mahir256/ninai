"""Holds argument filters and functions to create them.

Constructor objects are initialized through functions, which in Python
have a human-readable name intended to represent the type of the Constructor, that
arrange and process their arguments into core, scope, and non-Constructor arguments
before supplying them to the new Constructor object.
"""

import logging
from functools import partial
from graphlib import CycleError, TopologicalSorter
from typing import List, NamedTuple, Optional, Sequence

import udiron.base.constants as c

import ninai.base.constructorinterfaces as ci
import ninai.base.interfaces as i
from ninai.base.constructor import Constructor

logger = logging.getLogger(__name__)


def default_argument_filter(constructor_type: str, core_argument_list: List[str], *args: i.ConstructorArgument) -> Constructor:
    """i.ArgumentFilter for constructors, assuming they were not declared with a custom argument filter.

    Args:
        constructor_type: Name of the Constructor type.
        core_argument_list: List of names of core arguments.
        args: Actual arguments to the Constructor.

    Returns:
        Constructor for an identifier.
    """
    identifier: Optional[str] = None
    scope_arguments: i.ScopeArguments = []
    core_arguments: i.CoreArguments = {}
    child_identifiers: i.ChildIdentifiersMapping = {}
    other_arguments = []

    index: int = -1
    core_argument: str

    for index, core_argument in enumerate(core_argument_list):
        current_argument = args[index]
        if isinstance(current_argument, Constructor):
            child_identifiers = handle_identifiers(current_argument, core_argument, child_identifiers)
            core_arguments[core_argument] = current_argument
    for _, argument in enumerate(args[index + 1:]):
        if argument is None:
            continue
        elif isinstance(argument, Constructor):
            if ci.is_constructor_subtype(argument.constructor_type, "Identifier"):
                identifier = argument.identifier
                child_identifiers[identifier] = ((), constructor_type)
            else:
                child_identifiers = handle_identifiers(argument, "scope_" + argument.constructor_type, child_identifiers)
                scope_arguments.append(argument)
        else:
            other_arguments.append(argument)
    if identifier is None:
        identifier = constructor_type + "_" + Constructor.next_id()
    return Constructor(constructor_type, identifier, child_identifiers, core_arguments, scope_arguments, other_arguments)


def handle_other_options(constructor_type: str, other_options: Optional[Sequence[str]]) -> None:
    """Sets any number of other options when registering a Constructor type.

    Args:
        constructor_type: Type of a newly registered Constructor.
        other_options: (Possibly empty) list of other options provided as strings.
    """
    if other_options is not None:
        for option in other_options:
            if option == "config_insulator":
                ci.__config_insulators__.add(constructor_type)
            elif option == "unfiltered_framing":
                ci.__unfiltered_framing_constructors__.add(constructor_type)


def set_constructor_type_hierarchy(constructor_type: str, constructor_parents: List[str]) -> None:
    """Adds the Constructor type to the internal hierarchy of Constructor types.

    Args:
        constructor_type: Type of a newly registered Constructor.
        constructor_parents: Direct parents of the newly registered Constructor in the hierarchy.

    Raises:
        CycleError: if a cycle is detected in the hierarchy.
    """
    temp_hierarchy = ci.__constructor_type_hierarchy__.copy()
    temp_hierarchy[constructor_type] = constructor_parents
    try:
        TopologicalSorter(temp_hierarchy).prepare()
    except CycleError as exception:
        logger.exception("Inheritance cycle detected: %s", exception.args[1])
        raise
    else:
        ci.__constructor_type_hierarchy__[constructor_type] = constructor_parents


def get_core_argument_list(constructor_parents: List[str]) -> List[str]:
    """Retrieves the core arguments of a list of parent Constructors.

    Args:
        constructor_parents: List of parent types of a given Constructor.

    Returns:
        Core argument list assembled from concatenating the lists of the parent types in order.
    """
    core_argument_list: List[str] = []
    for parent in constructor_parents:
        parent_core_arguments = ci.__constructor_core_arguments__[parent]
        core_argument_list = core_argument_list + parent_core_arguments
    return core_argument_list


class ArgumentFilterOptions(NamedTuple):
    """Holds arguments to an ArgumentFilter.

    Attributes:
        constructor_type: The name used internally to refer to the type of the Constructors created by the returned argument filter.
        constructor_parents: The supertypes of Constructors created by the returned argument filter.
        core_arguments: The names assigned to the first few (obligatory) arguments of the returned argument filter.
        docstring: Docstring for the argument filter.
        argument_filter: The function on which the returned argument filter is based.
        other_options: A list of flags, each of which indicates an option used elsewhere in the Constructor rendering process.
    """
    constructor_type: str
    constructor_parents: List[str]
    core_arguments: List[str]
    docstring: str
    argument_filter: i.ArgumentFilterBasis = default_argument_filter
    other_options: Optional[Sequence[str]] = None


def set_argument_filter(options: ArgumentFilterOptions) -> None:
    """Sets the argument filter used with a Constructor.

    Args:
        options: Options for this ArgumentFilter.
    """
    constructor_type, constructor_parents, core_arguments, docstring, argument_filter, other_options = options
    set_constructor_type_hierarchy(constructor_type, constructor_parents)
    core_argument_list = get_core_argument_list(constructor_parents) + core_arguments
    ci.__constructor_core_arguments__[constructor_type] = core_argument_list
    ci.__constructor_docstrings__[constructor_type] = docstring
    handle_other_options(constructor_type, other_options)
    ci.__constructor_argument_filters__[constructor_type] = partial(argument_filter, constructor_type, core_argument_list)


def passthrough_argument_filter(constructor_type: str, core_argument_list: List[str], *args: i.ConstructorArgument) -> Constructor:
    """i.ArgumentFilter that does not keep track of any of the child identifiers of its core arguments.

    Args:
        constructor_type: Name of the Constructor type.
        core_argument_list: List of names of core arguments.
        args: Actual arguments to the Constructor.

    Returns:
        Constructor for an identifier.
    """
    identifier: Optional[str] = None
    child_identifiers: i.ChildIdentifiersMapping = {}
    core_arguments: i.CoreArguments = {}
    scope_arguments: i.ScopeArguments = []
    other_arguments = []

    scope_argument_start = len(core_argument_list)
    if args:
        for idx, core_argument in enumerate(core_argument_list):
            current_argument = args[idx]
            if isinstance(current_argument, Constructor):
                core_arguments[core_argument] = current_argument

    for argument in args[scope_argument_start:]:
        if argument is None:
            continue
        elif isinstance(argument, Constructor) and ci.is_constructor_subtype(argument.constructor_type, c.identifier):
            identifier = argument.identifier
            child_identifiers[identifier] = ((), constructor_type)
            continue
        elif isinstance(argument, Constructor):
            child_identifiers = handle_identifiers(argument, "scope_" + argument.constructor_type, child_identifiers)
            scope_arguments.append(argument)
        else:
            other_arguments.append(argument)
    if identifier is None:
        identifier = constructor_type + "_" + Constructor.next_id()
    return Constructor(constructor_type, identifier, child_identifiers, core_arguments, scope_arguments, other_arguments)


def identifier_argument_filter(constructor_type: str, core_argument_list: List[str], *args: i.ConstructorArgument) -> Constructor:
    """i.ArgumentFilter taking the first string in the ConstructorArgument list and setting it as a new Constructor's only non-core argument.

    Args:
        constructor_type: Name of the Constructor type.
        core_argument_list: List of names of core arguments.
        args: Actual arguments to the Constructor.

    Returns:
        Constructor for an identifier.

    Raises:
        ValueError: if no string is provided in args.
    """
    for arg in args:
        if isinstance(arg, str):
            return Constructor(constructor_type, arg, {}, {}, [], [arg])
    raise ValueError("String not provided as identifier")


def handle_identifiers(arg: Constructor, role: str, child_identifiers: i.ChildIdentifiersMapping) -> i.ChildIdentifiersMapping:
    """Adds any child identifiers known to the argument added to the mapping from the parent Constructor.

    Args:
        arg: Constructor used as an argument to another parent Constructor.
        role: Role of the argument relative to that parent Constructor.
        child_identifiers: Current ChildIdentifiersMapping of the parent Constructor.

    Returns:
        Revised ChildIdentifiersMapping.
    """
    new_child_identifiers: i.ChildIdentifiersMapping = child_identifiers.copy()
    new_child_identifiers[arg.identifier] = ((role,), arg.constructor_type)
    new_child_identifiers = ci.merge_identifier_dicts(arg.child_identifiers, new_child_identifiers, role)
    return new_child_identifiers


set_argument_filter(
    ArgumentFilterOptions(
        c.identifier, [], [],
        """Used to specify an identifier for the immediate parent Constructor:

        ["Q151885", "Q319", ["Q1773882", "jupiter_concept"]]

        Now the Concept Constructor above may be referred to in other Constructors,
        or in Constructor Framings, with the string "jupiter_concept".

        If an Identifier Constructor is not present within another Constructor, then
        that other Constructor will simply have an automatic identifier assigned to it.
        """,
        argument_filter=identifier_argument_filter,
    ),
)

set_argument_filter(
    ArgumentFilterOptions(
        c.signal, [], [],
        """Base class of all signals used to manually control features of other Constructors which, at render time, are often realized as inflections.

        (This should *not* be used directly; use one of its subtypes instead.)
        """,
    ),
)
