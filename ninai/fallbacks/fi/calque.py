"""Holds functions for generating calques in Finnish from Catenae for their elements."""

import tfsl.interfaces as tfsli
import udiron.base.constants as c
from tfsl import langs
from udiron import C_, CatenaZipper

from ninai.renderers import register


@register("binominal_compound_calque", langs.fi_)
def ecc_finnish(
        base: CatenaZipper,
        attribute: CatenaZipper,
        target_rel: tfsli.Qid,
) -> CatenaZipper:
    """Assembles an endocentric compound in Finnish.

    Args:
        base: Base of the compound.
        attribute: Modifier of the compound.
        target_rel: Syntactic relationship between base and modifier.

    Returns:
        Compound combining base and attribute.
    """
    attribute = attribute.add_to_config(C_.compounding(c.forwards))
    if target_rel == c.gen_attribute:
        attribute = attribute.add_inflections([c.genitive])
    output = base.attach_leftmost(attribute, target_rel)
    return output
