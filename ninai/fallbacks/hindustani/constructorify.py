"""Holds methods to generate a Constructor from a Hindustani lexeme."""

from typing import TYPE_CHECKING, Any, List

import udiron.base.constants as c
from tfsl import langs

import ninai.base.constructorinterfaces as ci
from ninai.renderers import register

if TYPE_CHECKING:
    from udiron import CatenaZipper

    from ninai.base.constructor import Constructor


def constructorify_hindustani_sub(catena: "CatenaZipper") -> List[Any]:
    """Subfunction of constructorify_hindustani that is used for recursion purposes.

    Args:
        catena: Catena containing a lexeme to be transformed into a Constructor.

    Returns:
        Abstract content representation of the Constructor approximation.
    """
    result = []

    has_subject = catena.has_rel(c.subject)
    has_copulative_verb = catena.has_rel(c.copula)
    has_verbal_root = catena.get_category() == c.verb
    has_possessive = catena.has_rel(c.possessive) and catena.rel(c.possessive).has_rel(c.adposition) and catena.rel(c.possessive).rel(c.adposition).get_lexeme().id == "L408913"

    if has_possessive:
        possessor = catena.rel(c.possessive)
        possessum = catena.detach_rel(c.possessive)
        possessor = possessor.detach_rel(c.adposition)
        result = [c.possession, constructorify_hindustani_sub(possessum), constructorify_hindustani_sub(possessor)]

    elif has_subject:
        constructored_subject = constructorify_hindustani_sub(catena.rel(c.subject))
        if has_copulative_verb and not has_verbal_root:
            predicate = catena.detach_rel(c.subject).detach_rel(c.copula)
            if predicate.get_category() == c.adjective:
                result = [c.attributional_construction, constructored_subject, [c.concept, predicate.get_sense().id]]
            else:
                result = [c.classificational_construction, constructored_subject, [c.concept, predicate.get_sense().id]]

    else:
        result = [c.concept, catena.get_sense().id or ""]
    return result


@register("constructorify", langs.hi_)
def constructorify_hindustani(catena: "CatenaZipper") -> "Constructor":
    """Attempts to generate a Constructor representing the content of a Hindustani lexeme.

    Args:
        catena: Catena containing a lexeme to be transformed into a Constructor.

    Returns:
        Constructor approximating the content of that lexeme.
    """
    result = constructorify_hindustani_sub(catena)
    return ci.deserialize_constructor(result)
