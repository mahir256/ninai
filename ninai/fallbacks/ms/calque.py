"""Holds functions for generating calques in Finnish from Catenae for their elements."""

import tfsl.interfaces as tfsli
import udiron.base.constants as c
from tfsl import langs
from udiron import C_, CatenaZipper

from ninai.renderers import register


@register("binominal_compound_calque", langs.ms_)
def ecc_malay(
        base: CatenaZipper,
        attribute: CatenaZipper,
        target_rel: tfsli.Qid,
) -> CatenaZipper:
    """Assembles an endocentric compound in Malay.

    Args:
        base: Base of the compound.
        attribute: Modifier of the compound.
        target_rel: Syntactic relationship between base and modifier.

    Returns:
        Compound combining base and attribute.
    """
    output = base.attach_rightmost(attribute, target_rel)
    return output
