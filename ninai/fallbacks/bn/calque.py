"""Holds functions for generating calques in Norwegian from Catenae for their elements."""

from typing import TYPE_CHECKING

import tfsl.interfaces as tfsli
import udiron.base.constants as c
from tfsl import langs

from ninai.renderers import register

if TYPE_CHECKING:
    from udiron import CatenaZipper


@register("binominal_compound_calque", langs.bn_)
def ecc_bn(
    base: "CatenaZipper",
    attribute: "CatenaZipper",
    target_rel: tfsli.Qid,
) -> "CatenaZipper":
    """Assembles an endocentric compound in Bengali.

    Args:
        base: Base of the compound.
        attribute: Modifier of the compound.
        target_rel: Syntactic relationship between base and modifier.

    Returns:
        Compound combining base and attribute.
    """
    if target_rel != c.adj_attribute:
        attribute = attribute.add_inflections([c.genitive])
    output = base.attach_leftmost(attribute, target_rel)
    return output
