"""Holds functions for generating calques in Turkish from Catenae for their elements."""

import tfsl.interfaces as tfsli
import udiron.base.constants as c
import udiron.langs.tr as tr
import udiron.langs.tr.constants as ctr
from tfsl import langs
from udiron import CatenaZipper, FunctionConfig

from ninai.renderers import register


@register("binominal_compound_calque", langs.tr_)
def ecc_tr(
        base: CatenaZipper,
        attribute: CatenaZipper,
        target_rel: tfsli.Qid,
) -> CatenaZipper:
    """Assembles an endocentric compound in Turkish.

    * ⓘ Q116446388 § 10 🗏 102

    Args:
        base: Base of the compound.
        attribute: Modifier of the compound.
        target_rel: Syntactic relationship between base and modifier.

    Returns:
        Compound combining base and attribute.
    """
    if attribute.has_rel(c.inflectional_suffix) and attribute.rel(c.inflectional_suffix).get_sense().id == ctr.possessive_affixes[c.third_person, c.singular]:
        attribute = attribute.detach_rel(c.inflectional_suffix)
    output = base.attach_leftmost(attribute, target_rel)
    if output.has_rel(c.inflectional_suffix) and output.rel(c.inflectional_suffix).get_sense().id == ctr.possessive_affixes[c.third_person, c.singular]:
        pass
    else:
        output = tr.add_suffix(output, ctr.possessive_affixes[c.third_person, c.singular], FunctionConfig.new())
    return output
