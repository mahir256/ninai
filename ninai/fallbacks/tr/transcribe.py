"""Holds functions for generating transcriptions into Turkish from other languages' lexemes."""

import contextlib
import logging
from typing import Iterator, Optional

import udiron.base.constants as c
from tfsl import (Lexeme, LexemeForm, LexemeLike, LexemeSenseLike, Statement,
                  langs)
from udiron import (Catena, CatenaConfig, CatenaZipper, DependentEntry,
                    DependentList)

from ninai.graph.utility import get_ipa
from ninai.renderers import register

logger = logging.getLogger(__name__)

yi_to_tr_chars = {
    "ב": "b",
    "ג": "g",
    "ד": "d",
    "ה": "h",
    "ו": "u",
    "ז": "z",
    "װ": "v",
    "ױ": "oy",
    "ח": "h",
    "ט": "t",
    "י": "i",
    "יי": "ey",
    "כ": "h",
    "ך": "h",
    "ל": "l",
    "מ": "m",
    "ם": "m",
    "נ": "n",
    "ן": "n",
    "ס": "s",
    "ע": "e",
    "פּ": "p",
    "פ": "f",
    "ף": "f",
    "צ": "ts",
    "ץ": "ts",
    "ק": "k",
    "ר": "r",
    "ש": "ş",
}

yi_to_tr_twochars = {
    "אַ": "a",
    "אָ": "o",
    "זש": "j",
    "טש": "ç",
}

tr_to_devoice = {
    "b": "p",
    "d": "t",
    "c": "ç",
    "g": "k",
}

ipa_to_tr_super_naive = {
    "b": "b",
    "ç": "ş",
    "ɕ": "ş",
    "d": "d",
    "ɖ": "d",
    "f": "f",
    "ɡ": "g",
    "h": "h",
    "j": "y",
    "k": "k",
    "l": "l",
    "ɫ": "l",
    "ɭ": "l",
    "m": "m",
    "n": "n",
    "ɳ": "n",
    "ŋ": "ng",
    "p": "p",
    "r": "r",
    "ɽ": "r",
    "s": "s",
    "ʂ": "ş",
    "t": "t",
    "ʈ": "t",
    "v": "v",
    "w": "v",
    "ɑ": "a",
    "a": "a",
    "æ": "e",
    "ɛ": "e",
    "e": "e",
    "ɪ": "i",
    "i": "i",
    "ɔ": "o",
    "o": "o",
    "œ": "ö",
    "ø": "ö",
    "ʊ": "ı",
    "u": "u",
    "ɵ": "ı",
    "ʉ": "ü",
    "ʏ": "ü",
    "y": "ü",
    "ə": "ı",
    "ɾ": "r",
    "ɧ": "ş",
}

ipa_to_tr_twochars_super_naive = {
    "ʈʂ": "ç",
    "tɕ": "ç",
    "ɑɪ": "ay",
    "æɪ": "ey",
    "æʉ": "eğü",
    "ɛɪ": "ey",
    "ɔʏ": "oy",
    "œʏ": "öy",
    "ʉɪ": "üy",
    "n̩": "ın",
    "ɳ̍": "ın",
    "l̩": "ıl",
}


def abelson_to_tr_text(yi_str: str) -> str:
    """Transforms (naïvely) a transcription from Paul Abelson's English-Yiddish Encyclopedic Dictionary into Turkish Latin orthography.

    Args:
        yi_str: Yiddish-script transcription to transform.

    Returns:
        Transformation of that transcription.
    """
    out_char_list = []
    i = 0
    while i < len(yi_str):
        if yi_str[i] == "ד" and yi_str[i:i + 3] == "דזש":
            out_char_list.append("c")
            i += 3
        elif yi_str[i:i + 2] in yi_to_tr_twochars:
            out_char_list.append(yi_to_tr_twochars[yi_str[i:i + 2]])
            i += 2
        elif yi_str[i] in yi_to_tr_chars:
            out_char_list.append(yi_to_tr_chars[yi_str[i]])
            i += 1
        else:
            i += 1
    with contextlib.suppress(IndexError):
        if out_char_list[-1] in tr_to_devoice:
            out_char_list[-1] = tr_to_devoice[out_char_list[-1]]
    return "".join(out_char_list)


def ipa_to_tr_text(ipa_str: str) -> str:
    """Transforms (naïvely) an IPA transcription into Turkish Latin orthography.

    Args:
        ipa_str: IPA transcription to transform.

    Returns:
        Transformation of that transcription.
    """
    out_char_list = []
    i = 0
    while i < len(ipa_str):
        if ipa_str[i:i + 2] in ipa_to_tr_twochars_super_naive:
            out_char_list.append(ipa_to_tr_twochars_super_naive[ipa_str[i:i + 2]])
            i += 2
        elif ipa_str[i] in ipa_to_tr_super_naive:
            out_char_list.append(ipa_to_tr_super_naive[ipa_str[i]])
            i += 1
        else:
            i += 1
    with contextlib.suppress(IndexError):
        if out_char_list[-1] in tr_to_devoice:
            out_char_list[-1] = tr_to_devoice[out_char_list[-1]]
    return "".join(out_char_list)


def abelson_to_tr(base_lex: LexemeLike, sense: LexemeSenseLike, statement: Statement) -> CatenaZipper:
    """Produces a Turkish pseudo-lexeme based on the transcription from Paul Abelson's English-Yiddish Encyclopedic Dictionary.

    Args:
        base_lex: Lexeme from another language to be used.
        sense: Sense of the lexeme in question to be used.
        statement: Pronunciation statement to be used.

    Returns:
        CatenaZipper using the new Turkish pseudo-lexeme.
    """
    transcription = abelson_to_tr_text(statement.get_monolingualtext().text)
    pseudoform = LexemeForm(transcription @ langs.tr_)
    pseudolexeme = Lexeme(transcription @ langs.tr_, langs.tr_, base_lex.category, [], [sense.to_editable()], [pseudoform])
    return CatenaZipper([
        DependentEntry(
            Catena(pseudolexeme, langs.tr_, sense, frozenset(), CatenaConfig.new(), DependentList([], []), Catena.next_id()),
            c.syntactic_root,
        ),
    ])


def ipa_to_tr(base_lex: LexemeLike, sense: LexemeSenseLike, statement: Statement) -> CatenaZipper:
    """Produces a Turkish pseudo-lexeme based on the IPA transcription of another language's lexeme form.

    Args:
        base_lex: Lexeme from another language to be used.
        sense: Sense of the lexeme in question to be used.
        statement: IPA transcription statement to be used.

    Returns:
        CatenaZipper using the new Turkish pseudo-lexeme.
    """
    transcription = ipa_to_tr_text(statement.get_str())
    pseudoform = LexemeForm(transcription @ langs.tr_)
    pseudolexeme = Lexeme(transcription @ langs.tr_, langs.tr_, base_lex.category, [], [sense.to_editable()], [pseudoform])
    return CatenaZipper([
        DependentEntry(
            Catena(pseudolexeme, langs.tr_, sense, frozenset(), CatenaConfig.new(), DependentList([], []), Catena.next_id()),
            c.syntactic_root,
        ),
    ])


def get_abelson_pronunciation(base_lex: LexemeLike) -> Optional[Statement]:
    """Finds pronunciation statements from Paul Abelson's English-Yiddish Encyclopedic Dictionary.

    Args:
        base_lex: Lexeme which may have forms with "pronunciation" statements.

    Returns:
        First "pronunciation" statement on a form of that lexeme that cites Q113799928.
    """
    for form in base_lex.get_forms():
        for pronunciation_stmt in form[c.pronunciation]:
            for reference in pronunciation_stmt.references:
                for claim in reference[c.stated_in]:
                    if claim.get_itemvalue().get_qid() == "Q113799928":
                        logger.info("Found an English-Yiddish Encyclopedic Dictionary pronunciation statement on %s", form.id)
                        return pronunciation_stmt
    return None


@register("transcribe", langs.tr_)
def transcribe_tr(base_lex: LexemeLike, sense: LexemeSenseLike) -> Iterator[CatenaZipper]:
    """Transcription function for Turkish.

    Args:
        base_lex: Lexeme containing a target sense for a concept.
        sense: The target sense in question.

    Yields:
        CatenaZipper reflecting the transcription of a term from another language.
    """
    if (statement := get_ipa(base_lex)) is not None:
        yield ipa_to_tr(base_lex, sense, statement)
    elif (statement := get_abelson_pronunciation(base_lex)) is not None:
        yield abelson_to_tr(base_lex, sense, statement)
