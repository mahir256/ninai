"""Holds functions for generating calques in Punjabi from Catenae for their elements."""

import tfsl.interfaces as tfsli
import udiron.base.constants as c
import udiron.langs.mul as mul
import udiron.langs.punjabi.constants as cpa
from tfsl import langs
from udiron import CatenaZipper, FunctionConfig

from ninai.graph.client import find_sense
from ninai.renderers import register


@register("binominal_compound_calque", langs.pa_)
def ecc_punjabi(
        base: CatenaZipper,
        attribute: CatenaZipper,
        target_rel: tfsli.Qid,
) -> CatenaZipper:
    """Assembles an endocentric compound in Punjabi.

    Args:
        base: Base of the compound.
        attribute: Modifier of the compound.
        target_rel: Syntactic relationship between base and modifier.

    Returns:
        Compound combining base and attribute.
    """
    if target_rel == c.adj_attribute:
        attribute = mul.simple_inflect_for_gender(attribute, base)
    else:
        linking_lexeme = find_sense(cpa.da_genitive, base.get_language(), FunctionConfig.new())
        linking_lexeme = mul.simple_inflect_for_gender(linking_lexeme, base)
        attribute = attribute.attach_right_of_root(linking_lexeme, c.adposition)
        attribute = attribute.add_inflections([c.oblique])
    output = base.attach_leftmost(attribute, target_rel)
    return output
