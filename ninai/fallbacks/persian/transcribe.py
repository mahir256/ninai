"""Holds functions for generating transcriptions into Persian from other languages' lexemes."""

import logging
import re
from typing import Iterator

import udiron.base.constants as c
from tfsl import (Language, Lexeme, LexemeForm, LexemeLike, LexemeSenseLike,
                  MonolingualText, langs)
from udiron import (Catena, CatenaConfig, CatenaZipper, DependentEntry,
                    DependentList)

from ninai.renderers import register


logger = logging.getLogger(__name__)

def rectify(transcription: MonolingualText) -> str:
    """Performs modifications of a transcription to bring it in line with New Persian orthography.

    Args:
        transcription: Transcription to be modified.

    Returns:
        Modified transcription.
    """
    rectified = transcription.text
    if transcription.language == langs.ru_:
        rectified = re.sub(
            "[А-Яа-я]",
            lambda k: {"Ц": "С", "ц": "с", "Щ": "Ч", "щ": "ч"}.get(k[0], k[0]),
            rectified[0],
        ) + re.sub(
            "[а-я]",
            lambda k: {"ъе": "йе", "ъа": "я", "ъо": "ё", "ъу": "ю", "ы": "и", "ц": "тс", "щ": "шч", "э": "е"}.get(k[0], k[0]),
            rectified[1: -1],
        ) + re.sub("ы", "а", rectified[-1])
    elif transcription.language == langs.tr_:
        rectified = re.sub(
            "[aâcçeğjoöôüûy]", lambda m: {
                "a": "o",
                "â": "o",
                "c": "ç",
                "ç": "c",
                "e": "a",
                "ğ": "ƣ",
                "j": "ƶ",
                "o": "ū",
                "ö": "ū",
                "ô": "ū",
                "ü": "u",
                "û": "u",
                "y": "j",
            }[m[0]], rectified,
        )
        if rectified[-1] in {"i", "ı", "î"}:
            rectified = rectified[:-1] + "ī"
        rectified = re.sub("[ıî]", "i", rectified)
    return rectified


def transform_lexeme_lemma(sense: LexemeSenseLike, base_lex: LexemeLike, language_in: Language, language_out: Language) -> CatenaZipper:
    """Produces a Catena with a pseudo-lexeme containing a transcription from another language.

    Args:
        sense: Sense of a lexeme for a concept.
        base_lex: The lexeme containing that sense.
        language_in: Language of the lemma to get a transcription for.
        language_out: Language to be rendered into.

    Returns:
        CatenaZipper containing the pseudo-lexeme.
    """
    logger.info("Transforming lemma of %s in %s to provide equivalent in %s", sense.id, language_in, language_out)
    transcription = base_lex[language_in]
    transcription_text = rectify(transcription)
    pseudoform = LexemeForm(transcription_text @ language_out)
    pseudolexeme = Lexeme(transcription_text @ language_out, language_out, base_lex.category, [], [base_lex.get_senses()[0].to_editable()], [pseudoform])
    return CatenaZipper([
        DependentEntry(
            Catena(pseudolexeme, language_out, sense, frozenset(), CatenaConfig.new(), DependentList([], []), Catena.next_id()),
            c.syntactic_root,
        ),
    ])


@register("transcribe", langs.new_persian_latn_)
def transcribe_persian_latn(base_lex: LexemeLike, sense: LexemeSenseLike) -> Iterator[CatenaZipper]:
    """Transcription function for New Persian in the Latin script.

    Args:
        base_lex: Lexeme containing a target sense for a concept.
        sense: The target sense in question.

    Yields:
        CatenaZipper reflecting the transcription of a term from another language.
    """
    if base_lex.language in [langs.tr_, langs.en_]:
        yield transform_lexeme_lemma(sense, base_lex, base_lex.language, langs.new_persian_latn_)


@register("transcribe", langs.new_persian_aran_)
def transcribe_persian_aran(base_lex: LexemeLike, sense: LexemeSenseLike) -> Iterator[CatenaZipper]:
    """Transcription function for New Persian in the Arabic script.

    Args:
        base_lex: Lexeme containing a target sense for a concept.
        sense: The target sense in question.

    Yields:
        CatenaZipper reflecting the transcription of a term from another language.
    """
    if base_lex.language == langs.ar_:
        yield transform_lexeme_lemma(sense, base_lex, langs.ar_, langs.new_persian_aran_)
    elif base_lex.language == langs.tr_:
        yield transform_lexeme_lemma(sense, base_lex, langs.ota_, langs.new_persian_aran_)


@register("transcribe", langs.new_persian_hebr_)
def transcribe_persian_hebr(base_lex: LexemeLike, sense: LexemeSenseLike) -> Iterator[CatenaZipper]:
    """Transcription function for New Persian in the Hebrew script.

    Args:
        base_lex: Lexeme containing a target sense for a concept.
        sense: The target sense in question.

    Yields:
        CatenaZipper reflecting the transcription of a term from another language.
    """
    if base_lex.language == langs.he_:
        yield transform_lexeme_lemma(sense, base_lex, langs.he_, langs.new_persian_hebr_)


@register("transcribe", langs.new_persian_cyrl_)
def transcribe_persian_cyrl_(base_lex: LexemeLike, sense: LexemeSenseLike) -> Iterator[CatenaZipper]:
    """Transcription function for New Persian in the Cyrillic script.

    Args:
        base_lex: Lexeme containing a target sense for a concept.
        sense: The target sense in question.

    Yields:
        CatenaZipper reflecting the transcription of a term from another language.
    """
    if base_lex.language in [langs.ru_, langs.uk_]:
        yield transform_lexeme_lemma(sense, base_lex, base_lex.language, langs.new_persian_cyrl_)
