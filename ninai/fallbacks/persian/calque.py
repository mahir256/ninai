"""Holds functions for generating calques in Persian from Catenae for their elements."""

import tfsl.interfaces as tfsli
import udiron.base.constants as c
import udiron.langs.persian.constants as cfa
from tfsl import langs
from udiron import C_, CatenaZipper, FunctionConfig

from ninai.graph.client import find_sense
from ninai.renderers import register


@register("binominal_compound_calque", langs.new_persian_aran_)
def ecc_persian(
        base: CatenaZipper,
        attribute: CatenaZipper,
        target_rel: tfsli.Qid,
) -> CatenaZipper:
    """Assembles an endocentric compound in Persian.

    * ⓘ Q127651975 § 4.4 🗏 51

    Args:
        base: Base of the compound.
        attribute: Modifier of the compound.
        target_rel: Syntactic relationship between base and modifier.

    Returns:
        Compound combining base and attribute.
    """
    linking_lexeme = find_sense(cfa.e_ezafe, base.get_language(), FunctionConfig.new())
    linking_lexeme = linking_lexeme.add_to_config(C_.compounding(c.backwards))
    attribute = attribute.attach_left_of_root(linking_lexeme, c.adposition)
    output = base.attach_rightmost(attribute, target_rel)
    return output
