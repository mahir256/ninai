"""Holds functions for generating calques in Norwegian from Catenae for their elements."""

import tfsl.interfaces as tfsli
import udiron.base.constants as c
import udiron.langs.norwegian.constants as cno
from tfsl import langs
from udiron import C_, CatenaZipper, FunctionConfig

from ninai.graph.client import find_sense
from ninai.renderers import register


@register("binominal_compound_calque", langs.nb_)
def ecc_norwegian(
        base: CatenaZipper,
        attribute: CatenaZipper,
        target_rel: tfsli.Qid,
) -> CatenaZipper:
    """Assembles an endocentric compound in Norwegian.

    * ⓘ Q117978803 § 11.2 🗏 458

    Args:
        base: Base of the compound.
        attribute: Modifier of the compound.
        target_rel: Syntactic relationship between base and modifier.

    Returns:
        Compound combining base and attribute.
    """
    attribute = attribute.add_to_config(C_.compounding(c.forwards))
    if target_rel == c.gen_attribute:
        linking_lexeme = find_sense(cno.linking_s, base.get_language(), FunctionConfig.new())
        linking_lexeme = linking_lexeme.add_to_config(C_.compounding(c.forwards))
        attribute = attribute.attach_right_of_root(linking_lexeme, c.adposition)
    output = base.attach_leftmost(attribute, target_rel)
    return output
