"""Holds functions for generating transcriptions into Norwegian from other languages' lexemes."""

import logging
from typing import Iterator

import udiron.base.constants as c
from tfsl import (Lexeme, LexemeForm, LexemeLike, LexemeSenseLike,
                  MonolingualText, langs)
from udiron import (Catena, CatenaConfig, CatenaZipper, DependentEntry,
                    DependentList)

from ninai.renderers import register

logger = logging.getLogger(__name__)

def transform_et_text(text: MonolingualText) -> str:
    """Performs some very naive substitutions on Estonian text to make it look like Norwegian.

    Args:
        text: Some Estonian text.

    Returns:
        Transformed Estonian text.
    """
    return text.text.replace("ä", "æ").replace("õ", "a").replace("ü", "y").replace("š", "sj")


def transform_et_lexeme_lemma(sense: LexemeSenseLike, base_lex: LexemeLike) -> CatenaZipper:
    """Transforms the lemma of an Estonian lexeme into something resembling Norwegian.

    Args:
        sense: Sense of the lexeme in question.
        base_lex: The lexeme itself.

    Returns:
        CatenaZipper with a Norwegian pseudo-lexeme for the concept represented by the sense.
    """
    logger.info("Transforming lemma of %s to provide equivalent in Norwegian", sense.id)
    transcription = transform_et_text(base_lex[langs.et_])
    pseudoform = LexemeForm(transcription @ langs.nb_)
    pseudolexeme = Lexeme(transcription @ langs.nb_, langs.nb_, base_lex.category, [], [base_lex.get_senses()[0].to_editable()], [pseudoform])
    return CatenaZipper([
        DependentEntry(
            Catena(pseudolexeme, langs.nb_, sense, frozenset(), CatenaConfig.new(), DependentList([], []), Catena.next_id()),
            c.syntactic_root,
        ),
    ])


@register("transcribe", langs.nb_)
def transcribe_nb(base_lex: LexemeLike, sense: LexemeSenseLike) -> Iterator[CatenaZipper]:
    """Transcription function for Norwegian.

    Args:
        base_lex: Lexeme containing a target sense for a concept.
        sense: The target sense in question.

    Yields:
        CatenaZipper reflecting the transcription of a term from another language.
    """
    if base_lex.language == langs.et_:
        yield transform_et_lexeme_lemma(sense, base_lex)
