"""Holds functions related to accessing Wikidata's Linked Data Fragments endpoint."""

from typing import List, Optional

import requests


def run_ldf_request(
        subj: Optional[str] = None,
        pred: Optional[str] = None,
        obj: Optional[str] = None,
        base_url: str = "https://query.wikidata.org/bigdata/ldf",
        max_pages: int = 5,
) -> List[str]:
    """Calls out to the Wikidata Query Service's Linked Data Fragments endpoint.

    Args:
        subj: Subject to search for.
        pred: Predicate to search for.
        obj: Object to search for.
        base_url: Path to the LDF endpoint.
        max_pages: Number of pages of results to return.

    Returns:
        List of entries returned by the LDF endpoint.
    """
    params = {
        "subject": subj or "",
        "predicate": pred or "",
        "object": obj or "",
    }
    headers = {
        "Accept": "application/ld+json",
    }
    response = requests.get(base_url, params=params, headers=headers, timeout=60)
    response_json = response.json()

    pages = 1
    subjects = []
    continuation_information = {}

    while pages < max_pages:
        for entry in response_json["@graph"]:
            if entry["@id"] in ["_:b0", "_:b1", "_:b2", "_:b3", "https://query.wikidata.org/bigdata/ldf#dataset"]:
                continue
            elif "https://query.wikidata.org/bigdata/ldf?" in entry["@id"]:
                continuation_information = entry
            else:
                subjects.append(entry)
        if "nextPage" in continuation_information:
            response = requests.get(continuation_information["nextPage"], headers=headers, timeout=60)
            response_json = response.json()
        else:
            break
    return subjects
