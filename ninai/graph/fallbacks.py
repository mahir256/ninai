"""Holds functionality related to the fallback process should a sense not be found in a particular language."""

import logging
from collections import defaultdict
from typing import Optional

import tfsl.interfaces as tfsli
import udiron.base.constants as c
import udiron.base.interfaces as udironi
from tfsl import L, Language
from udiron import C_, CatenaZipper, FunctionConfig, zip_lid
from udiron.base.catena import build_dependents
from udiron.base.dependententry import DependentEntry
from udiron.base.functionconfig import StringSet

logger = logging.getLogger(__name__)


def add_to_exclusions(config: FunctionConfig, thing: str) -> FunctionConfig:
    """Adds an object to the "exclusion" set of the provided config.

    Args:
        config: Initial function configuration.
        thing: Thing to add to the exclusion set.

    Returns:
        FunctionConfig reflecting the added exclusion.
    """
    exclusions = config.get(C_.exclusion)
    if exclusions is not None:
        if isinstance(exclusions, StringSet):
            new_exclusions = exclusions.append(thing)
        else:
            new_exclusions = StringSet(frozenset([thing]))
    else:
        new_exclusions = StringSet(frozenset([thing]))
    return config.set(C_.exclusion, new_exclusions)


def expand_with_sense(sense: tfsli.LSid, language: Optional[Language] = None) -> CatenaZipper:
    """Version of CatenaZipper.expand that takes advantage of information in 'combines lexeme' statements present in senses if they exist.

    If it becomes a convention to put sense-specific combination information on sense-level 'combines lexemes' statements,
    then the implementation of udiron.Catena.expand will be replaced with this function.

    Args:
        sense: Sense whose lexeme should be expanded.
        language: Language to apply to the resultant Catena and its dependents.

    Returns:
        CatenaZipper for the root of the dependency syntax tree represented by the "combines lexeme" statements.

    Todo:
        at this point, merge into udiron.Catena.expand
    """
    new_zipper = zip_lid(sense)
    current_catena, current_rel = new_zipper.top()
    current_sense = current_catena.sense

    components = current_catena.lexeme[c.combines_lexeme]
    sense_components = current_sense[c.combines_lexeme]
    if len(components) == 0:
        return new_zipper

    base_catena_id = current_catena.id
    base_tracking = current_catena.config.tracking
    root_index = 0
    catena_objects = {}
    left_dependents = defaultdict(list)
    right_dependents = defaultdict(list)

    for component in components:
        current_lexeme_id = component.get_itemvalue()
        current_lexeme = L(current_lexeme_id)

        index_qualifier = component.qualifiers[c.series_ordinal][0]
        index = int(index_qualifier.get_str())
        try:
            sense_component = next(c for c in sense_components if c.qualifiers[c.series_ordinal][0] == index_qualifier.get_str())
        except StopIteration:
            sense_component = None

        object_inflections: udironi.InflectionSet = frozenset()
        if object_form_qualifier := component.qualifiers[c.object_form]:
            object_form_id = object_form_qualifier[0].get_itemvalue().get_lfid()
            object_form = current_lexeme[object_form_id]
            object_inflections = frozenset(object_form.features)

        try:
            if sense_component is not None:
                try:
                    object_sense_id = sense_component.qualifiers[c.object_sense][0].get_itemvalue().get_lsid()
                except (IndexError, KeyError):
                    object_sense_id = component.qualifiers[c.object_sense][0].get_itemvalue().get_lsid()
            else:
                object_sense_id = component.qualifiers[c.object_sense][0].get_itemvalue().get_lsid()
            object_sense = L(object_sense_id)[object_sense_id]
        except (IndexError, KeyError):
            object_sense = None

        syntactic_parent_qualifier = component.qualifiers[c.head_position][0]
        syntactic_parent = int(syntactic_parent_qualifier.get_str())
        if syntactic_parent == 0:
            root_index = index

        syntactic_relationship_value = component.qualifiers[c.head_relationship][0]
        syntactic_relationship = syntactic_relationship_value.get_itemvalue().get_qid()

        catena_objects[index] = (current_lexeme, object_sense, object_inflections, syntactic_parent, syntactic_relationship)
        if index < syntactic_parent:
            left_dependents[syntactic_parent].append(index)
        else:
            right_dependents[syntactic_parent].append(index)

    new_catena, _ = build_dependents(catena_objects, left_dependents, right_dependents, root_index, base_catena_id, current_sense.id, language, base_tracking)
    new_entry = DependentEntry(new_catena, current_rel)
    return CatenaZipper([new_entry] + new_zipper.zipper[1:])
