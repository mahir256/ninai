"""Holds logic for traversing sense networks using NetworkX."""

import cmd
import configparser
import os
import threading
import time
import xmlrpc.client
import defusedxml.xmlrpc
from functools import lru_cache
from pathlib import Path
from typing import Any, Callable, Dict, List, Optional, Sequence, Tuple, Union
from urllib.parse import urlparse
from xmlrpc.server import SimpleXMLRPCServer

import networkx as nx
import requests
import SPARQLWrapper
import tfsl.interfaces as tfsli
import udiron.base.constants as c

import ninai.base.interfaces as i

defusedxml.xmlrpc.monkey_patch()

sense_to_sense_properties = [c.translation, c.synonym, c.antonym, c.troponym_of, c.hyperonym, c.pertainym]
sense_to_item_properties = [c.item_for_this_sense, c.predicate_for, c.demonym_of]
lexeme_to_lexeme_properties = [c.derived_from_lexeme]
target_property_list = sense_to_item_properties + sense_to_sense_properties + lexeme_to_lexeme_properties

PREFIX_WD = "http://www.wikidata.org/entity/"
PREFIX_WDT = "http://www.wikidata.org/prop/direct/"
WDQS_ENDPOINT = "https://query.wikidata.org/sparql"

QUERY_PROLOGUE = "SELECT ?s ?slang ?p ?o ?olang {"
QUERY_EPILOGUE = "}"
SUBJECT_SENSE_LANG = "?s ^ontolex:sense/dct:language ?slang ."
OBJECT_SENSE_LANG = "?o ^ontolex:sense/dct:language ?olang ."
SUBJECT_LEXEME_LANG = "?s dct:language ?slang ."
OBJECT_LEXEME_LANG = "?o dct:language ?olang ."
SENSE_TO_SENSE_WDTS = " ".join(["wdt:" + prop for prop in sense_to_sense_properties])
SENSE_TO_SENSE_VALUES = "VALUES ?p { " + SENSE_TO_SENSE_WDTS + " }"
SENSE_TO_ITEM_WDTS = " ".join(["wdt:" + prop for prop in sense_to_item_properties])
SENSE_TO_ITEM_VALUES = "VALUES ?p { " + SENSE_TO_ITEM_WDTS + " }"
LEXEME_TO_LEXEME_WDTS = " ".join(["wdt:" + prop for prop in lexeme_to_lexeme_properties])
LEXEME_TO_LEXEME_VALUES = "VALUES ?p { " + LEXEME_TO_LEXEME_WDTS + " }"
SUBJECT_ITEM_LANG = "BIND(wd:" + c.wikidatan + " as ?slang)"
OBJECT_ITEM_LANG = "BIND(wd:" + c.wikidatan + " as ?olang)"


def read_config_server() -> Tuple[str, float, str, int]:
    """Reads the config file residing at /path/to/ninai/config.ini.

    Returns:
        The cache path, time to live for the cache, graph server host, and graph server port.
    """
    config = configparser.ConfigParser()
    current_config_path = (Path(__file__).parent / "../../config.ini").resolve()
    config.read(current_config_path)
    cpath = config["Ninai"]["CachePath"]
    ttl = float(config["Ninai"]["TimeToLive"])
    server_path = urlparse(config["Ninai"]["GraphServerURL"])
    return cpath, ttl, server_path


cache_path, time_to_live, server_path = read_config_server()
time_to_live = time_to_live * 1000000000
os.makedirs(cache_path, exist_ok=True)


def reformat_result(i: str, ilang: str, prop: str, j: str, jlang: str) -> Tuple[str, str, str, str, str]:
    """Strips provided strings of their RDF prefixes.

    Args:
        i: Subject of a statement.
        ilang: Language of that subject.
        prop: Predicate of a statement.
        j: Object of a statement.
        jlang: Language of that object.

    Returns:
        Appropriately modified strings.
    """
    return (
        i.removeprefix(PREFIX_WD),
        ilang.removeprefix(PREFIX_WD),
        prop.removeprefix(PREFIX_WDT),
        j.removeprefix(PREFIX_WD),
        jlang.removeprefix(PREFIX_WD),
    )


def lexeme_query(
    ls: List[tfsli.Lid],
    exclusions: Optional[List[tfsli.Lid]] = None,
) -> str:
    """Assembles a breadth-first search step when the origins are a list of lexemes.

    Args:
        ls: Origin lexeme IDs.
        exclusions: List of lexemes to not return in the output.

    Returns:
        SPARQL query string for the breadth-first search step.
    """
    forward_direction_parts = [
        "VALUES ?s { " + " ".join(["wd:" + l for l in ls]) + " }",
        LEXEME_TO_LEXEME_VALUES,
        "?s ?p ?o .",
        SUBJECT_LEXEME_LANG,
        OBJECT_LEXEME_LANG,
    ]
    if exclusions is not None:
        forward_direction_parts.append(
            "filter(?o not in (" + ",".join([
                "wd:" + ex for ex in exclusions
            ]) + "))",
        )
    forward_direction = "\n".join(forward_direction_parts)

    reverse_direction_parts = [
        "VALUES ?s { " + " ".join(["wd:" + l for l in ls]) + " }",
        LEXEME_TO_LEXEME_VALUES,
        "?o ?p ?s .",
        SUBJECT_LEXEME_LANG,
        OBJECT_LEXEME_LANG,
    ]
    if exclusions is not None:
        reverse_direction_parts.append(
            "filter(?s not in (" + ",".join([
                "wd:" + ex for ex in exclusions
            ]) + "))",
        )
    reverse_direction = "\n".join(reverse_direction_parts)

    both_directions = forward_direction + "\n} UNION {\n" + reverse_direction
    lexeme_query = "\n".join([
        QUERY_PROLOGUE,
        "{",
        both_directions,
        "}",
        QUERY_EPILOGUE,
    ])
    return lexeme_query


def sense_query(
    ses: List[tfsli.LSid],
    item_exclusions: Optional[List[tfsli.Qid]] = None,
    sense_exclusions: Optional[List[tfsli.LSid]] = None,
) -> str:
    """Assembles a breadth-first search step when the origins are a list of senses.

    Args:
        ses: Origin sense IDs.
        item_exclusions: List of items to not return in the output.
        sense_exclusions: List of senses not to return in the output.

    Returns:
        SPARQL query string for the breadth-first search step.
    """
    sense_subquery_parts = [
        "VALUES ?s { " + " ".join(["wd:" + s for s in ses]) + " }",
        SENSE_TO_SENSE_VALUES,
        "?s ?p ?o .",
        SUBJECT_SENSE_LANG,
        OBJECT_SENSE_LANG,
    ]
    if sense_exclusions is not None:
        sense_subquery_parts.append(
            "filter(?o not in (" + ",".join([
                "wd:" + ex for ex in sense_exclusions
            ]) + "))",
        )
    sense_subquery = "\n".join(sense_subquery_parts)

    item_subqueries = []
    for prop in sense_to_item_properties:
        item_subquery_parts = [
            "VALUES ?s_ { " + " ".join(["wd:" + s for s in ses]) + " }",
            "BIND(wdt:" + prop + " as ?p) bind(wd:" + c.wikidatan + " as ?olang)",
            "?s_ wdt:" + prop + " ?o . ?s wdt:" + prop + " ?o .",
            SUBJECT_SENSE_LANG,
        ]
        if sense_exclusions is not None:
            item_subquery_parts.append(
                "filter(?s not in (" + ",".join([
                    "wd:" + ex for ex in sense_exclusions
                ]) + "))",
            )
        if item_exclusions is not None:
            item_subquery_parts.append(
                "filter(?o not in (" + ",".join([
                    "wd:" + ex for ex in item_exclusions
                ]) + "))",
            )
        item_subqueries.append("\n".join(item_subquery_parts))
    all_item_subqueries = "\n} UNION {\n".join(item_subqueries)
    return "\n".join([
        QUERY_PROLOGUE,
        "{",
        sense_subquery,
        "} UNION {",
        all_item_subqueries,
        "}",
        QUERY_EPILOGUE,
    ])


def item_query(o: tfsli.Qid) -> str:
    """Assembles a breadth-first search step when the origin is an item.

    Args:
        o: Origin item ID.

    Returns:
        SPARQL query string for the breadth-first search step.
    """
    return "\n".join([
        QUERY_PROLOGUE,
        SENSE_TO_ITEM_VALUES,
        "BIND(wd:" + o + " as ?o)",
        "?s ?p ?o .",
        SUBJECT_SENSE_LANG,
        OBJECT_ITEM_LANG,
        QUERY_EPILOGUE,
    ])


def run_refresh_query(
    o: Union[tfsli.Qid, tfsli.LSid],
    retrieval_times: Dict[Union[tfsli.Qid, tfsli.LSid], int],
) -> Tuple[List[Union[tfsli.Qid, tfsli.LSid]], List[Tuple[str, str, str, str, str]], Dict[Union[tfsli.Qid, tfsli.LSid], int]]:
    """Performs a breadth-first search in as few queries as possible from a given entity.

    Args:
        o: Origin of the search.
        retrieval_times: Mapping from entity IDs to when those entities were last retrieved.

    Returns:
        List of retrieved entities, list of edges between those entities, and revised retrieval time list.
    """
    wdqs = SPARQLWrapper.SPARQLWrapper(WDQS_ENDPOINT, agent="ninai 0.0.1")
    wdqs.setReturnFormat(SPARQLWrapper.JSON)
    wdqs.setMethod(SPARQLWrapper.POST)
    wdqs.setTimeout(5)

    item_exclusions = set()
    sense_exclusions = set()

    if tfsli.is_qid(o):
        refreshed_query = item_query(o)
    elif tfsli.is_lid(o):
        refreshed_query = lexeme_query([o])
        sense_exclusions.add(o)
    else:
        refreshed_query = sense_query([o])
        sense_exclusions.add(o)

    all_entities = set()
    all_results = []

    while True:
        new_ses = set()
        wdqs.setQuery(refreshed_query)
        query_time = 0
        while True:
            try:
                relations_result = wdqs.query().convert()
                query_time = time.time_ns()
            except requests.exceptions.ConnectTimeout:
                time.sleep(1)
                continue
            else:
                break
        num_results = len(relations_result["results"]["bindings"])
        if num_results == 0:
            break
        for relation in relations_result["results"]["bindings"]:
            s_, slang_, p_, o_, olang_ = reformat_result(
                relation["s"]["value"],
                relation["slang"]["value"],
                relation["p"]["value"],
                relation["o"]["value"],
                relation["olang"]["value"],
            )
            all_results.append((s_, slang_, p_, o_, olang_))
            all_entities.update([s_, o_])
            retrieval_times[s_] = query_time
            retrieval_times[o_] = query_time
            if tfsli.is_lid(s_):
                if o_ not in sense_exclusions:
                    new_ses.add(o_)
                sense_exclusions.add(s_)
            else:
                if s_ == o:
                    sense_exclusions.add(s_)
                else:
                    if p_ in sense_to_item_properties:
                        item_exclusions.add(o_)
                        new_ses.add(s_)
                    else:
                        new_ses.add(o_)
                sense_exclusions.add(s_)
        if tfsli.is_lid(o):
            refreshed_query = lexeme_query(list(new_ses), list(sense_exclusions))
        else:
            refreshed_query = sense_query(list(new_ses), list(item_exclusions), list(sense_exclusions))
    return list(all_entities), all_results, retrieval_times


def update_total_graph(graph: nx.MultiDiGraph, entities: Sequence[tfsli.EntityId], properties: List[tfsli.Pid], results: List[Tuple[str, str, str, str, str]]) -> nx.MultiDiGraph:
    """Turns a list of relationships into a list of directed subgraphs.

    Args:
        graph: Initial total graph.
        entities: List of entities to update in the graph.
        properties: List of properties between those entities to remove in the graph.
        results: List of edges to add to the graph.

    Returns:
        Updated total graph.
    """
    removed_edges = [(u, v, k) for u, v, k, pred in graph.edges(entities, keys=True, data="pred") if pred in properties]
    graph.remove_edges_from(removed_edges)
    for i_, ilang_, prop_, j_, jlang_ in results:
        graph.add_node(i_, lang=ilang_)
        graph.add_node(j_, lang=jlang_)
        graph.add_edge(i_, j_, pred=prop_)
        if jlang_ == c.wikidatan:
            graph.add_edge(j_, i_, pred=prop_)
        elif prop_ == c.derived_from_lexeme:
            graph.add_edge(j_, i_, pred=c.derived_lexeme_label_item)
    return graph


def filter_function(total_graph: nx.MultiDiGraph, allowed_properties: List[tfsli.Pid]) -> Callable[[str, str, int], bool]:
    """Provides a function to check whether an edge between two nodes in the graph is one of the provided allowed properties.

    Args:
        total_graph: Graph of all currently known entities.
        allowed_properties: Properties to be filtered for.

    Returns:
        Filter function to check whether an edge between two nodes in the graph is one of the provided allowed properties.
    """
    def filter_function_out(node1: str, node2: str, key: int) -> bool:
        """Checks whether the predicate of an edge is in a set of allowed properties.

        Args:
            node1: Origin of an edge.
            node2: Destination of an edge.
            key: Index of a particular edge between the two nodes.

        Returns:
            Whether the predicate of an edge is in a set of allowed properties.
        """
        return total_graph[node1][node2][key]["pred"] in allowed_properties
    return filter_function_out


def build_translation_network(total_graph: nx.MultiDiGraph, property_list: List[tfsli.Pid]) -> nx.MultiDiGraph:
    """Composes a set of subgraphs into a single unified graph.

    Args:
        total_graph: Graph of all currently known entities.
        property_list: Properties to be filtered for.

    Returns:
        Subgraph view of the total graph reflecting only the filtered edges.
    """
    return nx.subgraph_view(total_graph, filter_edge=filter_function(total_graph, property_list))


def build_translation_networks(total_graph: nx.MultiDiGraph) -> Dict[str, nx.MultiDiGraph]:
    """Constructs all translation networks.

    Args:
        total_graph: Graph of all currently known entities.

    Returns:
        Mapping of names of sense subgraphs to the subgraphs themselves.
    """
    predicate_network = build_translation_network(total_graph, [c.predicate_for, c.translation, c.synonym])
    substantive_network = build_translation_network(total_graph, [c.item_for_this_sense, c.translation, c.synonym])
    demonym_network = build_translation_network(total_graph, [c.demonym_of, c.translation, c.synonym])
    origin_network = build_translation_network(total_graph, [c.derived_from_lexeme, c.derived_lexeme_label_item])
    return {
        "predicate": predicate_network,
        "substantive": substantive_network,
        "demonym": demonym_network,
        "origin": origin_network,
    }


def assemble_shortest_paths(network: nx.MultiDiGraph, path_set: Dict[str, List[str]]) -> i.CandidatePathSet:
    """Searches the provided network for all senses in the provided language linked to the provided concept.

    Args:
        network: Sense subgraph to search through.
        path_set: Mapping from senses to the vertices on the shortest path from a concept to those senses.

    Returns:
        Mapping from senses to the shortest paths to those senses.
    """
    candidate_paths: i.CandidatePathSet = {}
    for node, path in path_set.items():
        current_language = network.nodes[node]["lang"]
        pathgraph = nx.path_graph(path)
        new_candidate_path_node: i.CandidatePathNode = {
            "lang": current_language,
            "path": [({**network[s][t][0], "lang": network.nodes[t]["lang"]}, t) for (s, t) in pathgraph.edges()],
        }
        candidate_paths[node] = new_candidate_path_node
    return candidate_paths


class GraphServer:
    """Server providing facilities to query the graph of relations between lexeme senses and items."""

    def __init__(self) -> None:
        """Sets up the graph infrastructure."""
        self.total_graph = nx.MultiDiGraph()
        self.retrieval_times = {}
        self.network_mappings = build_translation_networks(self.total_graph)
        self.get_shortest_paths = lru_cache()(self.get_shortest_paths_uncached)
        self.search_graph_set = lru_cache()(self.search_graph_set_uncached)

    def search_graph_set_uncached(self, concept_in: str, network_in: str) -> i.CandidatePathSet:
        """Searches for the provided concept in the provided subgraph, returning those senses which are in the set of language IDs.

        Args:
            concept_in: Concept to start from.
            network_in: Sense subgraph to search through.

        Returns:
            Mapping of senses to paths from the provided concept to those senses.
        """
        candidate_paths: i.CandidatePathSet
        current_time = time.time_ns()
        if concept_in not in self.total_graph or self.retrieval_times[concept_in] > current_time + time_to_live:
            self.rebuild_entity(concept_in)
        try:
            current_network, shortest_paths = self.get_shortest_paths(concept_in, network_in)
            candidate_paths = assemble_shortest_paths(current_network, shortest_paths)
            if tfsli.is_lsid(concept_in) and concept_in in current_network:
                concept_lang = current_network.nodes[concept_in]["lang"]
                candidate_path_node: i.CandidatePathNode = {"lang": concept_lang, "path": []}
                candidate_paths[concept_in] = candidate_path_node
        except nx.exception.NodeNotFound:
            candidate_paths = {}
        return candidate_paths

    def search_graph(self, concept_in: str, network_in: str) -> i.CandidatePathSet:
        """Searches the provided network for all senses in the provided language linked to the provided concept, but also adding the concept itself if it happens to be a sense in the provided language.

        Args:
            concept_in: Concept to start from.
            network_in: Sense subgraph to search through.

        Returns:
            Mapping of senses to paths from the provided concept to those senses.
        """
        print(concept_in, network_in)
        return self.search_graph_set(concept_in, network_in)

    def get_shortest_paths_uncached(self, concept_in: str, network_in: str) -> Tuple[nx.MultiDiGraph, Dict[str, List[str]]]:
        """Gets the shortest paths between a concept and every other node (in)directly linked to it.

        Args:
            concept_in: Concept to start from.
            network_in: Sense subgraph to search through.

        Returns:
            The sense subgraph itself and the shortest paths from the concept to every other connected node.
        """
        current_network = self.network_mappings[network_in]
        return current_network, nx.shortest_path(current_network, concept_in)

    def rebuild_entity(self, arg: tfsli.Qid) -> str:
        """Rebuilds the set of links in the graph with the given item.

        Args:
            arg: Entity to rebuild the links to.

        Returns:
            Terminal output string indicating success.
        """
        entities, results, self.retrieval_times = run_refresh_query(arg, self.retrieval_times)
        self.total_graph = update_total_graph(self.total_graph, entities, sense_to_sense_properties + sense_to_item_properties, results)
        self.rebuild_other_functions()
        return "Rebuilt " + arg

    def rebuild_other_functions(self) -> None:
        """Common steps to run after a graph modification."""
        self.network_mappings = build_translation_networks(self.total_graph)
        self.get_shortest_paths = lru_cache()(self.get_shortest_paths_uncached)
        self.search_graph_set = lru_cache()(self.search_graph_set_uncached)

    def neighbors_with_prop(self, arg: tfsli.EntityId, prop: tfsli.Pid) -> List[str]:
        """Returns the set of neighbors of the provided concept linked by a given property.

        Args:
            arg: Entity ID to start from.
            prop: Property ID to check edges for.

        Returns:
            List of entity IDs linked to the arg by the prop.
        """
        neighbor_edges = []
        for (_, b, pred) in self.total_graph.edges(arg, data="pred"):
            if pred == prop:
                neighbor_edges.append((b, self.total_graph.nodes[b]["lang"]))
        return neighbor_edges


class ServerThread(threading.Thread):
    """Thread that handles RPC requests to the graph server."""

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        """Sets up the graph server thread.

        Args:
            args: Any positional arguments to threading.Thread.
            kwargs: Any keyword arguments to threading.Thread.
        """
        super().__init__(*args, **kwargs)
        self.graph_server = SimpleXMLRPCServer((server_path.hostname, server_path.port), logRequests=False)
        self.graph_server.register_instance(GraphServer())
        print(f"Listening on port {server_path.port}...")

    def run(self) -> None:
        """Serves the graph server forever."""
        self.graph_server.serve_forever()


class GraphServerRepl(cmd.Cmd):
    """Read-eval-print loop interface to the graph server.

    Attributes:
        intro: Message displayed when the REPL starts.
        prompt: Prompt for the REPL.
        server_thread: Actual graph server being controlled by the REPL.
    """
    intro: str = "The Ninai graph server is now running."
    prompt: str = "graph_server> "
    server_thread: Optional[ServerThread] = None

    def do_rebuild(self, arg: str) -> None:
        """Rebuilds the subgraph of relations with a particular property.

        Usage: rebuild arg

        Args:
            arg: entity in the graph to rebuild relationships for.
        """
        if tfsli.is_qid(arg) or tfsli.is_lsid(arg):
            with xmlrpc.client.ServerProxy(server_path.geturl()) as proxy:
                print(proxy.rebuild_entity(arg))
        else:
            print("Argument to 'rebuild' is not an item, property, or sense ID")

    def do_neighbors(self, arg: str) -> None:
        """Usage: neighbors entity prop

        Args:
            arg: "entity prop" where "entity" and "prop" are an entity/property in the graph.
        """
        try:
            entity, prop = arg.split(" ")
        except ValueError:
            print("An entity and a property must be provided")
            return
        with xmlrpc.client.ServerProxy(server_path.geturl()) as proxy:
            print(proxy.neighbors_with_prop(entity, prop))

    def do_search(self, arg: str) -> None:
        """Usage: search concept network lang_id1 [lang_id2 ...]

        Args:
            arg: "concept network lang_id1 [lang_id2 ...]" where 'concept' is the concept to search for, 'network' is the network in which to search for the concept, and 'lang_id1' onward are languages of results to return from this search.

        Raises:
            TypeError: if the result from a graph search is not a candidate path set.
        """
        concept, network, *language_ids = arg.split(" ")
        with xmlrpc.client.ServerProxy(server_path.geturl()) as proxy:
            results = proxy.search_graph(concept, network)
            if not isinstance(results, dict):
                raise TypeError("A candidate path set was not returned by the graph server")
            for _, path in results.items():
                if path["lang"] not in language_ids:
                    continue
                path_parts = []
                for edge, vertex in reversed(path["path"]):
                    edge_lang = edge["lang"]
                    edge_pred = edge["pred"]
                    path_parts.append(f"{vertex} ({edge_lang}) <-{edge_pred}-")
                path_str = " ".join(path_parts)
                output_str = " ".join([path_str, concept])
                print(output_str)

    def do_EOF(self, arg: str) -> bool:  # noqa: N802
        """Shuts down the Ninai graph server.

        Usage: <Ctrl-D>

        Args:
            arg: Ignored.

        Returns:
            True to signal the shutdown of the graph server.
        """
        print("Shutting down graph server...")
        return True

    def preloop(self) -> None:
        """Instantiates and starts the server thread."""
        self.server_thread = ServerThread(daemon=True)
        self.server_thread.start()


if __name__ == "__main__":
    GraphServerRepl().cmdloop()
