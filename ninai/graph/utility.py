"""Utility functions of particular relevance to searching the sense network."""

import logging
from typing import TYPE_CHECKING, Optional

import udiron.base.constants as c

import ninai.base.interfaces as i
import ninai.graph.client as graph

if TYPE_CHECKING:
    from tfsl import Language, LexemeLike, Statement


logger = logging.getLogger(__name__)

def get_target_language_set_paths(candidate_paths: i.CandidatePathSet, target_language: "Language") -> i.CandidatePathSet:
    """Filters the candidate path set for all senses in the provided target language.

    Args:
        candidate_paths: Current set of paths to draw from.
        target_language: Language range to filter for.

    Returns:
        Appropriately filtered path set.
    """
    language_fallbacks = graph.get_language_range(target_language)
    for lang in language_fallbacks:
        filtered_paths = {k: v for k, v in candidate_paths.items() if v["lang"] == lang}
        if filtered_paths:
            return filtered_paths
    return {}


def get_ipa(base_lex: "LexemeLike") -> Optional["Statement"]:
    """Gets the first IPA statement on the provided lexeme.

    Args:
        base_lex: Lexeme which may have forms with 'IPA transcription' statements.

    Returns:
        First 'IPA transcription' statement on a form of that lexeme.
    """
    for form in base_lex.get_forms():
        if form.haswbstatement(c.ipa_pronunciation):
            logger.info("Found an IPA pronunciation statement on %s", form.id)
            return form[c.ipa_pronunciation][0]
    return None
