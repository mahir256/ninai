"""Holds language-agnostic functionality for producing transcriptions of lexemes."""

import contextlib
import importlib
import logging
import os
from pathlib import Path
from typing import Iterator

import udiron.base.constants as c
from tfsl import L, Language
from udiron import C_, CatenaZipper, FunctionConfig

import ninai.base.interfaces as i
import ninai.graph.client
from ninai.renderers import find_transcribe

fallbacks_subfolder = Path(__file__).resolve().parent.parent.joinpath("fallbacks")
for subdir in os.listdir(fallbacks_subfolder):
    with contextlib.suppress(ImportError):
        importlib.import_module(".".join(["ninai.fallbacks", subdir, "transcribe"]))

logger = logging.getLogger(__name__)

def transcribe(item_in: str, language_in: Language, config_in: FunctionConfig, paths: i.CandidatePathSet) -> Iterator[CatenaZipper]:
    """Returns a transliteration of a statement.

    Args:
        item_in: Concept to generate a Catena for.
        language_in: Language in which to generate the Catena.
        config_in: Additional function configuration.
        paths: Paths from the concept to all senses linked to it.

    Yields:
        Catenae reflecting transliterations of lexemes in other languages.
    """
    logger.info("Attempting transcriptions of other lexemes for %s", item_in)
    for sense, path in paths.items():
        if path["lang"] == c.wikidatan:
            continue

        base_lex = L(sense)
        for output in find_transcribe(language_in)(base_lex, base_lex[sense]):
            current_adjacencies = output.get_config().get(C_.shortest_path_tree) or ""
            adjacencies_to_add = [
                *ninai.graph.client.build_sense_adjacency_list(item_in, path),
            ]
            new_adjacencies = current_adjacencies + "\n" + "\n".join(adjacencies_to_add)
            output = output.add_to_config(C_.shortest_path_tree(new_adjacencies))
            yield output
