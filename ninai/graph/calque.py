"""Holds language-agnostic functionality for producing calques of lexemes."""

import contextlib
import importlib
import logging
import os
from pathlib import Path
from typing import TYPE_CHECKING, Generator, Iterator, List

import tfsl.interfaces as tfsli
import udiron.base.constants as c
from tfsl import L, ItemValue, Language, langs
from udiron.base.functionconfig import C_
from udiron.base.interfaces import get_language_fallbacks
from udiron.langs.mul import zip_language

import ninai.base.interfaces as i
import ninai.graph.client
from ninai.graph.fallbacks import add_to_exclusions, expand_with_sense
from ninai.renderers import find_binominal_compound_calque

if TYPE_CHECKING:
    from udiron import CatenaZipper, FunctionConfig

logger = logging.getLogger(__name__)

fallbacks_subfolder = Path(__file__).resolve().parent.parent.joinpath("fallbacks")
for subdir in os.listdir(fallbacks_subfolder):
    with contextlib.suppress(ImportError):
        importlib.import_module(".".join(["ninai.fallbacks", subdir, "calque"]))

langs_to_light_verbs = {
    langs.bn_: tfsli.Lid("L40018"),
    langs.brh_: tfsli.Lid("L1120885"),
    Language("fa", "Q56356571"): tfsli.Lid("L407237"),
    langs.hi_: tfsli.Lid("L579999"),
    langs.ja_: tfsli.Lid("L830"),
    langs.ko_: tfsli.Lid("L741231"),
    langs.pa_: tfsli.Lid("L45436"),
    langs.sd_: tfsli.Lid("L1094559"),
    langs.tr_: tfsli.Lid("L1158881"),
}
light_verbs = set(langs_to_light_verbs.values())


def calque(item_in: str, language_in: Language, config_in: "FunctionConfig", paths: i.CandidatePathSet) -> Iterator["CatenaZipper"]:
    """Attempts to calque a lexeme into the target language depending on the presence of certain other statements of the lexeme.

    Args:
        item_in: Concept to generate a Catena for.
        language_in: Language to ultimately generate a Catena in.
        config_in: Additional function configuration.
        paths: Paths from item_in to senses in the sense graph.

    Yields:
        Endocentric compounds in the provided language.
    """
    for sense, path in paths.items():
        if path["lang"] == c.wikidatan:
            continue

        base_lex = L(sense)
        is_vns = base_lex.haswbstatement(c.instance_of, ItemValue(c.verbo_nominal_syntagma))
        is_ecc = base_lex.haswbstatement(c.instance_of, ItemValue(c.endocentric_compound))
        if is_vns or is_ecc:
            try:
                expanded_lex = expand_with_sense(sense)
            except Exception:
                logger.exception("Error expanding %s (%s)", sense, path["lang"])
                continue
            if is_vns:
                logger.info('Attempting verbo-nominal syntagma calque of %s (%s)', sense, path["lang"])
                yield from verbo_nominal_syntagma_calque(sense, expanded_lex, language_in, config_in)
                logger.warning("No further calques possible from %s (%s) into %s", sense, path["lang"], language_in)
            elif is_ecc:
                path_adjacency_list = ninai.graph.client.build_sense_adjacency_list(item_in, path)
                logger.info('Attempting endocentric compound calque of %s (%s)', sense, path["lang"])
                yield from endocentric_compound_calque(sense, expanded_lex, language_in, config_in, path_adjacency_list)
                logger.warning("No further calques possible from %s (%s) into %s", sense, path["lang"], language_in)


def verbo_nominal_syntagma_calque(sense: tfsli.LSid, zipper_in: "CatenaZipper", language_in: Language, config_in: "FunctionConfig") -> Generator["CatenaZipper", None, None]:
    """Tries to calque a lexeme marked as a verbo-nominal syntagma.

    Note that this is called only when the lexeme has the top-level statement "instance of: verbo-nominal syntagma".

    Args:
        sense: Sense to generate an equivalent Catena for.
        zipper_in: Expanded Catena for that sense in the language of that sense.
        language_in: Language to ultimately generate a Catena in.
        config_in: Additional function configuration.

    Returns:
        None if a calque cannot currently be created.

    Yields:
        Verbo-nominal syntagmata in the provided language.

    Todo:
        what of syntagmata with more than two "combines" statements?
    """
    nominal_element, verbal_element = None, None
    if zipper_in.has_rel(c.object_complement):
        nominal_element = zipper_in.rel(c.object_complement).get_sense().id
        verbal_element = zipper_in.get_sense().id
    elif zipper_in.has_rel(c.light_verb_construction):
        nominal_element = zipper_in.rel(c.light_verb_construction).get_sense().id
        verbal_element = zipper_in.get_sense().id
    else:
        yield

    if nominal_element:
        new_nominals = ninai.graph.client.find_senses(nominal_element, language_in, add_to_exclusions(config_in, sense))
    else:
        yield

    if verbal_element in light_verbs:
        for lang in get_language_fallbacks(language_in):
            try:
                new_verbals = [zip_language(langs_to_light_verbs[lang], language_in)]
            except KeyError:
                continue
    else:
        if verbal_element:
            new_verbals = [ninai.graph.client.find_predicate(verbal_element, language_in, add_to_exclusions(config_in, sense))]
        else:
            yield

    for new_nominal in new_nominals:
        for new_verbal in new_verbals:
            new_verbal = new_verbal.attach_left_of_root(new_nominal, c.light_verb_construction)
            new_verbal = new_verbal.add_to_config(C_.thematic_relation(sense))
            yield new_verbal


def endocentric_compound_calque(
    sense: tfsli.LSid,
    zipper_in: "CatenaZipper",
    language_in: Language,
    config_in: "FunctionConfig",
    path_adjacency_list: List[str],
) -> Iterator["CatenaZipper"]:
    """Tries to calque a lexeme marked as an endocentric compound.

    Note that this is called only when the lexeme has the top-level statement "instance of: endocentric compound".

    Args:
        sense: Sense to generate an equivalent Catena for.
        zipper_in: Expanded Catena for that sense in the language of that sense.
        language_in: Language to ultimately generate a Catena in.
        config_in: Additional function configuration.
        path_adjacency_list: Adjacency list representation of the path from a particular concept to the provided sense.

    Returns:
        None if a calque cannot currently be created.

    Yields:
        Endocentric compounds in the provided language.
    """
    attribute, base = None, None
    target_rel = c.nom_attribute

    for rel in [c.nom_attribute, c.adj_attribute, c.gen_attribute]:
        if zipper_in.has_rel(rel):
            attribute = zipper_in.rel(rel)
            base = zipper_in
            target_rel = rel

    if base is not None and attribute is not None:
        modified_exclusion_config = add_to_exclusions(config_in, sense)

        attribute_found = attribute.get_sense().id
        attribute_sense_ids = [attribute_found]
        if attribute.get_config().get(C_.underspecification) is not None:
            attribute_sense_ids = [attribute_sense.id for attribute_sense in attribute.get_lexeme().get_senses()]

        base_found = base.get_sense().id
        base_sense_ids = [base_found]
        lexeme_id = tfsli.get_lid_string(sense)
        base_hyperonym_list = L(lexeme_id)[sense][c.hyperonym]
        if len(base_hyperonym_list) > 0:
            first_hyperonym = base_hyperonym_list[0].get_itemvalue().get_lsid()
            if tfsli.get_lid_string(first_hyperonym) == tfsli.get_lid_string(base_found):
                base_sense_ids = [first_hyperonym]
        elif base.get_config().get(C_.underspecification) is not None:
            base_sense_ids = [base_sense.id for base_sense in base.get_lexeme().get_senses()]

        for attribute_element in attribute_sense_ids:
            for new_attribute in ninai.graph.client.find_senses(attribute_element, language_in, modified_exclusion_config):
                for base_element in base_sense_ids:
                    for new_base in ninai.graph.client.find_senses(base_element, language_in, modified_exclusion_config):
                        output = find_binominal_compound_calque(language_in)(new_base, new_attribute, target_rel)
                        current_adjacencies = output.get_config().get(C_.shortest_path_tree)
                        adjacencies_to_add = [
                            *path_adjacency_list,
                            f"{zipper_in.get_config().get(C_.combined_in_lexeme)} {attribute_element} P5238",
                            f"{zipper_in.get_config().get(C_.combined_in_lexeme)} {base_element} P5238",
                        ]
                        new_adjacencies = current_adjacencies + "\n" + "\n".join(adjacencies_to_add)
                        output = output.add_to_config(C_.shortest_path_tree(new_adjacencies))
                        yield output
