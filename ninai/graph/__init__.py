"""Holds functionality related to traversing the sense graph."""

import logging
from collections import defaultdict
from typing import DefaultDict, Iterator, List, Sequence

import tfsl.interfaces as tfsli
import udiron.base.constants as c
from tfsl import Q, ItemLike, Language, Lexeme, LexemeForm, LexemeSense, langs
from udiron import CatenaZipper, FunctionConfig, zip_lexeme
from udiron.base.interfaces import get_language_fallbacks

import ninai.base.interfaces as i
import ninai.graph.constructorify as ngc

logger = logging.getLogger(__name__)


def catena_from_item(item_in: ItemLike, language_in: Language, config_in: FunctionConfig) -> CatenaZipper:
    """Creates a Catena from the provided Item, using the label of that item in the provided language.

    Args:
        item_in: Item ID to draw a label from.
        language_in: Language of the label to use.
        config_in: Additional function configuration.

    Returns:
        Catena wrapping the label of the item.
    """
    try:
        target_label = item_in.get_label(language_in)
        logger.warning("Using item label as equivalent of %s in language %s", item_in.id, language_in)
    except KeyError:
        logger.warning("Using item ID as equivalent of %s in language %s", item_in.id, language_in)
        target_label = item_in.id
    return zip_lexeme(
        Lexeme(
            target_label @ language_in,
            language_in,
            c.proper_noun,
            forms=[LexemeForm(target_label @ language_in)],
            senses=[LexemeSense(target_label @ language_in)],
        ),
    )


def catena_from_item_id(item_in: str, language_in: Language, config_in: FunctionConfig, paths: i.CandidatePathSet) -> Sequence[CatenaZipper]:
    """Creates a Catena from the provided Qid.

    Args:
        item_in: Item ID to draw a label from.
        language_in: Language of the label to use.
        config_in: Additional function configuration.
        paths: List of paths from a concept to all linked senses.

    Returns:
        List containing a single Catena wrapping the label of the item.

    Todo:
        for names, have a utility function to assemble from name lexemes?
    """
    if not tfsli.is_qid(item_in):
        return []
    return [catena_from_item(Q(item_in), language_in, config_in)]


__fallback_chains__: DefaultDict[Language, DefaultDict[str, List[i.SenseFindingFallback]]] = defaultdict(lambda: defaultdict(list))
__fallback_chains__[langs.mul_]["substantive"] = [ngc.constructorify, catena_from_item_id]


def get_fallback_chain(lang: Language, network_in: str) -> List[i.SenseFindingFallback]:
    """Retrieves a list of i.SenseFindingFallbacks for a given language.

    Args:
        lang: Language to find fallbacks for.
        network_in: Particular sense subgraph to whose senses these will be applied.

    Returns:
        List of fallbacks to apply.
    """
    return __fallback_chains__[lang][network_in]


def find_fallback_chain(language_in: Language, network_in: str) -> List[i.SenseFindingFallback]:
    """Special case of get_fallback_chain where the language falls back to mul (Q20923490).

    Args:
        language_in: Language to find fallbacks for.
        network_in: Particular sense subgraph to whose senses these will be applied.

    Returns:
        List of fallbacks to apply.

    Raises:
        KeyError: if no fallback chain exists for a network-language combination.
    """
    for language_to_try in get_language_fallbacks(language_in):
        if (chain := get_fallback_chain(language_to_try, network_in)):
            return chain
        continue
    raise KeyError(f"No fallback chain for {network_in} in language {language_in}")


def add_to_fallback_chain(language_in: Language, network_in: str, sense_fallback: i.SenseFindingFallback) -> None:
    """Adds a language to the fallback chain used when searching for a sense in a particular language and particular network.

    Args:
        language_in: Language whose fallback chain is being modified.
        network_in: Sense subgraph to which this fallback chain applies.
        sense_fallback: Fallback function to add to the chain.
    """
    if __fallback_chains__[language_in][network_in]:
        __fallback_chains__[language_in][network_in] = [sense_fallback] + __fallback_chains__[language_in][network_in]
    else:
        __fallback_chains__[language_in][network_in] = [sense_fallback] + __fallback_chains__[langs.mul_][network_in]


def run_fallbacks(concept: str, network: str, lang: Language, config: FunctionConfig, paths: i.CandidatePathSet) -> Iterator[CatenaZipper]:
    """Tries each fallback defined for a particular language in a particular network.

    Args:
        concept: Concept to generate a Catena for.
        network: Sense subgraph being searched for an equivalent.
        lang: Language in which to generate the Catena.
        config: Additional function configuration.
        paths: Paths from concept to senses in the sense subgraph.

    Yields:
        Catenae from performing fallback functions defined for the given language.
    """
    fallback_chain = find_fallback_chain(lang, network)
    for fallback in fallback_chain:
        yield from fallback(concept, lang, config, paths)
