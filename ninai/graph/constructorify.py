"""Holds the basic method used to choose a Constructorifier."""

import contextlib
import importlib
import os
from pathlib import Path
from typing import Iterator

import tfsl.interfaces as tfsli
from tfsl import L, Language
from udiron import CatenaZipper, FunctionConfig, zip_lid

import ninai.base.interfaces as i
from ninai.renderers import find_constructorify

fallbacks_subfolder = Path(__file__).resolve().parent.parent.joinpath("fallbacks")
for subdir in os.listdir(fallbacks_subfolder):
    with contextlib.suppress(ImportError):
        importlib.import_module(".".join(["ninai.fallbacks", subdir, "constructorify"]))


def constructorify(sense: str, lang: Language, config: FunctionConfig, paths: i.CandidatePathSet, /) -> Iterator[CatenaZipper]:
    """Converts a compound lexeme with a particular sense into a Constructor.

    This delegates the actual transformation to a function to be found by find_constructorify.

    Args:
        sense: Concept to generate a Catena for.
        lang: Language in which to generate the Catena.
        config: Additional function configuration.
        paths: Paths from the concept to all senses linked to it.

    Yields:
        Catenae reflecting transformations of lexemes in other languages into Constructors.

    Todo:
        move this to the pre-hook step somehow?
    """
    for sense, _ in paths.items():
        if not tfsli.is_lsid(sense):
            continue
        source_language = L(sense).language

        try:
            constructorifier = find_constructorify(source_language)
        except KeyError:
            continue

        continue  # TODO: find a way to prevent loops with the line below
        renderer = constructorifier(zip_lid(sense).expand()).render(lang)
        if isinstance(renderer, CatenaZipper):
            yield renderer
