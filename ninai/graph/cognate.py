"""Holds language-agnostic functionality for finding cognates of lexemes."""

import logging
from typing import Iterator

import tfsl.interfaces as tfsli
import udiron.base.constants as c
from tfsl import L, Language
from udiron import CatenaZipper, FunctionConfig, zip_lid
from udiron.base.functionconfig import C_

import ninai.base.interfaces as i
import ninai.graph.client

logger = logging.getLogger(__name__)

def cognate(entity_in: str, language_in: Language, config_in: FunctionConfig, paths: i.CandidatePathSet) -> Iterator[CatenaZipper]:
    """Searches for lexemes linked etymologically to the lexeme of a particular sense, returning each cognate of that lexeme in the target language.

    Args:
        entity_in: Concept to generate a Catena for.
        language_in: Language in which to generate the Catena.
        config_in: Additional function configuration.
        paths: Paths from the concept to all senses linked to it.

    Yields:
        Catenae that are cognates of the concept provided.
    """
    if tfsli.is_lsid(entity_in):
        logger.info("Attempting cognate search for %s", entity_in)
        thing_to_search_for = tfsli.get_lid_string(entity_in)

        for lexeme, path in ninai.graph.client.search_graph(thing_to_search_for, "origin").items():
            if path["lang"] == c.wikidatan:
                continue
            elif L(lexeme).language == language_in:
                logger.info("Found %s, a cognate in language %s of %s (linked to %s)", lexeme, language_in, thing_to_search_for, entity_in)
                output = zip_lid(lexeme)
                current_adjacencies = output.get_config().get(C_.shortest_path_tree)
                adjacencies_to_add = [
                    *ninai.graph.client.build_sense_adjacency_list(entity_in, path),
                ]
                new_adjacencies = current_adjacencies or "" + "\n" + "\n".join(adjacencies_to_add)
                output = output.add_to_config(C_.shortest_path_tree(new_adjacencies))
                yield output
