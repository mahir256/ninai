"""Functionality for Ninai's graph client."""

import configparser
import logging
import ssl
import xmlrpc.client
import defusedxml.xmlrpc
from functools import lru_cache
from pathlib import Path
from typing import Dict, Iterator, List, Tuple
from urllib.parse import urlparse

import tfsl.interfaces as tfsli
import udiron.base.constants as c
from tfsl import L, Language, langs
from tfsl.auth import retrieve_multiple_entities
from udiron import Catena, CatenaZipper, DependentEntry, DependentList
from udiron.base.catenaconfig import CatenaConfig
from udiron.base.functionconfig import C_, FunctionConfig, StringSet
from udiron.base.interfaces import get_language_fallbacks

import ninai.base.interfaces as i
import ninai.base.utility as u
import ninai.graph.utility as gu
from ninai.graph import run_fallbacks
from ninai.renderers import find_sense_refine

defusedxml.xmlrpc.monkey_patch()

sslcontext = ssl.SSLContext()

logger = logging.getLogger(__name__)

__language_range__: Dict[Language, Tuple[tfsli.Qid, ...]] = {}


def read_config_client() -> Tuple[str, int]:
    """Reads the config file residing at /path/to/ninai/config.ini.

    Returns:
        Host and port of the graph server.
    """
    config = configparser.ConfigParser()
    current_config_path = (Path(__file__).parent / "../../config.ini").resolve()
    config.read(current_config_path)
    server_path = urlparse(config["Ninai"]["GraphServerURL"])
    return server_path


server_path = read_config_client()


def standalone_filtering(chosen_candidate_paths: i.CandidatePathSet, language_in: Language, config_in: FunctionConfig) -> i.CandidatePathSet:
    """The default sense refinement mechanism, assuming a language does not have a separate one defined.

    Note that any such mechanisms defined for a language will still need to call this function directly
    if it is still desired.

    Args:
        chosen_candidate_paths: Paths from a concept to senses in a given language.
        language_in: The language in question.
        config_in: Additional function configuration.

    Returns:
        Filtered candidate paths.
    """
    chosen_candidate_paths = gu.get_target_language_set_paths(chosen_candidate_paths, language_in)
    chosen_candidate_paths = u.filter_for_location_of_sense_usage(chosen_candidate_paths, config_in)
    chosen_candidate_paths = u.filter_for_language_style(chosen_candidate_paths, config_in)
    chosen_candidate_paths = u.get_shortest_candidate_paths(chosen_candidate_paths)
    return chosen_candidate_paths


def build_sense_adjacency_list(concept: str, sense_path: i.CandidatePathNode) -> List[str]:
    """Turns the provided sense path into an adjacency list.

    Args:
        concept: Concept at the start of the path.
        sense_path: The path itself.

    Returns:
        List of edges in the path.
    """
    adjacencies = []
    previous_vertex = concept
    for node in sense_path["path"]:
        path_details, current_vertex = node
        current_row = previous_vertex + " " + current_vertex + " " + path_details["pred"]
        adjacencies.append(current_row)
        previous_vertex = current_vertex
    return adjacencies


def process_candidates(concept: str, candidate_paths: i.CandidatePathSet, language_in: Language, config_in: FunctionConfig) -> Iterator[CatenaZipper]:
    """Processes the candidate paths to find a suitable sense, and then constructs a Catena with that sense.

    Args:
        concept: Concept to produce a Catena for.
        candidate_paths: Paths from that concept to all linked senses.
        language_in: Language in which to generate a Catena.
        config_in: Additional function configuration.

    Yields:
        Catena representing the provided concept in the provided language.

    Raises:
        KeyError: if for whatever reason no paths were provided to this function.
    """
    new_config = CatenaConfig.new()

    if len(candidate_paths) == 0:
        raise KeyError("How did you even get to this point?")

    chosen_candidate_paths = candidate_paths
    try:
        refine_function = find_sense_refine(language_in)
    except KeyError:
        refine_function = None
    if refine_function:
        chosen_candidate_paths, new_config = refine_function(chosen_candidate_paths, language_in, new_config, config_in)
    else:
        chosen_candidate_paths = standalone_filtering(chosen_candidate_paths, language_in, config_in)

    for sense_id, sense_path in chosen_candidate_paths.items():
        new_lexeme = L(sense_id)
        adjacencies = build_sense_adjacency_list(concept, sense_path)
        new_config = new_config.set(C_.shortest_path_tree, "\n".join(adjacencies))
        yield CatenaZipper([
            DependentEntry(
                Catena(
                    new_lexeme,
                    language_in,
                    new_lexeme[sense_id],
                    frozenset(),
                    new_config,
                    DependentList([], []),
                    Catena.next_id(),
                ), c.syntactic_root,
            ),
        ])


def add_language_range(language_in: Language, target_languages: List[Language]) -> None:
    """Sets a language range based on the items in the set of Languages.

    Args:
        language_in: Language to adjust the range of.
        target_languages: Language to add to that range.
    """
    __language_range__[language_in] = tuple(language.item for language in target_languages)


def get_language_range(language_in: Language) -> Tuple[tfsli.Qid, ...]:
    """Retrieves, for a particular input language, the range of languages for which to consider senses in a graph search.

    Args:
        language_in: Language to find a range for.

    Returns:
        Language set.
    """
    if language_in in __language_range__:
        return (language_in.item,) + __language_range__[language_in]
    return tuple(lang.item for lang in get_language_fallbacks(language_in))


@lru_cache()
def search_graph(concept_in: str, network_in: str) -> i.CandidatePathSet:
    """Calls out to the graph server to return paths from the provided concept to senses in any of the languages in the provided language list in the named sense network in question.

    Args:
        concept_in: Concept to search for.
        network_in: Name of sense subgraph to search in.

    Returns:
        Mapping from sense IDs to information about the paths from the concept to those senses.

    Raises:
        TypeError: if the result from a graph search is not a candidate path set.
    """
    context_to_use = sslcontext if server_path.scheme == "https" else None
    with xmlrpc.client.ServerProxy(server_path.geturl(), context=context_to_use) as proxy:
        candidate_paths = proxy.search_graph(concept_in, network_in)
        if not isinstance(candidate_paths, dict):
            raise TypeError("A candidate path set was not returned by the graph server")
    return candidate_paths


def has_exclusion(config: FunctionConfig, k: tfsli.LSid) -> bool:
    """Checks for an exclusion in the provided FunctionConfig.

    Args:
        config: FunctionConfig possibly with an exclusion set.
        k: Entry to check for.

    Returns:
        Whether the provided entry is in the exclusion set.
    """
    exclusions = config.get(C_.exclusion)
    if exclusions is None:
        return False
    if isinstance(exclusions, StringSet):
        return exclusions.has(k)
    return False


def filter_paths(all_paths: i.CandidatePathSet, target_language_range: Tuple[tfsli.Qid, ...]) -> i.CandidatePathSet:
    """Simply returns those paths where the language of the sense is in the target language range.

    Args:
        all_paths: Current set of candidate paths.
        target_language_range: Range of languages to select for.

    Returns:
        Filtered path set.
    """
    return {concept: path for concept, path in all_paths.items() if path["lang"] in target_language_range}


def pre_retrieve_entities(entities: List[tfsli.LSid]) -> List[tfsli.EntityPublishedSettings]:
    """Retrieves a list of entities based on their IDs.

    Args:
        entities: List of entities to retrieve.

    Returns:
        Mapping from entity IDs to the entities themselves.
    """
    entities = [(tfsli.get_lid_string(entity) if (tfsli.is_lfid(entity) or tfsli.is_lsid(entity)) else entity) for entity in entities]
    return retrieve_multiple_entities(entities)


def find_or_fallback(concept: str, network: str, language: Language, config: FunctionConfig) -> Iterator[CatenaZipper]:
    """Produces an iterator through the set of possible lexical concepts for a given language.

    Args:
        concept: Concept to find an equivalent for.
        network: Sense subgraph to search for an equivalent.
        language: Language to find an equivalent for.
        config: Additional function configuration.

    Yields:
        Catenae representing something, or constructs one using the language's fallback chain if no sense is found and a fallback is passed in.
    """
    target_language_range = get_language_range(language)[:-1]
    all_paths = search_graph(concept, network)
    candidate_paths = filter_paths(all_paths, target_language_range)

    concept_language = langs.mis_.item
    if tfsli.is_lsid(concept):
        concept_language = L(concept).language.item
    if concept_language in target_language_range or "Q20923490" in target_language_range:
        candidate_paths[concept] = {"lang": target_language_range[0], "path": []}

    empty_candidate_list = len(candidate_paths) == 0
    wrong_target_language_for_sense = tfsli.is_lsid(concept) and len(candidate_paths) == 1 and concept in candidate_paths and concept_language not in target_language_range
    if not (empty_candidate_list or wrong_target_language_for_sense):
        if concept_language != langs.mis_.item:
            logger.info("Found %d senses for %s (%s) in %s network in language %s", len(candidate_paths), concept, concept_language, network, language)
        else:
            logger.info("Found %d senses for %s in %s network in language %s", len(candidate_paths), concept, network, language)
        pre_retrieve_entities(list(candidate_paths.keys()))
        yield from process_candidates(concept, candidate_paths, language, config)
        logger.info("Exhausted all senses for %s in %s network in language %s, falling back...", concept, network, language)
    else:
        if concept_language != langs.mis_.item:
            logger.info("No sense found for %s (%s) in %s network in language %s, falling back...", concept, concept_language, network, language)
        else:
            logger.info("No sense found for %s in %s network in language %s, falling back...", concept, network, language)
    all_paths = {k: v for k, v in all_paths.items() if k not in candidate_paths and not has_exclusion(config, k)}
    if tfsli.is_lsid(concept) and concept not in all_paths:
        all_paths[concept] = {"lang": c.undetermined_language, "path": []}
    pre_retrieve_entities(list(all_paths.keys()))
    yield from run_fallbacks(concept, network, language, config, all_paths)


def find_senses(concept_in: str, language_in: Language, config_in: FunctionConfig) -> Iterator[CatenaZipper]:
    """Produces an iterator through the substantive network.

    Args:
        concept_in: Concept to find an equivalent for.
        language_in: Language to find an equivalent for.
        config_in: Additional function configuration.

    Returns:
        Iterator through all possible outputs from the sense finding step.
    """
    return find_or_fallback(concept_in, "substantive", language_in, config_in)


def find_sense(concept_in: str, language_in: Language, config_in: FunctionConfig) -> CatenaZipper:
    """Gets a lexical equivalent for a particular concept.

    Args:
        concept_in: Concept to find an equivalent for.
        language_in: Language to find an equivalent for.
        config_in: Additional function configuration.

    Returns:
        Catena representing a (usually nominal) concept in the provided language.
    """
    output_iterator = find_senses(concept_in, language_in, config_in)
    return next(output_iterator)


def find_demonym(concept_in: str, language_in: Language, config_in: FunctionConfig) -> CatenaZipper:
    """Gets a lexical equivalent for a particular demonym.

    Args:
        concept_in: Concept to find an equivalent for.
        language_in: Language to find an equivalent for.
        config_in: Additional function configuration.

    Returns:
        Catena representing a demonym.
    """
    output_iterator = find_or_fallback(concept_in, "demonym", language_in, config_in)
    return next(output_iterator)


def find_predicate(concept_in: str, language_in: Language, config_in: FunctionConfig) -> CatenaZipper:
    """Gets a lexical equivalent for a particular predicate.

    Args:
        concept_in: Concept to find an equivalent for.
        language_in: Language to find an equivalent for.
        config_in: Additional function configuration.

    Returns:
        Catena representing a predicate.
    """
    output_iterator = find_or_fallback(concept_in, "predicate", language_in, config_in)
    return next(output_iterator)
