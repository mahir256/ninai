"""This is intended to print some simple sentences in Turkish (in both the Latin and Ottoman scripts)...but doesn't do that quite yet.

The expected output from the above should look something like the following
(or something equally random but with the same lexemes in the same order):

```
Biz sana suyu Galata'nın içinde verdik.
بز سڭا صویی غلطهنڭ ایچنده ویردیك.
```
"""

from datetime import datetime, timedelta

import udiron.base.constants as c
from tfsl import langs

from ninai.base.constructorinterfaces import deserialize_constructor
from ninai.renderers import import_language

import_language("tr")

sentences = []
current_time = datetime.now()
for subject in [c.speaker, c.listener]:
    for concept_in in ["L1225904-S1", "L415307-S1", "L1222666-S1", "L729736-S1"]:
        for timedifference in [-1, 0, 1]:
            thistime = current_time + timedelta(days=timedifference)
            sentences.append(
                [
                    c.context,
                    [
                        c.predicate,
                        "Q16509017",
                        [c.thematic_relation, [subject], c.agent],
                        [
                            c.thematic_relation,
                            [c.concept, concept_in], c.patient,
                        ],
                        [
                            c.thematic_relation,
                            [c.listener], c.recipient,
                        ],
                        [
                            c.inessive,
                            [c.concept, "Q501817"],
                        ],
                        [c.identifier, "Hello"],
                        [c.timestamp_computing, c.occurrence_time, thistime.isoformat()],
                    ],
                ],
            )

languages = [langs.tr_]  # , langs.ota_]

if __name__ == "__main__":
    for sentence in sentences:
        ctor = deserialize_constructor(sentence)
        for LANG in languages:
            x_out = ctor.render(LANG)
            print(x_out)
