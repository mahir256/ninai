"""This is intended to print some simple sentences in Norwegian (approximating the chorus to Jahn Teigen's "Optimist")...but doesn't do that quite yet.

The expected output from the above should look something like the following
(or something equally random but with the same lexemes in the same order):

```
Jeg er en optimist.
Jeg vet det gå bra til sist.
Jeg er en optimist så lenge jeg lever her.
```
"""

from itertools import product

import udiron.base.constants as c
from tfsl import langs

from ninai.base.constructorinterfaces import deserialize_constructor
from ninai.renderers import import_language

import_language("norwegian")

sentences = [
    [
        c.context, [
            c.classificational_construction, [
                c.concept, "Q44320",
            ],
            [
                c.concept, "Q482994",
            ],
        ],
        [
            c.classificational_construction,
            [c.speaker],
            [c.concept, "Q108492219"],
        ],
        [
            c.predicate, "Q46744", [
                c.thematic_relation, [c.speaker], c.experiencer,
            ],
            [
                c.thematic_relation, [c.predicate, "Q7562091"], c.stimulus,
            ],
        ],
        [
            c.classificational_construction, [c.listener],
            [
                c.concept, "Q108492219",
            ],
            [
                c.simultaneity, [
                    c.predicate, "Q21075715", [
                        c.thematic_relation, [c.speaker], c.experiencer,
                    ],
                    [
                        c.locative, [c.concept, "Q3947"],
                    ],
                ],
            ],
        ],
    ],
]

languages = [langs.nb_]  # , langs.nn_]

RUNS = product(sentences, languages)

if __name__ == "__main__":
    for sentence, LANG in RUNS:
        ctor = deserialize_constructor(sentence)
        x_out = ctor.render(LANG)
        print(x_out)
