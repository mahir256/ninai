"""This is intended to print some simple sentences in Turkish calqued from proverbs in Hindustani.

The expected output from the above should look something like the following

```
Beşer açiz.
Bende beşer.
```
"""

from itertools import product

import udiron.base.constants as c
from tfsl import langs

from ninai.base.constructorinterfaces import deserialize_constructor
from ninai.renderers import import_language

import_language("tr")

sentences = [
    [c.concept, "L1230797-S1"],
    [c.concept, "L1230799-S1"],
]

languages = [langs.tr_]

RUNS = product(sentences, languages)

if __name__ == "__main__":
    for sentence, LANG in RUNS:
        ctor = deserialize_constructor(sentence)
        x_out = ctor.render(LANG)
        print(x_out)
