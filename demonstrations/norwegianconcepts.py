"""This simply tests that a list of concepts that don't have Norwegian lexemes can still be calqued into Norwegian."""

from itertools import product

import udiron.base.constants as c
from tfsl import langs

from ninai.base.constructorinterfaces import deserialize_constructor
from ninai.renderers import import_language

import_language("norwegian")

senses = [
    "Q73533201", "L424991-S1",
    "L415370-S1",
    "L383560-S1",
    "L415093-S1",
    "L439278-S1",
    "L351487-S1",
    "L415305-S1",
    "L351182-S1",
    "L415301-S1",
    "L415294-S1",
    "L359837-S1",
    "L421149-S1",
    "L433527-S1",
    "L415300-S1",
    "L367509-S1",
    "L425517-S1",
    "L425557-S1",
    "L444424-S1",
    "L394993-S1",
    "L415396-S1",
    "L415398-S1",
    "L351040-S1",
    "L427133-S1",
    "L426937-S1",
    "L428524-S1",
    "L415295-S1",
    "L419428-S1",
    "L415103-S1",
    "L357195-S1",
    "L444645-S1",
    "L350730-S1",
    "L356460-S1",
    "L351179-S1",
    "L387965-S1",
    "L419554-S1",
    "L446291-S1",
    "L439326-S1",
    "L423555-S1",
    "L363291-S1",
]
sentences = [[c.context, [c.concept, sense]] for sense in senses]

languages = [langs.nb_]  # , langs.nn_]

RUNS = product(sentences, languages)

if __name__ == "__main__":
    for sentence, LANG in RUNS:
        ctor = deserialize_constructor(sentence)
        x_out = ctor.render(LANG)
        print(x_out)
        break
